﻿!function () {
    'use strict';
    var app = angular.module("SavisApp");
    app.controller('EmployeeController', ['$scope', '$location', '$filter', '$http', '$uibModal', '$routeParams', '$log', '$q', 'UtilsService', 'toastr', 'UserApiService', 'ConstantsApp', 'CinemaApiService',
        function ($scope, $location, $filter, $http, $uibModal, $routeParams, $log, $q, UtilsService, $notifications, UserApiService, ConstantsApp, CinemaApiService) {

            /* CONFIGs */
            /* ------------------------------------------------------------------------------- */
            $scope.Config = {};
            /* Declare variables url form popup add,edit,info */
            var passwordChangeDialogTemplateUrl = '/app-data/views/core/users/passwordChangeDialog.html';
            var itemDialogTemplateUrl = '/app-data/views/core/employee/employee-item.html';

            // Lấy id người dùng hiện tại
            var currentUserId = app.CurrentUser.Id;
            var currentAppId = app.CurrentUser.ApplicationId;
            /* ------------------------------------------------------------------------------- */

            /* GRIDVIEWs */
            /* ------------------------------------------------------------------------------- */
            $scope.Grid = ConstantsApp.GRID_ATTRIBUTES;

            $scope.Grid.DataHeader = [
                { Key: 'UserName', Value: "Tên đăng nhập", Width: '15%', IsSortable: false },
                { Key: 'FullName', Value: "Họ tên NV", Width: '20%', IsSortable: false },
                { Key: 'CenimaName', Value: "NV rạp", Width: '20%', IsSortable: false },
                { Key: 'LastActivityDate', Value: "H.động gần nhất", Width: '20%', IsSortable: true },
                { Key: 'Active', Value: "Trạng thái", Width: '10%', IsSortable: true },
                { Key: 'Actions', Value: "Thao tác", Width: 'auto', IsSortable: false }
            ];

            $scope.Grid.Search = function () {
                $location.search("pn", 1);
                $location.search("ps", $scope.Grid.PageSize);
                $location.search("ts", $scope.Filter.Text);
                initData();
            };
            /* Sorting default*/
            var orderBy = $filter('orderBy');
            $scope.Grid.Order = function (predicate, reverse) {
                $scope.Grid.Data = orderBy($scope.Grid.Data, predicate, reverse);
            };
            /* This function is to Check all item */
            $scope.Grid.CheckAllItem = function () {
                $scope.Grid.Check = !$scope.Grid.Check;

                angular.forEach($scope.Grid.Data, function (item) {
                    item.selected = $scope.Grid.Check;
                });

                // If list has more than 1 item
                if ($scope.Grid.Check && $scope.Grid.Data.length > 1) {
                    $scope.Button.Update.Visible = false;
                    $scope.Button.Delete.Visible = true;
                }

                // If list has 1 item
                if ($scope.Grid.Check && $scope.Grid.Data.length === 1) {
                    $scope.Button.Update.Visible = true;
                    $scope.Button.Delete.Visible = true;
                }

                if (!$scope.Grid.Check) {
                    $scope.Button.Update.Visible = false;
                    $scope.Button.Delete.Visible = false;
                }
            };
            /* This function is to Check items in grid highlight*/
            $scope.Grid.GetItemClass = function (item) {
                if (item.selected === true) {
                    return 'selected';
                } else {
                    return '';
                }
            };
            /* This function is to Check items in grid highlight*/
            $scope.Grid.GetItemHeaderClass = function (item) {
                if (item.selected === true) {
                    return 'sorting_asc';
                } else if (item.selected === false) {
                    return 'sorting_desc';
                } else if (!item.IsSortable) {
                    return '';
                } else {
                    return 'sorting';
                }
            };
            /* Sorting grid By Click to header */
            $scope.Grid.ClickToHeader = function (item) {
                if (item.IsSortable) {
                    // Set all class header to default
                    angular.forEach($scope.dataHeader, function (items) {
                        if (items !== item) {
                            items.selected = null;
                        }
                    });
                    // Set for item click sorted
                    if (item.selected === true) {
                        item.selected = false;
                        $scope.Grid.Order(item.Key, false);
                    } else {
                        item.selected = true;
                        $scope.Grid.Order("-" + item.Key, false);
                    }
                }
            };
            /* This function is to Check items in grid*/
            $scope.Grid.CheckItem = function (item) {
                var count = 0;
                // Calculate item selected count     
                angular.forEach($scope.Grid.Data, function (item) {
                    if (item.selected === true) { count++; }
                });

                // Reset all buttons
                $scope.Button.Update.Visible = false;
                $scope.Button.Delete.Visible = false;
                $scope.Grid.Check = false;

                if (count === 1) {
                    $scope.Button.Update.Visible = true;
                    $scope.Button.Delete.Visible = true;
                    $scope.Grid.CheckAll = false;
                }

                if (count > 1) {
                    $scope.Button.Update.Visible = false;
                    $scope.Button.Delete.Visible = true;
                    $scope.Grid.CheckAll = false;
                }
                // If all items is checked
                if (count > 0 && count === $scope.Grid.Data.length) {
                    $scope.Button.Update.Visible = false;
                    $scope.Button.Delete.Visible = true;
                    $scope.Grid.Check = true;
                    $scope.Grid.CheckAll = true;
                }
            };
            /* This function click refesh grid program */
            $scope.Grid.Refesh = function () {
                $scope.Grid.PageNumber = 1;
                $scope.Filter.Text = "";
                $location.search("ts", $scope.Filter.Text);
                initData();
            };
            /* ------------------------------------------------------------------------------- */
            $scope.Grid.ExportExcel = function () {
                excelExport();
            };

            /* FORM */
            /* ------------------------------------------------------------------------------- */
            $scope.Form = {};
            $scope.Form.OptionActive = ConstantsApp.OPTION_ACTIVE_DEACTIVE;

            $scope.FormMode = {};
            $scope.FormMode.IsReadMode = false;
            $scope.FormMode.IsAllowDelete = false;

            /* ------------------------------------------------------------------------------- */


            /* BUTTONs */
            /* ------------------------------------------------------------------------------- */
            $scope.Button = {};
            // Button ADD
            $scope.Button.Add = { Enable: true, Visible: true, Title: "Thêm mới", GrantAccess: true };
            $scope.Button.Add.Click = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: itemDialogTemplateUrl,
                    controller: 'EmployeeModalController',
                    size: 'md',
                    keyboard: false,
                    backdrop: 'static',
                    resolve: {
                        item: function () {
                            var obj = {};
                            return obj;
                        },
                        option: function () {
                            return [{ Type: 'add' }];
                        }
                    }
                });
                modalInstance.result.then(function (response) {
                    if (response.Status === 1) {
                        $notifications.success('Thêm mới thành công', 'Thông báo');
                        initData();
                    };
                }, function () {
                    // $log.info('Modal dismissed at: ' + new Date());
                });
            };

            // Button UPDATE
            $scope.Button.Update = { Enable: true, Visible: true, Title: "Cập nhật", GrantAccess: true };
            $scope.Button.Update.Click = function (item) {
                var modalInstance = $uibModal.open({
                    templateUrl: itemDialogTemplateUrl,
                    controller: 'EmployeeModalController',
                    size: 'md',
                    keyboard: false,
                    backdrop: 'static',
                    resolve: {
                        item: function () {
                            var obj = {};
                            obj = item;
                            return obj;
                        },
                        option: function () {
                            return [{ Type: 'edit' }];
                        }
                    }
                });

                modalInstance.result.then(function (response) {
                    if (response.Status !== 1) {
                        $notifications.error('Cập nhật xảy ra lỗi');
                        $scope.Grid.PageNumber = 1;
                    } else if (response.Status === 1) {
                        $notifications.success('Cập nhật thành công');
                    }
                    $scope.Button.Delete.Visible = false;
                    initData();
                }, function () {
                    //$log.info('Modal dismissed at: ' + new Date());
                });
            };

            // Button DETAIL
            $scope.Button.Detail = {};
            $scope.Button.Detail.Click = function (item) {
                var modalInstance = $uibModal.open({
                    templateUrl: itemDialogTemplateUrl,
                    controller: 'OrganizationModalCtrl',
                    size: 'lg',
                    //windowClass: 'modal-full',
                    keyboard: false,
                    backdrop: 'static',
                    // Set parameter to chidform (popup form)
                    resolve: {
                        item: function () {
                            var obj = item;
                            return obj;
                        },
                        option: function () {
                            return { Type: 'detail', ReaderId: item.ReaderId };
                        }
                    }
                });

                modalInstance.result.then(function (response) {
                    if (response.Status === -2) {
                        $log.info('Modal dismissed at: ' + new Date());
                    }
                    $scope.Grid.Check = false;
                    $scope.Button.Delete.Visible = false;
                    $scope.Button.Update.Visible = false;
                    $scope.Grid.PageNumber = 1;
                    initData();
                }, function () {
                    //$log.info('Modal dismissed at: ' + new Date());
                });
            };

            // Button DELETE
            $scope.Button.Delete = { Enable: true, Visible: false, Title: "Xóa đã chọn", GrantAccess: true };
            $scope.Button.Delete.Click = function (item) {
                var listItemDeleteId = [];
                var count = 0;
                if (item !== null && typeof item !== 'undefined') {
                    listItemDeleteId.push(item.OrganizationId);
                    count++;
                } else {
                    for (var i = 0; i < $scope.Grid.Data.length; i++) {

                        if ($scope.Grid.Data[i].selected === true) {
                            listItemDeleteId.push($scope.Grid.Data[i].OrganizationId);
                            count++;
                        }
                    }
                }

                var message = "";
                if (count > 1) {
                    message = "Bạn có chắc muốn xóa những đơn vị, phòng ban này ?";
                } else {
                    message = "Bạn có chắc muốn xóa đơn vị, phòng ban này ?";
                }
                var infoResult = UtilsService.OpenDialog(message, 'Xác nhận', 'Đồng ý', 'Hủy', 'sm', null);

                infoResult.result.then(function (modalResult) {
                    if (modalResult === 'confirm') {
                        var listDeleteField = [];

                        var promise = OrganizationApiService.DeleteMany(listItemDeleteId);

                        promise.then(function (response) {
                            angular.forEach(response.data.Data, function (item) {
                                if (item.Result !== -1) {
                                    listDeleteField.push({ Name: item.Name, Result: item.Result === 1 ? true : false, Message: item.Message });
                                }
                                else if (item.Result === -1) {
                                    $scope.error('Lỗi hệ thống !');
                                }
                            });
                            $scope.Grid.PageNumber = 1;
                            initData();

                            var infoDeleteResult = UtilsService.OpenDialog('Kết quả xóa', 'Chú ý', '', 'Đóng', 'md', listDeleteField);
                        });
                    }
                });
                /* Check all item is selected => Request to API  */
            };

            // Button GOTOPAGE
            $scope.Button.GoToPage = {};
            $scope.Button.GoToPage.Click = function (pageTogo) {
                $location.search("pn", pageTogo);
                $location.search("ts", $scope.Filter.Text);
                $location.search("ps", $scope.Grid.PageSize);
                initData();
            };

            // Button GOTOPAGE
            $scope.Button.ChangePageSize = {};
            $scope.Button.ChangePageSize.Click = function () {
                $location.search("ps", $scope.Grid.PageSize);
                initData();
            };

            /* ------------------------------------------------------------------------------- */

            /* DROPDOWNLIST FUNCTIONS */
            /* ------------------------------------------------------------------------------- */
            $scope.DropDownList = {};

            $scope.DropDownList.Cinema = { Visible: false, Data: [], Seleted: {} };

            $scope.DropDownList.Cinema.SelectedChange = function () {
                initData();
            };
            /* ------------------------------------------------------------------------------- */

            /* FILTERs */
            /* ------------------------------------------------------------------------------- */
            $scope.Filter = {};
            $scope.Filter.Text = "";

            /* ------------------------------------------------------------------------------- */

            /* INIT FUNCTIONS */
            /* ------------------------------------------------------------------------------- */

            // Init nút chức năng theo quyền người dùng
            var initButtonByRightOfUser = function () {
                $scope.Button.Add.GrantAccess = UtilsService.CheckRightOfUser("EMPLOYEE-ADD");
                $scope.Button.Update.GrantAccess = UtilsService.CheckRightOfUser("EMPLOYEE-UPDATE");
                $scope.Button.Delete.GrantAccess = UtilsService.CheckRightOfUser("EMPLOYEE-DELETE");
                return true;
            };

            // Init nhóm quyền của người dùng đăng nhập
            var initCinema = function () {
                var p = CinemaApiService.GetListCinema(1, 100, "", null);
                p.then(function onSuccess(response) {
                    var responseResult = response.data;
                    if (responseResult.Status === 1) {
                        $scope.DropDownList.Cinema.Data = responseResult.Data;
                    } else $notifications.error("Lấy danh sách thất bại", "Lỗi");
                });
                return p;
            };

            /* Load data from API binding to Grid*/
            var initData = function () {
                $scope.Grid.CheckAll = false;
                $scope.Button.Delete.Visible = false;
                angular.forEach($scope.Grid.Data, function (items) {
                    items.selected = null;
                });
                $scope.Grid.Check = null;

                var qs = $location.search();
                /* PageNumber-----*/
                if (typeof (qs["pn"]) !== 'undefined') {
                    $scope.Grid.PageNumber = parseInt(qs["pn"]);
                } else {
                    $location.search("pn", "1");
                    $scope.Grid.PageNumber = 1;
                };
                /* PageSize-----*/
                if (typeof (qs["ps"]) !== 'undefined') {
                    $scope.Grid.PageSize = parseInt(qs["ps"]);
                } else {
                    $location.search("ps", "10");
                    $scope.Grid.PageSize = 10;
                };
                /* TextSearch-----*/
                if (typeof (qs["ts"]) !== 'undefined') {
                    $scope.Filter.Text = qs["ts"];
                } else {
                    $location.search("ts", "");
                    $scope.Filter.Text = "";
                };
                // Get data from api
                var postData = {};
                postData.PageNumber = $scope.Grid.PageNumber;
                postData.PageSize = $scope.Grid.PageSize;
                postData.TextSearch = $scope.Filter.Text;
                postData.Type = 1; // 1: Employee               
                if (currentAppId !== ConstantsApp.HEAD_OFFICE_APP_ID) postData.ApplicationId = currentAppId;
                else {
                    if ($scope.DropDownList.Cinema.Selected !== "undefined" && typeof ($scope.DropDownList.Cinema.Selected) !== "undefined" && $scope.DropDownList.Cinema.Selected !== null) {
                        postData.ApplicationId = $scope.DropDownList.Cinema.Selected.CinemaId;
                    }
                }
                var p = UserApiService.GetFilter(postData);
                p.then(function onSuccess(response) {
                    console.log("SUCCESS", response);
                    var responseResult = response.data;
                    $scope.Grid.Data = responseResult.Data;
                    //Enable next button
                    $scope.Grid.TotalCount = responseResult.TotalCount;
                    $scope.Grid.TotalPage = Math.ceil($scope.Grid.TotalCount / $scope.Grid.PageSize);
                    $scope.Grid.FromRecord = parseInt(($scope.Grid.PageNumber - 1) * $scope.Grid.PageSize + 1);
                    $scope.Grid.ToRecord = $scope.Grid.FromRecord + $scope.Grid.Data.length - 1;

                    $scope.Grid.Data.forEach(function (employee) {
                        var cinemaOfEmployee = $scope.DropDownList.Cinema.Data.filter(function (st) { return st.CinemaId === employee.ApplicationId })[0];
                        employee.Cinema = cinemaOfEmployee.Name;
                    });
                    //$notifications.info("Tải dữ liệu thành công", "Dữ liệu");
                }, function onError(response) {
                    console.log("ERROR", response);
                });
            };

            var init = function () {
                // Check HO
                if (currentAppId === ConstantsApp.HEAD_OFFICE_APP_ID) $scope.DropDownList.Cinema.Visible = true;
                else $scope.DropDownList.Cinema.Visible = false;

                $q.all([initCinema()]).then(function () {
                    initButtonByRightOfUser();
                    initData();
                });
            };

            var excelExport = function () {
                // Get data from api
                var postData = {};
                postData.PageNumber = $scope.Grid.PageNumber;
                postData.PageSize = $scope.Grid.PageSize;
                postData.TextSearch = $scope.Filter.Text;
                postData.Type = 1; // 1: Employee               
                if (currentAppId !== ConstantsApp.HEAD_OFFICE_APP_ID) postData.ApplicationId = currentAppId;
                else {
                    if ($scope.DropDownList.Cinema.Selected !== "undefined" && typeof ($scope.DropDownList.Cinema.Selected) !== "undefined" && $scope.DropDownList.Cinema.Selected !== null) {
                        postData.ApplicationId = $scope.DropDownList.Cinema.Selected.CinemaId;
                    }
                }
                var p = UserApiService.ExcelExport(postData);
                //window.open(p);
                $http({
                    method: 'GET',
                    url: p,
                    headers: {
                        'Content-type': 'application/json; charset=utf-8'
                    },
                    responseType: 'arraybuffer'
                }).then(function (data, status, headers) {
                    //console.log(data);
                    //console.log(status);
                    //console.log(headers);
                    headers = data.headers();
                    var filename = 'Danh sách nhân viên bán hàng.xlsx';
                    var contentType = headers['content-type'];
                    var linkElement = document.createElement('a');
                    try {
                        var blob = new Blob([data.data], {
                            type: contentType
                        });
                        var url = window.URL.createObjectURL(blob);

                        linkElement.setAttribute('href', url);
                        linkElement.setAttribute("download", filename);

                        var clickEvent = new MouseEvent("click", {
                            "view": window,
                            "bubbles": true,
                            "cancelable": false
                        });
                        linkElement.dispatchEvent(clickEvent);
                    } catch (ex) {
                        $scope.ToExcel = true;
                        $notifications.error("Có lỗi xảy ra", "Lỗi");
                    }
                }).then(function () {
                    $scope.ToExcel = true;
                    //$notifications.error("Có lỗi xảy ra", "Lỗi");
                });
            };

            init();

            /* ------------------------------------------------------------------------------- */
        }]);
    /* Controller for froms Popup Add,Edit,Delete or Info */
    app.controller("EmployeeModalController", ["$scope", "$q", "$http", "$uibModalInstance", '$uibModal', 'item', 'option', "toastr", 'UtilsService', "UserApiService", "RoleApiService", 'CinemaApiService',
        function ($scope, $q, $http, $uibModalInstance, $uibModal, item, option, $notifications, UtilsService, UserApiService, RoleApiService, CinemaApiService) {

            $scope.Form = {};
            $scope.Form.Title = "";
            $scope.Form.Item = item;
            $scope.Form.FormType = option[0].Type;
            $scope.Form.RoleDefault = {
                Code: "NVBH", RoleId: "c8509cc3-aac0-4b66-a45c-a53348335c01"
            };

            $scope.FormMode = { IsAdd: true, IsEdit: false, IsReadOnly: false };

            //$scope.word = /^\s*\w*\s*\g*$/;
            $scope.word = /(?!.*[\.\-\_]{2,})^[a-zA-Z0-9\.\-\_]{3,24}$/;
            var profileDialogTemplateUrl = '/app-data/views/core/employee/employee-change-password.html';
            var infoDialogTemplateUrl = '/app-data/views/core/users/infoDialog.html';
            var defaultOrgani = { OrganizationId: null, Name: "--Chọn đơn vị/phòng ban--" };
            var defaultChucVu = { CatalogItemId: null, Name: "--Chọn chức vụ--" };
            /* Declare constant domain and path to Api */
            $scope.Autofocus = true;

            /* BUTTONs */
            /* ------------------------------------------------------------------------------- */

            $scope.Button = {};

            $scope.Button.ChangePassword = { GrantAccess: true };
            $scope.Button.ChangePassword.Click = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: profileDialogTemplateUrl,
                    controller: "AdminChangePassWordCtrl",
                    size: "md",
                    resolve: {
                        item: function () {
                            var obj = {
                                UserName: $scope.UserName
                            }
                            return obj;
                        }
                    }
                });
                modalInstance.result.then(function (returnedUser) {
                    if (returnedUser.Status === 1) {
                        $notifications.success(returnedUser.Message, "Thông báo");
                    } else {
                        $notifications.error(returnedUser.Message);
                    };
                }, function () {
                    // $log.info('Modal dismissed at: ' + new Date());
                });
            };

            $scope.Button.Save = {};
            $scope.Button.Save.Click = function () {
                $scope.user_form.$setSubmitted();
                if ($scope.user_form.$invalid) {
                    angular.element("[name='" + $scope.user_form.$name + "']").find('.ng-invalid:visible:first').focus();
                    UtilsService.OpenDialog('Vui lòng kiểm tra thông tin đã nhập!', 'Chú ý', '', 'Đóng', 'md', '');
                    return false;
                }

                /* If Type of parameter = 'add' ==> request to API with parameter */
                if (option[0].Type === "add") {
                    $scope.UserName = ($scope.UserName + "").replace(/(?:\s)\s/g, '');
                    if ($scope.UserName === "undefined")
                        $scope.UserName = "";

                    var checkName = UserApiService.CheckNameAvailability($scope.UserName);
                    checkName.then(function onSuccess(response) {
                        if (response.data.Status === -1) {
                            UtilsService.OpenDialog("Tên tài khoản đã tồn tại, vui lòng chọn tên khác!", "Chú ý", '', "Đóng", "md", '');
                            return false;
                        }

                        /* Check the two inserted password */
                        if ($scope.Form.Item.Password !== $scope.Form.Item.ConfirmedPassword) {
                            UtilsService.OpenDialog('Mật khẩu xác nhận không chính xác!', 'Chú ý', '', 'Đóng', 'md', '');
                            return false;
                        }

                        if ($scope.DropDownList.Cenima.Model === null) {
                            UtilsService.OpenDialog("Bạn cần chọn rạp!", 'Chú ý', '', 'Đóng', 'md', '');
                            return false;
                        }

                        // Add user
                        var postData = { Roles: [] };
                        postData.Roles.push($scope.Form.RoleDefault);
                        postData.ApplicationId = $scope.DropDownList.Cenima.Model.CinemaId;
                        addEmployee(postData);

                        return true;
                    });
                }
                    /* If Type of parameter = 'edit' ==> request to API with parameter */
                else {
                    var putData = {};
                    putData.UserId = $scope.Form.Item.UserId;
                    putData.ApplicationId = $scope.DropDownList.Cenima.Model.CinemaId;
                    updateEmployee(putData);
                };
            };

            $scope.Button.Close = {
                Click: function () {
                    $uibModalInstance.dismiss();
                }
            };

            /* ------------------------------------------------------------------------------- */

            /* DROPDOWNLIST FUNCTIONS */
            /* ------------------------------------------------------------------------------- */
            $scope.DropDownList = {};

            $scope.DropDownList.Cenima = { Data: [], Model: null };
            /* ------------------------------------------------------------------------------- */

            /* 
            * User name availability check
            * This section provive user availability check, to check whether an user name is valid or not
            * A scope function will be defined to store information of availability
            */
            $scope.Availability = {};
            $scope.Availability.Status = 0;

            $scope.SelectAllRole = function () {
                ////angular.forEach($scope.Roles, function (rl) {
                ////    $scope.SelectedRoles.push(rl);
                ////});
                //$scope.SelectedRoles = $scope.Roles;
            };




            /* Check availability function */
            $scope.CheckNameAvailability = function (name) {
                var p = UserApiService.CheckNameAvailability(name);
                p.then(function onSuccess(response) {
                    if (response.data.Status === -1) {
                        $scope.Availability.Status = -1;
                        return false;
                    }
                    $scope.Availability.Status = 1;
                    return true;
                });
            };

            /* Form data initiate */
            var addEmployee = function (postData) {
                postData.UserName = ($scope.UserName + "").replace(/(?:\s)\s/g, '');
                postData.FullName = ($scope.FullName + "").replace(/(?:\s)\s/g, '');
                postData.Mobile = $scope.Mobile;
                postData.Email = $scope.Email;
                postData.CreatedByUserId = app.CurrentUser.Id;
                postData.ModifiedByUserId = app.CurrentUser.Id;
                postData.Password = $scope.Form.Item.Password;
                postData.Active = $scope.Active;
                postData.Type = 1;
                postData.NickName = ($scope.NickName + "").replace(/(?:\s)\s/g, '');

                var p = UserApiService.AddNewEmployee(postData);
                p.then(function onSuccess(response) {
                    var dataResult = response.data;
                    if (dataResult.Status === 1) {
                        $uibModalInstance.close(dataResult);
                    } else if (dataResult.Status === 0 && dataResult.Message === "Code exist") {
                        $notifications.warning("Mã người dùng đã tồn tại", "Cảnh báo");
                    } else {
                        $notifications.error("Thêm mới thất bại", "Lỗi");
                    }
                });
            };

            var updateEmployee = function (putData) {
                putData.UserName = $scope.UserName;
                putData.ModifiedByUserId = app.CurrentUser.Id;
                putData.FullName = ($scope.FullName + "").replace(/(?:\s)\s/g, '');
                putData.Mobile = $scope.Mobile;
                putData.Email = $scope.Email;
                putData.Active = $scope.Active;
                putData.NickName = ($scope.NickName + "").replace(/(?:\s)\s/g, '');
                putData.UpdateDate = new Date();

                var p = UserApiService.UpdateEmployee(putData);
                p.then(function onSuccess(response) {
                    var dataResult = response.data;
                    if (dataResult.Status === 1) {
                        $uibModalInstance.close(dataResult);
                    } else {
                        $notifications.error("Cập nhật thất bại", "Lỗi");
                    }
                });
            };

            var initButtonByRightOfUser = function () {
                $q.all([UtilsService.Promise.Rights]).then(function () {
                    $scope.Button.ChangePassword.GrantAccess = UtilsService.CheckRightOfUser("EMPLOYEE-CHANGE-PASSWORD");
                });
                return true;
            };

            var initCinema = function () {

                var promise = CinemaApiService.GetListCinema(0, 0, "", null);
                promise.then(function (response) {
                    var responseResult = response.data;
                    if (responseResult.Status === 1) {
                        $scope.DropDownList.Cenima.Data = responseResult.Data;
                        // Check if not in site HO, default select current app 
                        var cinemaInList = $scope.DropDownList.Cenima.Data.filter(function (dt) { return dt.CinemaId === app.CurrentUser.ApplicationId });
                        if (typeof cinemaInList !== "undefined" && cinemaInList.length > 0) {
                            $scope.DropDownList.Cenima.Data = cinemaInList;
                            $scope.DropDownList.Cenima.Model = cinemaInList[0];
                        }
                    }
                });

                return promise;
            };

            var initFormData = function () {
                /* Check parameters type = 'edit' to load data edit to form edit */
                if (option[0].Type === "edit") {
                    $scope.FormMode.IsAdd = false;
                    $scope.FormMode.IsEdit = true;
                    $scope.FormMode.IsReadOnly = false;
                    $scope.Form.Title = "Cập nhật nhân viên bán hàng";
                    if ($scope.Form.Item != null) {
                        var p = UserApiService.GetUserInfo($scope.Form.Item.UserId);
                        p.then(function onSuccess(response) {
                            var dataResult = response.data;
                            $scope.UserName = dataResult.Data.UserName;
                            $scope.Email = dataResult.Data.Email;
                            $scope.Mobile = dataResult.Data.Mobile;
                            $scope.Comment = dataResult.Data.Comment;
                            $scope.NickName = dataResult.Data.NickName;
                            $scope.FullName = dataResult.Data.FullName;
                            $scope.AnhDaiDien = dataResult.Data.Avatar;
                            if ($scope.AnhDaiDien == null || $scope.AnhDaiDien === '') {
                                $scope.AnhDaiDien = 'assets/admin/layout4/img/icon-user-default.png';
                            }
                            $scope.Active = dataResult.Data.Active;

                        });
                    }

                } /* Check parameters type = 'info' to load data info to form info */
                else if (option[0].Type === "info") {
                    $scope.IsInfo = true;
                    $scope.Messenger = option[0].Messager;
                    $scope.readOnly = true;
                    $scope.ProgramTitle = "Thông tin chi tiết tài khoản người dùng";
                    if ($scope.CurrentUser != null) {
                        var promise = $http({
                            method: 'GET',
                            url: apiUrl + ApiUsersUrl + '/' + $scope.CurrentUser.UserId + '/profile',
                        })
                            .success(function (data) {
                                if (data.Data != null) {
                                    $scope.UserName = data.Data.UserName;
                                    $scope.Email = data.Data.Email;
                                    $scope.Mobile = data.Data.Mobile;
                                    $scope.Comment = data.Data.Comment;
                                    $scope.NickName = data.Data.NickName;
                                    $scope.FullName = data.Data.FullName;
                                    $scope.AnhDaiDien = data.Data.Avatar;
                                    if ($scope.AnhDaiDien == null || $scope.AnhDaiDien == '') {
                                        $scope.AnhDaiDien = 'assets/admin/layout4/img/icon-user-default.png';
                                    }
                                    $scope.PhongBanInfo = data.Data.OrganizationName;
                                    $scope.DonVi = data.Data.ParentOrganizationName;

                                    chucVuI.then(function (a) {
                                        angular.forEach($scope.ChucVuData, function (item) {
                                            if (item.CatalogItemId === data.Data.ChucVuId) {
                                                $scope.ChucVuInfo = item.Name;
                                            }
                                        });
                                    });
                                    // $scope.Mobile = $scope.CurrentUser.Mobile;
                                    //$scope.ChucVu = $scope.CurrentUser.ChucVu;
                                    $scope.Active = $scope.CurrentUser.Active;

                                }
                            }).error(function (response) {
                                $log.error();
                                $log.debug(response);
                            });
                    }
                } /* Check parameters type = 'add' to load data add to form add */

                else if (option[0].Type === "add") {
                    $scope.Form.Title = "Thêm mới nhân viên";
                    $scope.FormMode.IsAdd = true;
                    $scope.FormMode.IsEdit = false;
                    $scope.FormMode.IsReadOnly = false;
                    $scope.Availability.Status = 1;
                }
            };

            var init = function () {
                $q.all([initCinema(), initButtonByRightOfUser()]).then(function () {
                    initFormData();
                });
            }

            init();

        }]);
    /*
    *  Administrator controller
    *  This controller is used to change user password
    */
    app.controller("AdminChangePassWordCtrl", ["$scope", "$uibModalInstance", 'item', "toastr", "UserApiService", "UtilsService",
        function ($scope, $uibModalInstance, item, $notifications, UserApiService, UtilsService) {

            $scope.Form = {};

            $scope.Form.Title = "Đổi mật khẩu";

            $scope.btnSave = function () {
                var putData = {};
                putData.UserName = item.UserName;
                putData.Password = $scope.PasswordNew;
                putData.PasswordNew = $scope.PasswordNew;
                putData.IsChangePassApplicationSide = true;

                if ($scope.PasswordNew !== $scope.ConfirmedPassword) {
                    UtilsService.OpenDialog('Mật khẩu xác nhận không chính xác!', 'Chú ý', '', 'Đóng', 'md', '');
                    return false;
                }

                var p = UserApiService.ChangePassEmployee(putData);
                p.then(function onSuccess(response) {
                    var dataResult = response.data;
                    $uibModalInstance.close(dataResult);
                });

            };

            /* Button close Popup Form */
            $scope.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };

        }]);
}();
