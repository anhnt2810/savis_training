﻿define(function () {
    'use strict';
    var app = angular.module("SavisApp");
    app.controller('CatalogClientCtrl', ['$scope', '$location', '$filter', '$http', '$uibModal', '$q', '$routeParams', 'UtilsService', '$timeout', '$log', 'toastr', 'CatalogService', 'CatalogItemService', 'MetadataTemplateService',
        function ($scope, $location, $filter, $http, $uibModal, $q, $routeParams, UtilsService, $timeout, $log, $notifications, CatalogService, CatalogItemService, MetadataTemplateService) {

            var catalogItemUrl = '/app-data/views/core/catalog/catalog-item.html';
            var catalogItemClientUrl = '/app-data/views/core/catalog/catalog-client-item.html';

            /*------------------------------------------------------------------**/
            $scope.ListPageSize = [35, 30, 25, 20, 15, 10, 5];
            $scope.Pagination = {};
            $scope.ListData = [];
            $scope.ListSelected = [];
            $scope.SelectAll = false;
            $scope.Mode = 1;
            /* Header grid datatable */
            const header = [
                { Key: 'Order', Value: "STT", Width: '5%', Align: 'left' },
                { Key: 'Code', Value: "Mã", Width: 'auto', Align: 'left' },
                { Key: 'Name', Value: "Tên", Width: 'auto', Align: 'left' },
                { Key: 'OrderCatalog', Value: "Thứ tự sắp xếp", Width: '10%', Align: 'left' },
                { Key: 'Status', Value: "Trạng thái", Width: '100px', Align: 'left' },
                //{ Key: 'Handller', Value: "Thao tác", Width: '15%', Align: 'left' },
            ];
            $scope.Headers = angular.copy(header);

            /* Sorting default*/
            var orderBy = $filter('orderBy');
            $scope.order = function (predicate, reverse) {
                $scope.ListData = orderBy($scope.ListData, predicate, reverse);
            };
            $scope.SelectItem = function (item) {
                if (!item.Selecting) {
                    var index = $scope.ListSelected.indexOf(item);
                    if (index >= 0) {
                        $scope.ListSelected.splice(index, 1);
                    }
                } else {
                    $scope.ListSelected.push(item);
                }
                if ($scope.ListSelected.length === $scope.ListData.length) {
                    $scope.SelectAll = true;
                } else {
                    $scope.SelectAll = false;
                }
            }

            $scope.SelectAllItem = function () {
                if ($scope.ListSelected.length === $scope.ListData.length) {
                    $scope.ListSelected = [];
                    angular.forEach($scope.ListData, function (file) {
                        file.Selecting = false;
                    });
                    $scope.SelectAll = false;
                } else {
                    $scope.ListSelected = [];
                    angular.forEach($scope.ListData, function (file) {
                        file.Selecting = true;
                        $scope.ListSelected.push(file);
                    });
                    $scope.SelectAll = true;
                }
            }
            /* This function is to Check items in grid highlight*/
            $scope.GetItemHeaderClass = function (item) {
                if (item.selected == true) {
                    return 'table-sort-asc';
                } else if (item.selected == false) {
                    return 'table-sort-desc';
                } else {
                    return 'table-sort-both';
                };
            };
            /* Sorting grid By Click to header */
            $scope.ClickToHeader = function (item) {
                angular.forEach($scope.header, function (items) {
                    if (items != item) {
                        items.selected = null;
                    };
                });
                if (item.selected == true) {
                    item.selected = false;
                    $scope.order(item.Key, false);
                } else {
                    item.selected = true;
                    $scope.order("-" + item.Key, false);
                };
            };
            $scope.Button = {};
            $scope.Button.Create = {};
            $scope.Button.Create.Click = function (parent) {
                var modalInstance;
                modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: catalogItemClientUrl,
                    controller: 'CatalogClientItemCtrl',
                    size: 'lg',
                    //windowClass: "modal-full",
                    backdrop: 'static',
                    // Set parameter to childform (popup form)
                    resolve: {
                        item: function () {
                            return null;
                        },
                        option: function () {
                            var obj = {};
                            obj.Mode = "add";
                            obj.Parent = parent;
                            obj.Catalog = $scope.CurentCatalog;
                            return obj
                        },
                    }
                });
                modalInstance.result.then(function (field) {
                    $location.search("pn", $scope.Pagination.TotalPage);
                    $scope.InitView($scope.ViewList);
                }, function (response) { });
            };
            $scope.Button.Update = {};
            $scope.Button.Update.Click = function (item) {
                if (typeof (item) === "undefined") {
                    item = $scope.ListSelected[0];
                }
                var modalInstance;
                var modalInstance;
                modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: catalogItemClientUrl,
                    controller: 'CatalogClientItemCtrl',
                    size: 'lg',
                    //windowClass: "modal-full",
                    backdrop: 'static',
                    // Set parameter to childform (popup form)
                    resolve: {
                        item: function () {
                            return item;
                        },
                        option: function () {
                            var obj = {};
                            obj.Mode = "edit";
                            obj.Catalog = $scope.CurentCatalog;
                            return obj
                        },
                    }
                });

                modalInstance.result.then(function (response) {
                    $scope.InitView($scope.ViewList);
                }, function (response) { });
            };

            $scope.Button.Delete = {};
            $scope.Button.Delete.Click = function (item) {
                var infoResult = UtilsService.OpenDialog('Bạn có chắc chắn muốn xóa bản ghi "' + item.Name + '" không?', 'Xác nhận', 'Đồng ý', 'Hủy', 'sm', null);
                infoResult.result.then(function (modalResult) {
                    if (modalResult == 'confirm') {
                        var promise = CatalogItemService.Delete(item.CatalogItemId);
                        promise.then(function onSuccess(response) {
                            console.log("SUCCESS", response);
                            var responseResult = response.data;
                            $scope.InitView($scope.ViewList);
                            if (responseResult.Status === 1) {
                                if (responseResult.Data.Result) {
                                    $notifications.success("Xóa bản ghi \"" + item.Name + "\"  thành công!", "Thông báo");
                                } else {
                                    $notifications.warning("Xóa thất bại! " + responseResult.Message, "Cảnh báo");
                                }
                            } else {
                                $notifications.error("Xóa thất bại! " + responseResult.Message, "Lỗi");
                            }
                        }, function onError(response) {
                            $notifications.error("Xóa thất bại!", "Thông báo");
                            console.log("ERROR", response);
                        });
                    };
                });
            };
            $scope.Button.DeleteMany = {};
            $scope.Button.DeleteMany.Click = function () {
                var listDeleteting = [];
                angular.forEach($scope.ListData, function (item) {
                    if (item.Selecting) {
                        listDeleteting.push(item.CatalogItemId);
                    }
                });
                var infoResult = UtilsService.OpenDialog('Bạn có chắc chắn muốn xóa những bản ghi này?', 'Xác nhận', 'Đồng ý', 'Hủy', 'sm', null);
                infoResult.result.then(function (modalResult) {
                    if (modalResult == 'confirm') {
                        var promise = CatalogItemService.DeleteMany(listDeleteting);
                        promise.then(function onSuccess(response) {
                            console.log("SUCCESS", response);
                            response = response.data;
                            $scope.ListSelected = [];
                            $scope.InitView($scope.ViewList);
                            if (response.Status === 1) {
                                var listDeleteField = [];
                                angular.forEach(response.Data, function (item) {
                                    if (item.Result == true) {
                                        listDeleteField.push({
                                            Name: item.Name,
                                            Result: "Xóa thành công",
                                            Description: ""
                                        });
                                        //$log.debug(item);
                                    } else if (item.Result == false) {
                                        //$log.debug(item);
                                        listDeleteField.push({
                                            Name: item.Name,
                                            Result: "Xóa thất bại",
                                            Description: item.Description
                                        });
                                    };
                                });

                                var data = {};
                                data.Items = listDeleteField;
                                //todo: Thông báo xóa nhiều
                                //var infoDeleteResult = UtilsService.OpenDialog('Xóa thành công', 'Thông báo', '', 'Đóng', 'md', data);
                            } else {
                                $notifications.warning("Bản ghi đang được sử dụng!", "Cảnh báo");
                            }
                        }, function onError(response) {
                            $notifications.error("Xóa thất bại!", "Cảnh báo");
                            console.log("ERROR", response);
                        });
                    };
                });
            };
            $scope.AddCatalog = function (catalog) {
                var modalInstance;
                modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: catalogItemUrl,
                    controller: 'CatalogItemCtrl',
                    size: 'lg',
                    windowClass: "modal-full",
                    // Set parameter to childform (popup form)
                    resolve: {
                        item: function () {
                            return null;
                        },
                        option: function () {
                            var obj = {};
                            obj.Mode = "add";
                            obj.Parent = catalog;
                            return obj
                        },
                    }
                });
                modalInstance.result.then(function (response) {
                    initCatalogTree();

                }, function (response) { });
            }
            $scope.UpdateCatalog = function (catalog) {
                var modalInstance;
                modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: catalogItemUrl,
                    controller: 'CatalogItemCtrl',
                    size: 'lg',
                    windowClass: "modal-full",
                    backdrop: 'static',
                    // Set parameter to chidform (popup form)
                    resolve: {
                        item: function () {
                            return catalog;
                        },
                        option: function () {
                            var obj = {};
                            obj.Mode = "update";
                            return obj
                        },
                    }
                });
                modalInstance.result.then(function (response) {
                    initCatalogTree();
                }, function (response) { });
            }
            $scope.DeleteCatalog = function (catalog) {
                // var infoResult = UtilsService.OpenDialog(message, 'Xác nhận', 'Đồng ý', 'Hủy', 'sm', null);
                var infoResult = UtilsService.OpenDialog('Bạn có chắc chắn muốn xóa danh mục "' + catalog.Name + '" này không?', 'Xác nhận', 'Đồng ý', 'Hủy', 'sm', null);
                infoResult.result.then(function (modalResult) {
                    if (modalResult == 'confirm') {
                        var promise = CatalogService.Delete(catalog.CatalogMasterId);
                        promise.then(function onSuccess(response) {
                            console.log("SUCCESS", response);
                            var response = response.data;
                            initCatalogTree();
                            if (response.Status === 1) {
                                if (response.Data.Result) {
                                    $notifications.success("Xóa thành công danh mục \"" + catalog.Name, "Thông báo");
                                } else {
                                    $notifications.error("Xóa thất bại! " + response.Message, "Cảnh báo");
                                }
                            } else {
                                $notifications.error("Xóa thất bại! " + response.Message, "Cảnh báo");
                            }
                        }, function onError(response) {
                            $notifications.error("Xóa thất bại!", "Cảnh báo");
                            console.log("ERROR", response);
                        });

                    };
                });
            }
            $scope.GetMenuOption = function (catalog) {
                var addCatalog = ['<i class="fa fa-plus" aria-hidden="true"></i> Thêm danh mục', function ($itemScope) {
                    $scope.AddCatalog();
                }];
                var addChildCatalog = ['<i class="fa fa-plus" aria-hidden="true"></i> Thêm danh mục con', function ($itemScope) {
                    $scope.AddCatalog(catalog);
                }];
                var addCatalogItem = ['<i class="fa fa-plus" aria-hidden="true"></i> Thêm bản ghi cho danh mục', function ($itemScope) {
                    $scope.Button.Create.Click();
                }];
                var updateCatalog = ['<i class="fa fa-edit" aria-hidden="true"></i> Cập nhật danh mục', function ($itemScope) {
                    $scope.UpdateCatalog(catalog);
                }];
                var deleteCatalog = ['<i class="fa fa-trash" aria-hidden="true"></i> Xóa danh mục', function ($itemScope) {
                    $scope.DeleteCatalog(catalog);
                }];
                var result = [];

                result.push(addCatalog);
                result.push(addChildCatalog);
                result.push(addCatalogItem);
                result.push(updateCatalog);
                result.push(deleteCatalog);

                return result;
            }
            $scope.ClickToCategory = function (category) {

                $scope.IsNotReload = true; $scope.IsNotReload = true; $location.search("pn", "1");
                $scope.IsNotReload = true; $scope.IsNotReload = true; $location.search("ps", "10");

                $scope.CurentCatalog.Selected = false;
                $scope.CurentCatalog = category;
                $scope.CurentCatalog.Selected = true;
                //Set default view là dạng list 
                $scope.InitView($scope.ViewList);
                category.IsCollapse = !category.IsCollapse;
            }
            $scope.CollapseCatalog = function (category) {
                category.IsCollapse = !category.IsCollapse;
            }
            $scope.GetMenuOptionContent = function (catalog) {
                var addCatalog = ['<i class="fa fa-plus" aria-hidden="true"></i> Thêm bản ghi', function ($itemScope) {
                    $scope.Button.Create.Click();
                }];
                var addChildCatalog = ['<i class="fa fa-plus" aria-hidden="true"></i> Thêm bản ghi con', function ($itemScope) {
                    $scope.Button.Create.Click(catalog);
                }];
                var updateCatalog = ['<i class="fa fa-edit" aria-hidden="true"></i> Cập nhật bản ghi', function ($itemScope) {
                    $scope.Button.Update.Click(catalog);
                }];
                var deleteCatalog = ['<i class="fa fa-trash" aria-hidden="true"></i> Xóa bản ghi', function ($itemScope) {
                    $scope.Button.Delete.Click(catalog);
                }];
                var result = [];

                result.push(addCatalog);
                result.push(addChildCatalog);
                result.push(updateCatalog);
                result.push(deleteCatalog);

                return result;
            }

            $scope.ClickToCategoryContent = function (category) {
                $scope.CurentCatalogtem.Selected = false;
                $scope.CurentCatalogtem = category;
                $scope.CurentCatalogtem.Selected = true;
                category.IsCollapse = !category.IsCollapse;
            }
            $scope.CollapseCatalogContent = function (category) {
                category.IsCollapse = !category.IsCollapse;
            }

            //p.then(function onSuccess(response) {
            //    console.log("SUCCESS", response);
            //    var responseResult = response.data;
            //    $scope.Grid.Data = responseResult.Data;

            //    //Enable next button
            //    $scope.Grid.TotalCount = responseResult.TotalCount;
            //    $scope.Grid.TotalPage = Math.ceil($scope.Grid.TotalCount / $scope.Grid.PageSize);
            //    $scope.Grid.FromRecord = parseInt(($scope.Grid.PageNumber - 1) * $scope.Grid.PageSize + 1);
            //    $scope.Grid.ToRecord = $scope.Grid.FromRecord + $scope.Grid.Data.length - 1;

            //    $notifications.info("Tải dữ liệu thành công", "Dữ liệu");
            //}, function onError(response) {
            //    console.log("ERROR", response);
            //});

            $scope.InitView = function (isList) {
                if (isList) {
                    $scope.ViewList = true;
                    // catalog hiện tại $scope.CurentCatalog || lấy cấu trúc header 
                    var cmd = MetadataTemplateService.GetMetadataFieldByMetadataTemplateId($scope.CurentCatalog.MetadataTemplateId);
                    cmd.then(function onSuccess(response) {
                        console.log("SUCCESS", response);
                        var responseResult = response.data;
                        var listField = responseResult.Data;
                        $scope.Headers = angular.copy(header);
                        loadListData();
                    }, function onError(response) {
                        console.log("ERROR", response);
                    });
                } else {
                    $scope.ViewList = false;
                    loadTreeData();
                }
            };
            var loadListData = function () {
                var qs = $location.search();
                if (typeof (qs["s"]) !== 'undefined') {
                    $scope.TextSearch = qs["s"];
                } else {
                    $scope.IsNotReload = true; $scope.IsNotReload = true; $location.search("s", "");
                    $scope.TextSearch = "";
                }
                if (typeof (qs["pn"]) !== 'undefined') {
                    $scope.Pagination.PageNumber = parseInt(qs["pn"]);
                    $scope.Pagination.PageNumber = Math.ceil($scope.Pagination.PageNumber);
                    $scope.IsNotReload = true; $scope.IsNotReload = true; $location.search("pn", $scope.Pagination.PageNumber);
                    if ($scope.Pagination.PageNumber <= 0) {
                        $scope.IsNotReload = true; $scope.IsNotReload = true; $location.search("pn", "1");
                        $scope.Pagination.PageNumber = 1;
                    }
                    if ($scope.Pagination.PageNumber > $scope.Pagination.TotalPage && typeof ($scope.Pagination.TotalPage) !== 'undefined') {
                        $scope.IsNotReload = true; $scope.IsNotReload = true; $location.search("pn", $scope.Pagination.TotalPage);
                        if ($scope.Pagination.TotalPage > 0) { $scope.Pagination.PageNumber = $scope.Pagination.TotalPage; }
                    }
                } else {
                    $scope.IsNotReload = true; $scope.IsNotReload = true; $location.search("pn", "1");
                    $scope.Pagination.PageNumber = 1;
                }

                if (typeof (qs["ps"]) !== 'undefined') {
                    $scope.Pagination.PageSize = parseInt(qs["ps"]);
                    if ($scope.Pagination.PageSize <= 0) {
                        $scope.IsNotReload = true; $scope.IsNotReload = true; $location.search("ps", "10");
                        $scope.Pagination.PageSize = 10;
                    }
                } else {
                    $scope.IsNotReload = true; $scope.IsNotReload = true; $location.search("ps", "10");
                    $scope.Pagination.PageSize = 10;
                }



                var postData = {};
                //
                $scope.Pagination.TotalCount = 100;
                postData.PageNumber = $scope.Pagination.PageNumber;
                postData.PageSize = $scope.Pagination.PageSize;
                postData.TextSearch = $scope.TextSearch;
                postData.CatalogMasterId = $scope.CurentCatalog.CatalogMasterId;
                postData.Order = 'CREATE_DATE_ASC';
                postData.Status = true;
                // postData.Order = "ORDER_ASC";
                var promise = CatalogItemService.GetFilter(postData);
                promise.then(function (response) {

                    $log.debug(response);
                    $scope.ListData = [];
                    $scope.Pagination.TotalCount = 0;
                    $scope.Pagination.TotalPage = 0;
                    $scope.Pagination.FromRecord = 0;
                    $scope.Pagination.ToRecord = 0;
                    response = response.data;
                    if (response.Status === 1) {
                        //$scope.sucess("Tải dữ liệu thành công !");
                        $scope.ListData = response.Data;
                        $scope.Pagination.TotalCount = response.TotalCount;
                        $scope.Pagination.TotalPage = Math.ceil($scope.Pagination.TotalCount / $scope.Pagination.PageSize);
                        $scope.Pagination.FromRecord = 0;
                        $scope.Pagination.ToRecord = 0;
                        if ($scope.Pagination.TotalCount !== 0) {
                            $scope.Pagination.FromRecord = Math.ceil(($scope.Pagination.PageNumber - 1) * $scope.Pagination.PageSize + 1);
                            $scope.Pagination.ToRecord = $scope.Pagination.FromRecord + $scope.Pagination.PageSize - 1;
                            if ($scope.Pagination.ToRecord > $scope.Pagination.TotalCount) {
                                $scope.Pagination.ToRecord = $scope.Pagination.TotalCount;
                            }
                        }
                    } else if (response.Status == 0) {
                        $notifications.warning("Không có dữ liệu!");
                    } else {
                        if (response.Status == -2) {
                            $notifications.error("Không tồn tại mã danh mục/ Danh mục không có item!");
                        }
                        else
                            $scope.warning("Không tải được dữ liệu:" + response.Message);
                    }
                }, function (reasonResponse) {
                    $log.debug(reasonResponse);
                    $notifications.error("Không tải được dữ liệu:" + reasonResponse);
                });
                return promise;
            };

            $scope.$on('$routeUpdate', function (e) {
                if (!$scope.IsNotReload) {
                    loadListData();
                } else {
                    $scope.IsNotReload = false;
                }
            });
            var loadTreeData = function () {
                var promise = CatalogService.GetItemTree($scope.CurentCatalog.CatalogMasterId);
                promise.then(function onSuccess(response) {
                    console.log("SUCCESS", response);
                    var responseResult = response.data;
                    if (responseResult.Status != null) {
                        $scope.TreeData = responseResult.Data;
                        CaculateTree();
                    }
                }, function onError(response) {
                    console.log("ERROR", response);
                });
                return promise;
            }

            var initCatalogTree = function () {
                $scope.ViewList = true;
                $scope.CurentCatalog = {};
                $scope.CurentCatalogtem = {};
                var promise = CatalogService.GetTree();
                promise.then(function (response) {
                    var data = response.data;

                    if (data.Data != null) {
                        $scope.ListCatalogData = data.Data;
                        if (typeof ($routeParams.code) !== "undefined" && $routeParams.code !== null) {
                            var postdata = {};
                            postdata.VocabularyCode = $routeParams.code;
                            postdata.Status = true;
                            var itemResult = CatalogService.GetFilter(postdata);
                            itemResult.then(function onSuccess(response) {
                                response = response.data;
                                console.log("SUCCESS", response);
                                if (response.Data != null) {
                                    $scope.ClickToCategory(response.Data[0]);
                                }
                            }, function onError(response) {
                                console.log("ERROR", response);
                            });
                        }
                        else if ($scope.ListCatalogData.length > 0) {
                            $scope.ClickToCategory($scope.ListCatalogData[0])
                        };
                    }
                }, function (reasonResponse) {
                    $log.debug(reasonResponse);
                    //$scope.error("Không tải được dữ liệu :" + reasonResponse);
                });
                return promise;
            };


            /* END REGION : INIT */
            initCatalogTree();
            var CaculateTree = function () {
                $timeout(function () {
                    $("#tree-view").slimScroll({
                        height: "478px",
                    });
                });
            }

            $timeout(function () {
                $("#catalog-tree2").slimScroll({
                    height: "478px",
                });

                $("#mainContent").colResizable({
                    liveDrag: true,
                    draggingClass: "dragging",
                    resizeMode: 'fit',
                    minWidth: 250
                });
            });

            $scope.Button.Reload = {};
            $scope.Button.Reload.Click = function () {
                $scope.IsNotReload = true; $scope.IsNotReload = true; $location.search("s", "");
                $scope.IsNotReload = true; $scope.IsNotReload = true; $location.search("pn", "1");
                $scope.SelectAll = false;
                $scope.ListSelected = [];
                $scope.InitView($scope.ViewList);
            };
            $scope.Button.Search = {};
            $scope.Button.Search.Click = function () {
                $scope.IsNotReload = true; $scope.IsNotReload = true; $location.search("s", $scope.TextSearch);
                $scope.IsNotReload = true; $scope.IsNotReload = true; $location.search("pn", "1");
                loadListData();
            }

            $scope.Button.GoToPage = {};
            $scope.Button.GoToPage.Click = function (pageTogo) {
                $scope.IsNotReload = true; $scope.IsNotReload = true; $location.search("pn", pageTogo);
                $scope.IsNotReload = true; $scope.IsNotReload = true; $location.search("ps", $scope.Pagination.PageSize);
                loadListData();
            };
        }


    ]);
}());
