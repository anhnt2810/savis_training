﻿!function () {
    'use strict';
    var app = angular.module("SavisApp");
    //Todo: bỏ các service ko dùng đến đi: FormlyService, FieldService
    app.controller("CatalogItemCtrl", ["$scope", "$uibModalInstance", "$filter", "item", "option", "$log", "$timeout", '$uibModal', 'CatalogService', 'FieldService',
        'UtilsService', 'toastr', 'FormlyService', 'MetadataTemplateService', 'MetadataFieldService', '$sce',
    function ($scope, $uibModalInstance, $filter, item, option, $log, $timeout, $uibModal, CatalogService, FieldService, UtilsService, $notifications,
         FormlyService, MetadataTemplateService, MetadataFieldService, $sce) {
        //todo: xem lại phần close popup
        $scope.closeAlert = function (item) {
            notifications.pop(item);
        };

        var addMetadataFieldUrl = '/app-data/views/core/catalog/metadata-field/metadata-field-item.html';
        var selectMetadataFieldUrl = '/app-data/views/core/catalog/metadata-template/metadata-field-select.html';
        /*------------------------------------------------------------------**/
        //khai báo các template
        var template = {
            editLabelUrl: '/app-data/views/core/formly/formly-edit-label.html',
            editControlUrl: '/app-data/views/core/formly/formly-edit-control.html',
            editRowUrl: '/app-data/views/core/formly/formly-edit-row.html'
        };

        //khai báo các controller
        var controller = {
            editLabelCtr: 'FormlyEditLabel',
            editControlCtr: 'FormlyEditControl',
            editRowCtr: 'FormlyEditRow'
        };
        /**------------------------------------------------------------------**/

        var defaultParentCatalog = { TermId: null, Name: "---Không có danh mục cha---" };

        /* REGION : INIT */
        /* Init function, this function will be executed on page load */

        var initCatalog = function () {
            // Set page loading 
            var promise = CatalogService.GetAll();
            promise.then(function onSuccess(response) {
                debugger
                console.log("SUCCESS", response);
                response = response.data;
                if (response.Data != null) {
                    $scope.ListCatalogData = response.Data;
                    $scope.ListCatalogData.unshift(defaultParentCatalog);
                }
            }, function onError(response) {
                console.log("ERROR", response);
                $log.error();
                $log.debug(response);
            });
            return promise;
        };
        $scope.ListMode = 1;
        $scope.SubMode = 1;

        //lấy danh sách các filed theo Id
        $scope.ListFormlyMetadataField = [];
        var initFormlyMetadataField = function (id) {
            var promise = MetadataTemplateService.GetMetadataFieldByMetadataTemplateId(id);
            promise.then(function onSuccess(response) {
                var data = response.data;
                debugger
                if (data.Status != null) {
                    console.log("SUCCESS", data);
                    $scope.ListFormlyMetadataField = data.Data;

                    angular.forEach($scope.ListFormlyMetadataField, function (item) {
                        item.FormlyContentObj = [];
                        try {
                            var obj = FormlyService.DeSerializeJSON(item.FormlyContent);
                            item.FormlyContentObj.push(obj);
                        } catch (e) { console.log(e); }
                    });
                }
            }, function onError(response) {
                $notifications.error("Xóa thất bại!", "Lỗi");
                console.log("ERROR", response);
            });
            return promise;
        }

        /* Sorting default*/
        var orderBy = $filter('orderBy');
        /*Formly*/
        $scope.Formly = {};
        $scope.ListSelectedFormly = [];
        $scope.SelectAllFormly = false;
        /* Header grid datatable */
        $scope.HeadersFormly = [
            { Key: 'Name', Value: "Thông tin cơ bản", Width: 'auto', Align: 'left' },
            { Key: '', Value: "Xem trước", Width: '45%', Align: 'left' },
            { Key: 'Handler', Value: "Xử Lý", Width: '150px', Align: 'left' },
        ];
        $scope.orderFormly = function (predicate, reverse) {
            $scope.ListFormlyMetadataField = orderBy($scope.ListFormlyMetadataField, predicate, reverse);
        };
        $scope.SelectItemFormly = function (item) {
            if (!item.Selecting) {
                var index = $scope.ListSelectedFormly.indexOf(item);
                if (index >= 0) {
                    $scope.ListSelectedFormly.splice(index, 1);
                }
            } else {
                $scope.ListSelectedFormly.push(item);
            }
            if ($scope.ListSelectedFormly.length === $scope.ListFormlyMetadataField.length) {
                $scope.SelectAllFormly = true;
            } else {
                $scope.SelectAllFormly = false;
            }
        }
        $scope.SelectAllItemFormly = function () {
            if ($scope.ListSelectedFormly.length === $scope.ListFormlyMetadataField.length) {
                $scope.ListSelectedFormly = [];
                angular.forEach($scope.ListFormlyMetadataField, function (file) {
                    file.Selecting = false;
                });
                $scope.SelectAllFormly = false;
            } else {
                $scope.ListSelectedFormly = [];
                angular.forEach($scope.ListFormlyMetadataField, function (file) {
                    file.Selecting = true;
                    $scope.ListSelectedFormly.push(file);
                });
                $scope.SelectAllFormly = true;
            }
        }
        /* This function is to Check items in grid highlight*/
        $scope.GetItemHeaderClassFormly = function (item) {
            if (item.selected === true) {
                return 'table-sort-asc';
            } else if (item.selected === false) {
                return 'table-sort-desc';
            } else {
                return 'table-sort-both';
            };
        };
        /* Sorting grid By Click to header */
        $scope.ClickToHeaderFormly = function (item) {
            // Set all class header to default
            angular.forEach($scope.ListFormlyMetadataField, function (items) {
                if (items != item) {
                    items.selected = null;
                };
            });
            // Set for item click sorted
            if (item.selected === true) {
                item.selected = false;
                $scope.orderFormly(item.Key, false);
            } else {
                item.selected = true;
                $scope.orderFormly(-item.Key, false);
            };
        };
        $scope.Formly.AddCommonMetadataField = function () {
            for (var i = 0; i < $scope.ListFormlyCommonMetadataField.length; i++) {
                var field = $scope.ListFormlyCommonMetadataField[i];
                var check = false;
                for (var j = 0; j < $scope.ListFormlyMetadataField.length; j++) {
                    if ($scope.ListFormlyMetadataField[j].MetadataFieldId === field.MetadataFieldId) {
                        check = true;
                    }
                }
                if (check) continue;

                field.DisplayCategory = true;
                field.DisplaySearchCategory = true;
                field.DisplayReport = true;
                field.DisplaySearchReport = true;
                $scope.ListFormlyMetadataField.push(field);
            }
        };
        $scope.Formly.AddMetadataField = function () {
            var modalInstance;
            modalInstance = $uibModal.open({
                animation: true,
                templateUrl: addMetadataFieldUrl,
                controller: 'MetadataFieldItemCtrl',
                size: 'lg',
                //windowClass: "modal-full",
                backdrop: 'static',
                // Set parameter to chidform (popup form)
                resolve: {
                    item: function () {
                        return null;
                    },
                    option: function () {
                        var obj = {};
                        obj.Mode = "add";
                        return obj
                    },
                }
            });

            modalInstance.result.then(function (field) {
                try {
                    field.FormlyContentObj = [];
                    var obj = FormlyService.DeSerializeJSON(field.FormlyContent);
                    field.FormlyContentObj.push(obj);
                } catch (e) { console.log(e); }
                $scope.ListFormlyMetadataField.push(field);
            }, function (response) { });
        };
        $scope.Formly.SelectMetadataField = function () {
            var modalInstance;
            modalInstance = $uibModal.open({
                //animation: true,
                templateUrl: selectMetadataFieldUrl,
                controller: 'MetadataFieldSelectCtrl',
                size: 'lg',
                windowClass: "modal-full",
                backdrop: 'static',
                // Set parameter to chidform (popup form)
                resolve: {

                }
            });

            modalInstance.result.then(function (response) {
                for (var i = 0; i < response.length; i++) {
                    var field = response[i];
                    var check = false;
                    for (var j = 0; j < $scope.ListFormlyMetadataField.length; j++) {
                        if ($scope.ListFormlyMetadataField[j].MetadataFieldId === field.MetadataFieldId) {
                            check = true;
                        }
                    }
                    if (check) continue;
                    field.DisplayCategory = true;
                    field.DisplaySearchCategory = true;
                    field.DisplayReport = true;
                    field.DisplaySearchReport = true;
                    $scope.ListFormlyMetadataField.push(field);
                }


            }, function (response) { });
        };
        $scope.Formly.Update = function (item) {
            var modalInstance;
            modalInstance = $uibModal.open({
                animation: true,
                templateUrl: addMetadataFieldUrl,
                controller: 'MetadataFieldItemCtrl',
                size: 'lg',
                //windowClass: "modal-full",
                backdrop: 'static',
                // Set parameter to chidform (popup form)
                resolve: {
                    item: function () {
                        return item;
                    },
                    option: function () {
                        var obj = {};
                        obj.Mode = "update";
                        return obj
                    },
                }
            });

            modalInstance.result.then(function (field) {
                try {
                    field.FormlyContentObj = [];
                    var obj = FormlyService.DeSerializeJSON(field.FormlyContent);
                    field.FormlyContentObj.push(obj);
                } catch (e) { console.log(e); }
                item = angular.extend(item, field);
            }, function (response) { });
        };
        $scope.Formly.Delete = function (item) {
            var infoResult = UtilsService.OpenDialog('Bạn có chắc chắn muốn xóa thông tin này!', 'Xác nhận', 'Đồng ý', 'Hủy', 'sm', null);
            infoResult.result.then(function (modalResult) {
                if (modalResult === 'confirm') {
                    var index = $scope.ListFormlyMetadataField.indexOf(item);
                    if (index >= 0) {
                        $scope.ListFormlyMetadataField.splice(index, 1);
                    }
                };
            });
        };
        $scope.Formly.DeleteMulti = function () {
            var infoResult = UtilsService.OpenDialog('Bạn có chắc chắn muốn xóa những thông tin này!', 'Xác nhận', 'Đồng ý', 'Hủy', 'sm', null);
            infoResult.result.then(function (modalResult) {
                if (modalResult === 'confirm') {
                    angular.forEach($scope.ListSelectedFormly, function (item) {
                        var index = $scope.ListFormlyMetadataField.indexOf(item);
                        if (index >= 0) {
                            $scope.ListFormlyMetadataField.splice(index, 1);
                        }
                    });
                };
            });
        };
        $scope.FormlyButtonSelecting = null
        $scope.Formly.GetSortClass = function (item) {
            if (item === $scope.FormlyButtonSelecting) {
                return "btn-danger";
            } else {
                if ($scope.FormlyButtonSelecting === null) {
                    return "";
                } else {
                    return "btn-primary";
                }
            }
        }
        $scope.Formly.Sort = function (item) {
            if ($scope.FormlyButtonSelecting === null) {
                $scope.FormlyButtonSelecting = item;
            } else {
                if (item === $scope.FormlyButtonSelecting) {
                    $scope.FormlyButtonSelecting = null;
                } else {
                    var indexA = $scope.ListFormlyMetadataField.indexOf(item);
                    var indexB = $scope.ListFormlyMetadataField.indexOf($scope.FormlyButtonSelecting);
                    var temp = $scope.ListFormlyMetadataField[indexA];
                    $scope.ListFormlyMetadataField[indexA] = $scope.ListFormlyMetadataField[indexB];
                    $scope.ListFormlyMetadataField[indexB] = temp;
                    $scope.FormlyButtonSelecting = null;
                }
            }
        }
        $scope.Formly.MoveTo = function (item) {
            var infoResult = UtilsService.OpenInputDialog('Nhập vị trí cần di chuyển: ', 'Di chuyển', 'Đồng ý', 'Hủy', 'sm', 'number');
            infoResult.result.then(function (modalResult) {
                console.log(modalResult);
                var old_index = $scope.ListFormlyMetadataField.indexOf(item);
                var new_index = modalResult - 1;

                if (new_index >= $scope.ListFormlyMetadataField.length) {
                    var k = new_index - $scope.ListFormlyMetadataField.length;
                    while ((k--) + 1) {
                        $scope.ListFormlyMetadataField.push(undefined);
                    }
                }
                $scope.ListFormlyMetadataField.splice(new_index, 0, $scope.ListFormlyMetadataField.splice(old_index, 1)[0]);

            });
        };

        // Design form
        /*START common function-----------------------------------------------------------*/
        //Step 1 
        $scope.deliberatelyTrustDangerousSnippet = function (html) {
            return $sce.trustAsHtml(html);
        };
        $timeout(function () {
            // $("#formly-toolbox").slimScroll({
            //     height: "470px",
            // });
            $("#formly-mainscreen").slimScroll({
                height: "470px",
            });
        });
        var selectingItem = {
            data: {}
        };
        $scope.vm = {};
        $scope.vm.Data = {};
        $scope.vm.Model = [];
        $scope.vm.Scheme = [];
        $scope.component = [];
        $scope.ListControl = [];
        /*Định danh level 1------------------------------------------------------------*/
        var toolbox = {
            "data": {
                "child": [],
                "hasChild": true,
                "level": 0,
                "name": "Công cụ",
                "imageSrc": "/assets/image/control.svg"
            }
        };
        var controlbox = {
            "data": {
                "child": [],
                "hasChild": true,
                "level": 0,
                "name": "Công cụ",
                "imageSrc": "/assets/image/input.svg"
            }
        };
        /*định danh level 2--------------------------------------*/

        var col12Cpm = {
            "data": {
                "type": 3,
                "name": "Cột 100%"
            },
            "wrapper": "layout",

            "className": "col-md-12",
            "fieldGroup": [],
        };
        var col6Cpm = {
            "data": {
                "type": 3,
                "name": "Cột 50%"
            },
            "wrapper": "layout",

            "className": "col-md-6",
            "fieldGroup": [],
        };
        var col4Cpm = {
            "data": {
                "type": 3,
                "name": "Cột 30%"
            },
            "wrapper": "layout",

            "className": "col-md-4",
            "fieldGroup": [],
        };
        var col3Cpm = {
            "data": {
                "type": 3,
                "name": "Cột 25%"
            },
            "wrapper": "layout",

            "className": "col-md-3",
            "fieldGroup": [],
        };
        var textCpm = {
            "className": "col-md-12",
            "data": {
                "name": "Nhãn",
                "type": 2
            },
            "template": "Sửa lại dòng chữ"
        };
        toolbox.data.child.push(textCpm);
        toolbox.data.child.push(col12Cpm);
        toolbox.data.child.push(col6Cpm);
        toolbox.data.child.push(col4Cpm);
        toolbox.data.child.push(col3Cpm);
        $scope.component.push(toolbox);
        //Click to category
        $scope.ClickToCategory = function (item) {
            selectingItem.data.Selected = false;
            selectingItem = item;
            selectingItem.data.Selected = true;
            selectingItem.data.IsExpand = !selectingItem.data.IsExpand;
        };

        //Thêm một dòng
        $scope.AddRow = function () {
            var fieldGroup = [];
            $scope.vm.Model.push({
                "data": {
                    "type": 1,
                    "index": FormlyService.NewGuid(),
                    "name": "Hàng"
                },
                "wrapper": "layout",
                "className": "row",
                "fieldGroup": [],
            });
            var item = angular.copy($scope.vm.Model);
            delete $scope.vm.Model;
            $scope.vm.Model = item;
            initSortableAndDropable();
        }
        //Sửa một row
        $scope.EditRow = function (compoment) {
            var modalInstance;
            modalInstance = $uibModal.open({
                templateUrl: template.editRowUrl,
                controller: controller.editRowCtr,
                size: 'lg',
                // windowClass :"modal-full",
                backdrop: 'static',
                // Set parameter to chidform (popup form)
                resolve: {
                    item: function () {
                        return compoment;
                    },

                }
            });
            modalInstance.result.then(function (response) {
                //2 lớp only
                var index = $scope.vm.Model.indexOf(compoment);
                if (index >= 0) {
                    $scope.vm.Model[index] = response;

                    reloadJqueryObj();
                }
            }, function (response) { });
        };
        var initDraggable = function () {
            debugger
            $timeout(function () {
                try {
                    $(".draggable,.draggable-item").draggable("destroy");
                } catch (e) {
                    // console.log(e);
                }
                $(".draggable,.draggable-item").draggable({
                    revert: "invalid", // when not dropped, the item will revert back to its initial position
                    appendTo: "body",
                    cursorAt: {
                        top: 0,
                        left: 0
                    },
                    cursor: "-webkit-grabbing",
                    //delay: 300,
                    distance: 5, //khoang cach pixel chuot bat buoc phai di truoc khi muon dragg
                    //refreshPositions: true,
                    helper: function () {
                        return '<span class="alert alert-success" > <i class="fa fa-lg fa-plus-circle"></i> </span>'
                        // var item = $(this).clone();
                        // return item;
                    }
                });
            }, 10000);
            //todo: bỏ time out đi
        };
        var initSortableAndDropable = function () {
            $timeout(function () {
                var startposRow;
                var endposRow;
                try {
                    $(".sortablerow").sortable("destroy");
                } catch (e) {
                    // console.log(e);
                }
                $(".sortablerow").sortable({
                    placeholder: "ui-state-highlight",
                    tolerance: "pointer",
                    forcePlaceholderSize: true,
                    stop: function (event, ui) {
                        endposRow = ui.item.index();
                        var b = $scope.vm.Model[endposRow];
                        $scope.vm.Model[endposRow] = $scope.vm.Model[startposRow]
                        $scope.vm.Model[startposRow] = b;
                    },
                    start: function (event, ui) {
                        startposRow = ui.item.index();
                    },
                });
                $(".sortablerow").disableSelection();
                var currentSortListId;
                var startpos;
                var endpos;
                try {
                    $(".sortable").sortable("destroy");
                } catch (e) {
                    // console.log(e);
                }
                $(".sortable").sortable({
                    placeholder: "ui-state-highlight",
                    tolerance: "pointer",
                    forcePlaceholderSize: true,
                    stop: function (event, ui) {
                        currentSortListId = ui.item.parent().attr("id");
                        endpos = ui.item.index();
                        angular.forEach($scope.vm.Model, function (component) {
                            if (component.data.index === currentSortListId) {
                                var a = component.fieldGroup[startpos];
                                component.fieldGroup.splice(startpos, 1);
                                component.fieldGroup.splice(endpos, 0, a);

                            }
                        });
                    },
                    start: function (event, ui) {
                        startpos = ui.item.index();
                    },
                });
                $(".sortable").disableSelection();
                try {
                    $(".droppable").droppable("destroy");
                } catch (e) {
                    //console.log(e);
                }
                $(".droppable").droppable({
                    accept: ".draggable",
                    tolerance: "pointer",
                    over: function (event, ui) {
                        $(this).addClass("hover");
                    },
                    out: function (event, ui) {
                        $(this).removeClass("hover");
                    },
                    drop: function (event, ui) {
                        $(this).removeClass("hover");
                        var index = $(this).attr("id");
                        var putitem = angular.copy(selectingItem);
                        //putitem.key = FormlyService.NewGuid();
                        putitem.data.index = FormlyService.NewGuid();
                        angular.forEach($scope.vm.Model, function (row) {
                            if (row.data.index === index) {
                                row.fieldGroup.push(putitem);
                            }
                        });
                        initSortableAndDropable();
                        $scope.$apply();
                    }
                });

                try {
                    $(".droppable-column").droppable("destroy");
                } catch (e) {
                    //console.log(e);
                }
                $(".droppable-column").droppable({
                    accept: ".draggable-item",
                    tolerance: "pointer",
                    over: function (event, ui) {
                        $(this).addClass("hover");
                    },
                    out: function (event, ui) {
                        $(this).removeClass("hover");
                    },
                    drop: function (event, ui) {
                        $(this).removeClass("hover");
                        var index = $(this).attr("id");
                        angular.forEach($scope.vm.Model, function (row) {
                            angular.forEach(row.fieldGroup, function (column) {
                                if (column.data.index === index) {
                                    if (!Array.isArray(column.fieldGroup)) { column.fieldGroup = []; }
                                    if (column.fieldGroup.length === 0 || column.fieldGroup[0].data !== null) {
                                        selectingItem.data.inserted = true;
                                        var putitem = angular.copy(selectingItem);
                                        //putitem.key = FormlyService.NewGuid();
                                        column.fieldGroup = [];
                                        column.fieldGroup.push(putitem);
                                    }
                                }
                            });
                        });
                        $scope.$apply();
                    }
                });
            });
        };
        var reloadJqueryObj = function () {
            var item = angular.copy($scope.vm.Model);
            delete $scope.vm.Model;
            $scope.vm.Model = item;
            initSortableAndDropable();
        };
        initDraggable();

        //Xóa một dòng
        $scope.DeleteRow = function (compoment) {
            var index = $scope.vm.Model.indexOf(compoment);
            if (index >= 0) {
                $scope.vm.Model.splice(index, 1);
            }
            var item = angular.copy($scope.vm.Model);
            delete $scope.vm.Model;
            $scope.vm.Model = item;
            initSortableAndDropable();
        }
        //sao chép dòng một dòng
        $scope.CloneRow = function (compoment) {
            var index = $scope.vm.Model.indexOf(compoment);
            if (index >= 0) {
                var clone = angular.copy(compoment);
                clone.data.index = FormlyService.NewGuid();
                $scope.vm.Model.splice(index, 0, clone);
            }
            reloadJqueryObj();
        }
        $scope.RowSelecting = null;
        //Đổi vị trí dòng
        $scope.CheckSwapRow = function (compoment) {
            if (compoment === $scope.RowSelecting) {
                return "btn-danger";
            } else {
                if ($scope.RowSelecting === null) {
                    return "";
                } else {
                    return "btn-primary";
                }
            }
        };
        $scope.SwapRow = function (compoment) {

            if ($scope.RowSelecting === null) {
                $scope.RowSelecting = compoment;

            } else {
                if (compoment === $scope.RowSelecting) {
                    $scope.RowSelecting = null;
                } else {
                    var indexA = $scope.vm.Model.indexOf(compoment);
                    var indexB = $scope.vm.Model.indexOf($scope.RowSelecting);
                    var temp = $scope.vm.Model[indexA];
                    $scope.vm.Model[indexA] = $scope.vm.Model[indexB];
                    $scope.vm.Model[indexB] = temp;
                    $scope.RowSelecting = null;
                    var item = angular.copy($scope.vm.Model);
                    delete $scope.vm.Model;
                    $scope.vm.Model = item;
                }
            }

        };
        //Sửa một item
        $scope.EditItem = function (compoment, type) {
            var modalInstance;
            if (type === "label") {
                modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: template.editLabelUrl,
                    controller: controller.editLabelCtr,
                    size: 'lg',
                    // windowClass :"modal-full",
                    backdrop: 'static',
                    // Set parameter to chidform (popup form)
                    resolve: {
                        item: function () {
                            return compoment
                        },

                    }
                });
            } else {
                modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: template.editControlUrl,
                    controller: controller.editControlCtr,
                    size: 'lg',
                    // windowClass :"modal-full",
                    backdrop: 'static',
                    // Set parameter to chidform (popup form)
                    resolve: {
                        item: function () {
                            return compoment
                        },
                        option: function () {
                            var obj = {};
                            obj.listkey = listkey;
                            return obj
                        },
                    }
                });
            }

            modalInstance.result.then(function (response) {
                //2 lớp only
                angular.forEach($scope.vm.Model, function (row) {
                    var index = row.fieldGroup.indexOf(compoment);
                    if (index >= 0) {

                        row.fieldGroup[index] = response;

                        reloadJqueryObj();
                    }
                });
            }, function (response) { });
        };
        //Clone một item
        $scope.CloneItem = function (compoment) {
            //2 lớp only
            angular.forEach($scope.vm.Model, function (row) {
                var index = row.fieldGroup.indexOf(compoment);
                if (index >= 0) {

                    var clone = angular.copy(compoment);
                    clone.key = FormlyService.NewGuid();
                    row.fieldGroup.splice(index, 0, clone);


                    reloadJqueryObj();


                }
            });
        };
        //Xóa một item
        $scope.DeleteItem = function (compoment) {
            //2 lớp only
            try {
                if (Array.isArray(compoment.fieldGroup) && compoment.fieldGroup.length >= 1) {
                    var indexctrl = compoment.fieldGroup[0].data.index;
                    $scope.ListControl[indexctrl].data.inserted = false;
                }
            } catch (e) {
            }

            angular.forEach($scope.vm.Model, function (row) {
                var index = row.fieldGroup.indexOf(compoment);
                if (index >= 0) {
                    row.fieldGroup.splice(index, 1);
                    reloadJqueryObj();
                }
            });
        };
        $scope.DeleteMetadataField = function (compoment) {
            //3 lớp only
            angular.forEach($scope.vm.Model, function (row) {
                angular.forEach(row.fieldGroup, function (column) {
                    if (Array.isArray(column.fieldGroup)) {
                        var index = column.fieldGroup.indexOf(compoment);
                        if (index >= 0) {
                            var indexctrl = compoment.data.index;
                            try {
                                $scope.ListControl[indexctrl].data.inserted = false;
                            } catch (e) { }
                            column.fieldGroup.splice(index, 1);
                            reloadJqueryObj();
                        }
                    }
                });
            });
        };
        //Chuyển form config <=> preview
        $scope.TooglePreview = function (status) {
            $scope.Ispreview = status;
            $scope.vm.Scheme = [];
            $scope.vm.Data = {};
            if ($scope.Ispreview) {
                $scope.vm.Scheme = angular.copy($scope.vm.Model);
            }
        }
        $scope.GoToDesignForm = function () {
            $scope.SubMode = 0;
            if ($scope.ListMode === 0) {
                console.log($scope.Item.TemplateModel.FormConfig);
                console.log($scope.ListFormlyMetadataField);
                //Step1 
                $scope.ListControl = [];

                for (var i = 0; i < $scope.ListFormlyMetadataField.length; i++) {
                    var field = $scope.ListFormlyMetadataField[i];
                    try {
                        var obj = FormlyService.DeSerializeJSON(field.FormlyContent);
                        obj.data.inserted = false;
                        obj.data.index = i;
                        $scope.ListControl.push(obj);
                    } catch (e) { }

                }
                console.log($scope.ListControl);
                $scope.vm.Model = [];
                try {
                    $scope.vm.Model = FormlyService.DeSerializeJSON($scope.Item.TemplateModel.FormConfig);
                } catch (e) { }
                if (!Array.isArray($scope.vm.Model)) { $scope.vm.Model = []; }
                // 3 lớp only
                angular.forEach($scope.vm.Model, function (row) {
                    angular.forEach(row.fieldGroup, function (column) {
                        for (var i = 0; i < $scope.ListControl.length; i++) {
                            var control = $scope.ListControl[i];
                            if (Array.isArray(column.fieldGroup) && column.fieldGroup.length >= 1) {
                                if (column.fieldGroup[0].template === "<loki-index-" + i + ">") {
                                    column.fieldGroup = [];
                                    column.fieldGroup.push(control);
                                    control.data.inserted = true;
                                }
                            }
                        }
                    });
                });
            }
            initDraggable();
            initSortableAndDropable();
        }
        $scope.OutDesignForm = function (isSaveForm) {
            if (isSaveForm) {
                //Step 1 Check 
                for (var i = 0; i < $scope.ListControl.length; i++) {
                    if (!$scope.ListControl[i].data.inserted) {
                        $notifications.error("Vui lòng hoàn tất thiết kế form");
                        return;
                    }
                }
                console.log($scope.vm.Model);
                for (var i = 0; i < $scope.vm.Model.length; i++) {
                    var row = $scope.vm.Model[i];
                    for (var j = 0; j < row.fieldGroup.length; j++) {
                        var column = row.fieldGroup[j];
                        if (Array.isArray(column.fieldGroup) && column.fieldGroup.length >= 1) {
                            try {
                                var index = column.fieldGroup[0].data.index;
                                var objectReplace = {
                                    template: "<loki-index-" + index + ">"
                                }
                                column.fieldGroup = [];
                                column.fieldGroup.push(objectReplace);
                            } catch (e) {

                            }
                        }
                    }
                }
                if ($scope.ListMode === 0) {
                    $scope.Item.TemplateModel.FormConfig = FormlyService.SerializeJSON($scope.vm.Model);
                }
            }
            $scope.SubMode = 1;
        }

        $scope.ChangeList = function (index) {
            if ($scope.SubMode === 0) {
                $scope.warning("Vui lòng hoàn thành thiết kế form");
                return;
            } else {
                $scope.ListMode = index;
            }
        }

        $scope.FormlyButtonSelecting = null
        $scope.Formly.GetSortClass = function (item) {
            if (item === $scope.FormlyButtonSelecting) {
                return "btn-danger";
            } else {
                if ($scope.FormlyButtonSelecting === null) {
                    return "";
                } else {
                    return "btn-primary";
                }
            }
        }
        $scope.Formly.Sort = function (item) {
            if ($scope.FormlyButtonSelecting === null) {
                $scope.FormlyButtonSelecting = item;
            } else {
                if (item === $scope.FormlyButtonSelecting) {
                    $scope.FormlyButtonSelecting = null;
                } else {
                    var indexA = $scope.ListFormlyMetadataField.indexOf(item);
                    var indexB = $scope.ListFormlyMetadataField.indexOf($scope.FormlyButtonSelecting);
                    var temp = $scope.ListFormlyMetadataField[indexA];
                    $scope.ListFormlyMetadataField[indexA] = $scope.ListFormlyMetadataField[indexB];
                    $scope.ListFormlyMetadataField[indexB] = temp;
                    $scope.FormlyButtonSelecting = null;
                }
            }
        }

        var initFormMode = function () {
            debugger
            var cmd = initCatalog();
            $scope.Item = {};
            if (option.Mode === "add") {
                $scope.Item.Status = 1;
                $scope.Item.Order = 0;
                $scope.Item.TemplateModel = {};
                if (typeof (option.Parent) != "undefined" && option.Parent != null) {
                    $scope.Item.ParentTermId = option.Parent.MappedTermId;
                }
                $scope.Mode = "add";
                $scope.FormMode = "Thêm mới";
            } else {
                $scope.Mode = "edit";
                $scope.FormMode = "Cập nhật";
                $scope.Item = angular.copy(item);
                if ($scope.Item.Status)
                    $scope.Item.Status = 1;
                else
                    $scope.Item.Status = 0;
                cmd.then(function () {
                    angular.forEach($scope.ListCatalogData, function (catalog) {
                        if (catalog.TermId === $scope.Item.MappedTermId) {
                            var index = $scope.ListCatalogData.indexOf(catalog);
                            $scope.ListCatalogData.splice(index, 1);
                        }
                    });
                });
                //lấy thông tin template
                var promise = MetadataTemplateService.GetById($scope.Item.MetadataTemplateId);
                promise.then(function onSuccess(response) {
                    response = response.data;
                    console.log("SUCCESS", response);
                    if (response.Status === 1) {
                        $scope.Item.TemplateModel = response.Data;
                        // $scope.success("Thông báo", "Tải dữ liệu thành công");
                    } else {
                        $notifications.error("Tải dữ liệu thất bại", "Thông báo");
                    }
                }, function onError(response) {
                    $notifications.error("Tải dữ liệu thất bại", "Lỗi");
                    console.log("ERROR", response);
                });
                initFormlyMetadataField($scope.Item.MetadataTemplateId);
            }
        }

        $scope.ChangeName = function () {
            //check ky tu dac biet
            //validName($scope.Item.Name);
            if (option.Mode === "add")
                $scope.Item.Code = UtilsService.RemoveVietNamSign($scope.Item.Name);
        };

        $scope.ChangeCatalog = function () {
            initRecordField($scope.Item.ArchiveTypeId);
        }
        $scope.ListMode = 0;
        $scope.SubMode = 1;
        initFormMode();
        angular.isUndefinedOrNull = function (val) {
            return angular.isUndefined(val) || val === null
        }
        $scope.Save = function () {
            debugger
            $scope.MyForm.$setSubmitted();
            if ($scope.MyForm.$invalid) {
                var deferred = $q.defer();
                console.log("$scope.MyForm", $scope.MyForm);
                angular.element("[name='" + $scope.MyForm.$name + "']").find('.ng-invalid:visible:first').focus();
                $notifications.error("Vui lòng  kiểm tra thông tin đã nhập", "Lỗi");
                deferred.resolve('');
                return deferred.promise;
            }
            if (option.Mode === "add") {
                var postData = $scope.Item;
                postData.TemplateModel = {};
                postData.TemplateModel.ListMetadataField = [];
                for (var i = 0; i < $scope.ListFormlyMetadataField.length; i++) {
                    var item = $scope.ListFormlyMetadataField[i];
                    postData.TemplateModel.ListMetadataField.push({
                        "MetadataFieldId": item.MetadataFieldId,
                        "Order": 0,
                        "Type": 0,//0|1 
                    });
                }
                console.log('postData catalog: ', postData);

                var promise = CatalogService.Create(postData);
                promise.then(function onSuccess(response) {
                    response = response.data;
                    if (response.Status === 1) {
                        console.log("SUCCESS", response);
                        $notifications.success("Tạo danh mục thành công!");
                        $uibModalInstance.close(response.Data);
                    }
                    else {
                        if (response.Status === 0) {
                            $notifications.error("Tạo danh mục thất bại! " + response.Message);
                            $('#txtName').focus();
                        }
                        else {
                            console.log("ERROR", response);
                            //TODO;
                            //$scope.error("Tạo danh mục thất bại. Hãy kiểm tra lại dữ liệu!"); $('#txtName').focus(); 
                        }
                    }
                }, function onError(response) {
                    console.log("ERROR", response);
                    $notifications.error("Tạo danh mục thất bại!");
                    $('#txtName').focus();
                });
            } else {
                var putData = {};
                //$scope.Item.ParentTermId = $scope.Item.ParentTermId.TermId;
                var putData = $scope.Item;
                putData.TemplateModel.ListMetadataField = [];
                for (var i = 0; i < $scope.ListFormlyMetadataField.length; i++) {
                    var item = $scope.ListFormlyMetadataField[i];
                    putData.TemplateModel.ListMetadataField.push({
                        "MetadataFieldId": item.MetadataFieldId,
                        "Order": 0,
                        "Type": 0,//0|1 
                    });
                }
                var promise = CatalogService.Update($scope.Item.CatalogMasterId, putData);
                promise.then(function onSuccess(response) {
                    var data = response.data;
                    if (data.Status === 1) {
                        $notifications.success("Cập nhật danh mục thành công!");
                        $uibModalInstance.close(data.Data);
                    } else {
                        if (data.Status === 0)
                        { $notifications.error("Cập nhật danh mục thất bại! " + data.Message); $('#txtName').focus(); }
                        else
                        { $notifications.error("Cập nhật danh mục thất bại. Hãy kiểm tra lại dữ liệu!"); $('#txtName').focus(); }
                    }
                }, function onError(response) {
                    $notifications.error("Cập nhật danh mục thất bại!" + response.status);
                    $('#txtName').focus();
                    console.log("ERROR", response);
                });
            }
        };
        $scope.Cancel = function () {
            $uibModalInstance.dismiss();
        };
        /* Validate Form*/
        $scope.validateForm = {
            validName: false
        };
        var format = /[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]/;
        var validName = function (name) {
            if (typeof name == 'undefined' || name == null || name.trim() === "") {
                $scope.validateForm.validName = true;
                return false;
            }
            else {
                $scope.validateForm.validName = false;
                //    $scope.validateForm.validName = format.test(name);
                //    if (format.test(name)) {
                //        return false;
                //    } else {
                //        return true;
                //    }
            }
        }
    }
    ]);
}();