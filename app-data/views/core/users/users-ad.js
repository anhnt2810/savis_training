﻿!function () {
    'use strict';
    var app = angular.module("SavisApp");
    app.controller("UserADModalController", ["$scope", "$q", "$http", "$uibModalInstance", '$uibModal', 'user', 'options', 'roles', "toastr", 'UtilsService', "UserApiService", "RoleApiService", 'OrganizationApiService', 'applicationId', 'ConstantsApp',
        function ($scope, $q, $http, $uibModalInstance, $uibModal, user, options, roles, $notifications, UtilsService, UserApiService, RoleApiService, OrganizationApiService, applicationId, ConstantsApp) {

            $scope.Form = {};
            $scope.Form.Title = "Thêm mới người dùng từ AD";

            //$scope.word = /^\s*\w*\s*\g*$/;
            $scope.word = /(?!.*[\.\-\_]{2,})^[a-zA-Z0-9\.\-\_]{3,24}$/;
            $scope.options = options;
            $scope.Select = {};
            var defaultOrgani = { OrganizationId: null, Name: "--Chọn đơn vị/phòng ban--" };
            var defaultChucVu = { CatalogItemId: null, Name: "--Chọn chức vụ--" };
            /* Declare constant domain and path to Api */
            $scope.Autofocus = true;
            $scope.isEdit = false;
            $scope.IsInfo = false;
            /* assign parameters passed from Form Parent (programmanager.html) or controller (app)*/
            $scope.CurrentUser = user;
            if ($scope.CurrentUser != null) {
                $scope.selected = {
                    item: $scope.CurrentUser[0]
                };
            };

            $scope.items = {};

            $scope.readOnly = false;

            /* Define the form type : add/edit/info */
            var formType = $scope.options[0].Type;

            /*
            * System roles
            */
            $scope.Roles = roles;
            $scope.SelectedRoles = [];
            $scope.CurrentPhongBan = {};

            var initRolesOfCurrentUser = function () {

                var promise = UserApiService.GetRolesByUser($scope.CurrentUser.UserId, applicationId);
                promise.then(function (response) {
                    var userRoles = response.data.Data.Roles;

                    angular.forEach(userRoles, function (userRole) {
                        $scope.SelectedRoles.push(userRole.RoleCode);
                    });
                });

                return promise;
            };

            var initOrganizations = function () {
                var promise = OrganizationApiService.GetTree();
                promise.then(function (response) {
                    $scope.DanhSachNhom = response.data.Data;
                    $scope.DanhSachNhom.unshift(defaultOrgani);
                    $scope.PhongBan = angular.copy($scope.DanhSachNhom);

                    if ($scope.options[0].Type === 'edit') {
                        var position = null;
                        for (var i = 0; i < $scope.CurrentUser.length; i++) {
                            if ($scope.CurrentUser[i].UserId === $scope.options[0].id) {
                                position = i;
                                break;
                            };
                        };
                        if (position != null) {
                            angular.forEach($scope.DanhSachNhom, function (item) {
                                if (item.OrganizationId === items[position].OrganizationId) {
                                    $scope.Select.DanhSachNhomOp = item;
                                }
                            });
                        }
                    } else if ($scope.options[0].Type === 'add') $scope.Select.DanhSachNhomOp = $scope.DanhSachNhom.filter(function (pb) { return pb.Code === "BETA_HO"; })[0];
                });

                return promise;
            };

            var initFormData = function () {
                var organizations = initOrganizations();
                if ($scope.options[0].Type === "add") {
                    $scope.Form.Title = "Thêm mới người dùng từ AD";
                    $scope.Active = true;
                    //Select first item            
                }

            };

            $scope.Availability = {};
            $scope.Availability.Status = 0;

            /* Check availability function */
            var isADExist = false;
            var checkUserADExist = function () {
                debugger
                var p = UserApiService.GetUserADInfo($scope.UserName);
                p.then(function onSuccess(response) {
                    var responseResult = response.data;
                    if (responseResult.Status === 1) {
                        isADExist = true;
                        $scope.FullName = responseResult.Data.FullName;
                        $scope.Email = responseResult.Data.EmailAddress;
                    } else $notifications.warning("Người dùng AD không tồn tại", "Cảnh báo");
                }, function onError(response) {
                    $notifications.warning("Người dùng AD không tồn tại", "Cảnh báo");
                });
                return p;
            };
            $scope.CheckNameAvailability = function (name) {
                debugger
                var checkAd = checkUserADExist();
                $q.all([checkAd]).then(function () {
                    if (!isADExist) return isADExist;
                    else {
                        var p = UserApiService.CheckNameAvailability(name);
                        p.then(function onSuccess(response) {
                            if (response.data.Status === -1) {
                                $scope.Availability.Status = -1;
                                $notifications.warning("Người dùng đã tồn tại trong hệ thống", "Cảnh báo");
                                return false;
                            }
                            if (isADExist) $scope.Availability.Status = 1;
                            return true;
                        });
                    }
                });
            };

            var addUser = function () {
                var postData = {};
                postData.ApplicationId = applicationId;
                postData.UserOrganization = {};
                postData.UserOrganization.OrganizationId = (typeof $scope.Select.DanhSachNhomOp != 'undefined' && $scope.Select.DanhSachNhomOp != null) ? $scope.Select.DanhSachNhomOp.OrganizationId : null;
                postData.UserOrganization.CreatedByUserId = app.CurrentUser.Id;
                postData.UserOrganization.LastModifiedByUserID = app.CurrentUser.Id;
                postData.UserName = ($scope.UserName + "").replace(/(?:\s)\s/g, '');
                postData.FullName = ($scope.FullName + "").replace(/(?:\s)\s/g, '');
                postData.Mobile = $scope.Mobile;
                postData.Email = $scope.Email;
                postData.CreatedByUserId = app.CurrentUser.Id;
                postData.ModifiedByUserId = app.CurrentUser.Id;
                postData.Password = ConstantsApp.PasswordDefault;
                postData.Roles = [];
                postData.Active = $scope.Active;
                postData.Type = 0;
                postData.NickName = ($scope.NickName + "").replace(/(?:\s)\s/g, '');
                postData.Avatar = "assets/layout4/img/icon-user-default.png";
                postData.ChucVuId = (typeof $scope.Select.ChucVuOp != "undefined" && $scope.Select.ChucVuOp != null) ? $scope.Select.ChucVuOp.CatalogItemId : null;
                // Process selected roles
                angular.forEach($scope.SelectedRoles, function (selectedRoleCode) {
                    angular.forEach($scope.Roles, function (role) {
                        if (selectedRoleCode === role.RoleCode) {
                            // Add this selected role to putData
                            postData.Roles.push(role);
                        }
                    });
                });

                var p = UserApiService.AddNew(postData);
                p.then(function onSuccess(response) {
                    var dataResult = response.data;
                    if (dataResult.Status === 1) {
                        $uibModalInstance.close(dataResult);
                    } else if (dataResult.Status === 0 && dataResult.Message === "Code exist") {
                        $notifications.warning("Mã người dùng đã tồn tại", "Cảnh báo");
                    } else {
                        $notifications.error("Thêm mới thất bại", "Lỗi");
                    }
                });
            };

            /* Function button "Save" with add and edit button*/
            $scope.Save = function () {
                $scope.user_form.$setSubmitted();

                if ($scope.user_form.$invalid) {
                    angular.element("[name='" + $scope.user_form.$name + "']").find('.ng-invalid:visible:first').focus();
                    UtilsService.OpenDialog('Vui lòng kiểm tra thông tin đã nhập!', 'Chú ý', '', 'Đóng', 'md', '');
                    return false;
                }

                $scope.UserName = ($scope.UserName + "").replace(/(?:\s)\s/g, '');
                if ($scope.UserName === "undefined")
                    $scope.UserName = "";

                var checkName = UserApiService.CheckNameAvailability($scope.UserName);
                checkName.then(function onSuccess(response) {
                    if (response.data.Status === -1) {
                        UtilsService.OpenDialog("Tên tài khoản đã tồn tại, vui lòng chọn tên khác!", "Chú ý", '', "Đóng", "md", '');
                        return false;
                    }

                    /* Check the two inserted password */
                    if ($scope.items.Password !== $scope.items.ConfirmedPassword) {
                        UtilsService.OpenDialog('Mật khẩu xác nhận không chính xác!', 'Chú ý', '', 'Đóng', 'md', '');
                        return false;
                    }

                    if ($scope.Select.DanhSachNhomOp.OrganizationId === null) {
                        UtilsService.OpenDialog("Bạn cần chọn đơn vị/phòng ban!", 'Chú ý', '', 'Đóng', 'md', '');
                        return false;
                    }

                    // Add user
                    addUser();

                    return true;
                });
            };

            //SelectAllRole
            $scope.SelectAllRole = function () {
                ////angular.forEach($scope.Roles, function (rl) {
                ////    $scope.SelectedRoles.push(rl);
                ////});
                //$scope.SelectedRoles = $scope.Roles;
            };

            /* Button close Popup Form */
            $scope.Cancel = function () {
                $uibModalInstance.dismiss();
            };

            /* Form data initiate */
            initFormData();

        }]);
}();
