﻿define(['angularAMD'], function (angularAMD) {
    'use strict';
    angularAMD.controller('HeaderController', ['$scope', '$q', '$rootScope', '$uibModal', 'ngAuthController', 'SiteApiService', '$http', 'ConstantsApp', 'UtilsService',
        function ($scope, $q, $rootScope, $uibModal, ngAuthController, SiteApiService, $http, ConstantsApp, UtilsService) {
            $scope.$on('$includeContentLoaded', function () {
                Layout.initHeader(); // init header
            });
            $scope.User = {};
            /* Declare variables url form popup add,edit,info */
            var itemDialogTemplateUrl = '/app-data/views/template/header/update-pin.html';

            /* DROPDOWNLIST FUNCTIONS */
            /* ------------------------------------------------------------------------------- */
            $scope.DropDownList = {};

            $scope.DropDownList.Site = { Data: [], Model: null, Disabled: false };
            $scope.DropDownList.Site.SelectedChange = function () {
                var appSelectedId = $scope.DropDownList.Site.Model.SiteId;
                window.localStorage['site_id'] = appSelectedId;
                window.localStorage['site_name'] = $scope.DropDownList.Site.Model.SiteName;
                window.location = "/";
                //initUsersDetails();
            };

            /* ------------------------------------------------------------------------------- */

            /* BUTTON */
            /* ------------------------------------------------------------------------------- */
            $scope.Button = {};

            // LOGOUT
            $scope.Button.LogOut = {};
            $scope.Button.LogOut.Click = function () {
                var postData = {
                    FullName: window.localStorage['display_name'],
                    Object: "Hệ thống",
                    Description: "Đăng xuất thành công khỏi phần mềm",
                    Action: "LOGOUT"
                };

                var promise = $http({
                    method: 'POST',
                    url: ConstantsApp.BASED_API_URL + ConstantsApp.API_SYSTEM_TRACKING_URL,
                    headers: {
                        'Content-type': ' application/json'
                    },
                    data: postData
                });
                promise.then(function onSuccess(response) {
                    var responseResult = response.data;
                    if (responseResult.Status === 1) {
                        ngAuthController.logOut();
                    } else console.log("Tracking logout error", response);
                }, function onError(response) { console.log("Tracking logout error", response); });

            };

            // WEBSITE
            $scope.Button.GoToWebsite = {};
            $scope.Button.GoToWebsite.Click = function () {
                var websiteUrl = ConstantsApp.BASED_WEBSITE_ADMIN_URL;
                window.open(websiteUrl, "_blank");
            };

            // Open Update PIN
            $scope.Button.UpdatePIN = {};
            $scope.Button.UpdatePIN.Click = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: itemDialogTemplateUrl,
                    controller: 'HeaderModalController',
                    size: 'md',
                    keyboard: false,
                    backdrop: 'static',
                    resolve: {
                        item: function () {
                            var obj = {};
                            return obj;
                        },
                        option: function () {
                            return [{ Type: 'add' }];
                        }
                    }
                });
                modalInstance.result.then(function (response) {
                    if (response.Status === 1) {
                        //$notifications.success('Thêm mới thành công', 'Thông báo');
                    };
                }, function () {
                    // $log.info('Modal dismissed at: ' + new Date());
                });
            };

            /* ------------------------------------------------------------------------------- */
            var getAppVersion = function () {
                var isCheckedVersion = window.localStorage["isCheckedVersion"];
                if (isCheckedVersion !== "1") {
                    window.localStorage["isCheckedVersion"] = "1";
                    var promise = SiteApiService.GetAppVersion();
                    promise.then(function onSuccess(response) {
                        var responseResult = response.data;
                        if (responseResult.Status === 1) {
                            var version = "?bust=v" + responseResult.Data[0].Name;
                            if (version !== app.Version) window.location.reload();
                        }
                    });
                    return promise;
                }
            };

            var initSiteAccessible = function () {
                var promise = SiteApiService.GetAccessibleSite();
                promise.then(function onSuccess(response) {
                    var responseResult = response.data;
                    if (responseResult !== null && responseResult.Status === 1) {
                        $scope.DropDownList.Site.Data = responseResult.Data;
                    }
                    else {
                        // Không có site được phép truy cập đưa ra thông báo
                        var message = "Tài khoản của bạn chưa được phân quyền truy cập vào hệ thống. Liên hệ Quản trị hệ thống để được hỗ trợ !";
                        var deniedResult = UtilsService.OpenDialog(message, 'Thông báo', 'Xác nhận', '', 'md', '');

                        deniedResult.result.then(function (modalResult) {
                            if (modalResult === 'confirm') {
                                ngAuthController.logOut();
                            }
                        });
                    }
                }).catch(function (response) {
                    $log.error();
                    $log.debug(response);
                });
                return promise;
            };

            var loadUserInfo = function () {
                var displayName = window.localStorage["display_name"];
                if (displayName !== "" && typeof displayName !== "undefined") {
                    $scope.User.FullName = displayName;
                    return $scope.User;
                } else {
                    var promise = $http({ method: 'GET', url: ConstantsApp.BASED_API_URL + ConstantsApp.API_USER_URL + "/userinfo" });
                    promise.then(function onSuccess(response) {
                        var resultResponse = response.data;
                        if (resultResponse.Status === 1) {
                            $scope.User = resultResponse.Data;
                        }
                    });
                    return promise;
                }
            };

            var init = function () {
                getAppVersion();
                var appId = window.localStorage["site_id"];
                if (appId !== null && typeof appId !== 'undefined' && appId !== 'undefined' && $scope.DropDownList.Site.Data.length === 0) {
                    var cmd = initSiteAccessible();
                    var cmd2 = loadUserInfo();
                    $q.all([cmd, cmd2]).then(function () {
                        $scope.DropDownList.Site.Model = $scope.DropDownList.Site.Data.filter(function (st) { return st.SiteId === appId })[0];
                        if ($scope.DropDownList.Site.Model === null || typeof $scope.DropDownList.Site.Model === "undefined") {
                            $scope.DropDownList.Site.Model = $scope.DropDownList.Site.Data[0];
                            $scope.DropDownList.Site.SelectedChange();
                        }
                        if ($scope.DropDownList.Site.Data.length === 1) $scope.DropDownList.Site.Disabled = true;
                        else $scope.DropDownList.Site.Disabled = false;
                    });
                }
            };

            init();

        }]);
    angularAMD.controller('HeaderModalController', ['$scope', '$uibModalInstance', '$http', 'item', 'option', 'ConstantsApp', 'UtilsService', 'UserApiService',
        function ($scope, $uibModalInstance, $http, item, option, ConstantsApp, UtilsService, UserApiService) {

            /* FORM */
            /* ------------------------------------------------------------------------------- */
            $scope.Form = {
                IsAdd: true, IsUpdate: false, Item: {}, IsConfirmValid: true, IsCorrectOldPIN: true
            };
            /* ------------------------------------------------------------------------------- */

            /* TEXTBOX */
            /* ------------------------------------------------------------------------------- */
            $scope.TextBox = {
                ConfirmPIN: {
                    TextChanged: function () {
                        if ($scope.PINNew !== $scope.ConfirmedPIN) {
                            $scope.Form.IsConfirmValid = false;
                        } else $scope.Form.IsConfirmValid = true;
                    }
                },
                OldPIN: {
                    TextChanged: function () {
                        $scope.Form.IsCorrectOldPIN = true;
                    }
                }
            };
            /* ------------------------------------------------------------------------------- */

            /* BUTTON */
            /* ------------------------------------------------------------------------------- */
            $scope.Button = {};

            $scope.Button.Save = {};
            $scope.Button.Save.Click = function () {
                if ($scope.PINNew !== $scope.ConfirmedPIN) {
                    $scope.Form.IsConfirmValid = false;
                } else {
                    
                    var putData = {};
                    putData.NewPIN = $scope.PINNew;
                    if ($scope.Form.IsUpdate) putData.OldPIN = $scope.PINOld;
                    var promise = UserApiService.UpdatePIN(putData);
                    promise.then(function onSuccess(response) {
                        var resultResponse = response.data;
                        if (resultResponse.Status === 1) $uibModalInstance.close(resultResponse);
                        else $scope.Form.IsCorrectOldPIN = false;
                    });
                }

                //var dataResult = { Status: 1 };
                //$uibModalInstance.close(dataResult);
            };

            $scope.Button.Close = {
                Click: function () {
                    $uibModalInstance.dismiss('cancel');
                }
            };

            /* ------------------------------------------------------------------------------- */
            var loadUserInfo = function () {
                var promise = $http({ method: 'GET', url: ConstantsApp.BASED_API_URL + ConstantsApp.API_USER_URL + "/userinfo" });
                promise.then(function onSuccess(response) {
                    var resultResponse = response.data;
                    if (resultResponse.Status === 1) {
                        $scope.User = resultResponse.Data;
                        if ($scope.User.PIN !== null) {
                            $scope.Form.Title = "Cập nhật PIN";
                            $scope.Form.IsAdd = false;
                            $scope.Form.IsUpdate = true;
                        }
                        else $scope.Form.Title = "Thêm mới PIN";
                    }
                });
                return promise;
            };

            var init = function () {
                loadUserInfo();
            };

            init();

        }]);
});