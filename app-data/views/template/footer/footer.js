﻿define(['jquery', 'angularAMD'
], function (jQuery, angularAMD, authConfig) {
    'use strict';

    angularAMD.controller('FooterController', ['$scope', function ($scope) {
        $scope.$on('$includeContentLoaded', function () {
            Layout.initFooter(); // init footer
        });
    }]);
});