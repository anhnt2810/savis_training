﻿!function () {
    'use strict';
    var app = angular.module("SavisApp");
    app.controller('NavigationInstance', ['$scope', '$timeout', '$location', '$filter', '$http', '$uibModal', 'UtilsService', 'ConstantsApp', 'NavigationApiService', 'toastr', 'items', 'options', '$uibModalInstance',
        function ($scope, $timeout, $location, $filter, $http, $uibModal, UtilsService, ConstantsApp, NavigationApiService, $notifications, items, options, $uibModalInstance) {
            var apiUrl = Constants.ApiUrl;

            console.log(items);
            var Roles = [];
            $scope.Roles = [];
            $scope.items = items.modalItem;
            console.log(options);
            $scope.TitleForm = "Cập nhật Admin Menu";

            var LoadAllRoles = function () {
                $http({
                    method: 'GET',
                    url: Constants.ApiUrl + 'api/system/roles'
                })
                .success(function (data) {
                    if (data != null) {

                        $scope.ListRoleSelected = [];
                        Roles = data.Data;

                        angular.forEach(Roles, function (rl) {
                            angular.forEach($scope.items.RoleList, function (rl2) {
                                if (rl.RoleId == rl2) {
                                    rl.selected = true;
                                }
                            });
                        });

                        angular.forEach(Roles, function (rl) {
                            if (rl.selected == true) {
                                $scope.Roles.unshift(rl);
                                $scope.ListRoleSelected.unshift(rl);
                            } else {
                                $scope.Roles.push(rl);
                            }
                        });

                    }
                }).error(function (response) {
                    $log.error();
                    $log.debug(response);
                });
            };
            LoadAllRoles();

            $scope.SelectRole = function () {
                $scope.ListRoleSelected = [];
                angular.forEach(Roles, function (rl) {
                    if (rl.selected == true) {
                        $scope.ListRoleSelected.unshift(rl);
                    }
                });
            };

            $scope.SelectAllRole = function () {
                $scope.ListRoleSelected = [];
                angular.forEach(Roles, function (rl) {
                    rl.selected = $scope.checkall;
                    if (rl.selected == true) {
                        $scope.ListRoleSelected.unshift(rl);
                    }
                });
            };

            /* Function button "Save" with add and edit button*/
            $scope.Save = function () {
                //$log.debug(putData);
                var postData = {};
                $scope.items.RoleList = [];
                angular.forEach($scope.Roles, function (rl) {
                    if (rl.selected == true) {
                        $scope.items.RoleList.push(rl.RoleId);
                    };
                });

                //$scope.items.ApplicationID = app.CurrentUser.ApplicationId;

                postData = $scope.items;

                var promise = $http({
                    method: 'POST',
                    url: apiUrl + 'api/navigation',
                    headers: {
                        'Content-type': ' application/json'
                    },
                    data: postData
                }).success(function (response) {
                    $uibModalInstance.close(response);
                });
                return promise;
            };
            /* Button close Popup Form */
            $scope.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };

        }]);
}();
