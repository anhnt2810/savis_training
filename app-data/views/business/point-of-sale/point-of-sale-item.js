﻿!function () {
    'use strict';
    var app = angular.module("SavisApp");
    app.controller('CreatePointOfSaleController', ["$scope", "$q", "$uibModalInstance", '$timeout', 'Upload', 'UtilsService', 'ConstantsApp', "item", "option", "toastr", "CinemaApiService", "PointOfSaleApiService",
        function ($scope, $q, $uibModalInstance, $timeout, Upload, UtilsService, ConstantsApp, item, option, $notifications, CinemaApiService, PointOfSaleApiService ) {

            $scope.Form = {};
            $scope.Filter = {};
            $scope.ModalMode = '';

            $scope.Form.Title = "";
            $scope.PosModel = {
                PosId: '',
                Code: '',
                Name: '',
                MACAddress: '',
                Description: '',
                Status: true,
                ActionPermission: '',
                GhiNhanBan: true,
                GhiNhanMua: true,
                CinemaId: ''
            };

            $scope.Save = function () {
                if (option.Type === 'add') {
                    var postData = {
                        Code: $scope.PosModel.Code,
                        Name: $scope.PosModel.Name,
                        MACAddress: $scope.PosModel.MACAddress,
                        Description: $scope.PosModel.Description,
                        Status: $scope.PosModel.Status,
                        ActionPermission: $scope.PosModel.ActionPermission,
                        CinemaId: $scope.PosModel.CinemaId,
                    }
                    var request = PointOfSaleApiService.Add(postData);
                    request.then(function onSuccess(response) {
                        var dataResult = response.data;
                        if (dataResult.Status === 1) {
                            $uibModalInstance.close(dataResult);
                        } else if (dataResult.Status === 0) {
                            $notifications.warning('Mã POS đã tồn tại', 'Cảnh báo');
                        } else {
                            $notifications.error("Thêm mới thất bại", "Lỗi");
                        }
                    }, function onError(response) {
                        console.log(response);
                    });
                }
                else if (option.Type === 'edit') {
                    var posId = $scope.PosModel.PosId;
                    var putData = {
                        Code: $scope.PosModel.Code,
                        Name: $scope.PosModel.Name,
                        MACAddress: $scope.PosModel.MACAddress,
                        Description: $scope.PosModel.Description,
                        Status: $scope.PosModel.Status,
                        ActionPermission: $scope.PosModel.ActionPermission,
                        CinemaId: $scope.PosModel.CinemaId,
                    }
                    console.log('MACAddress ' + $scope.PosModel.MACAddress);
                    var request = PointOfSaleApiService.Update(posId, putData);
                    request.then(function onSuccess(response) {
                        console.log(response);
                        var dataResult = response.data;
                        if (dataResult.Status === 1) {
                            $uibModalInstance.close(dataResult);
                        }
                        else {
                            $notifications.error("Sửa thông tin POS thất bại", "Lỗi");
                        }
                    }, function onError(response) {
                        console.log(response);
                    });
                }
            }

            $scope.Button = {};

            // Button Close Modal
            $scope.Button.Close = {};
            $scope.Button.Close.Click = function() {
                $uibModalInstance.dismiss('cancel');
            };

            //Chon Rap Chieu
            $scope.ChooseRapChieu = function () {
                if ($scope.Filter.CinemaSelected != null) {
                    $scope.PosModel.CinemaId = $scope.Filter.CinemaSelected.CinemaId;
                }
                else {
                    $scope.PosModel.CinemaId = '';
                }
            }
            // Serialize ActionPermission
            $scope.SerializeActionPermission = function () {
                if ($scope.PosModel.GhiNhanBan && $scope.PosModel.GhiNhanMua) {
                    $scope.PosModel.ActionPermission = '["GHI_NHAN_BAN","GHI_NHAN_MUA"]';
                }
                else if ($scope.PosModel.GhiNhanBan) {
                    $scope.PosModel.ActionPermission = '["GHI_NHAN_BAN"]';
                } else if ($scope.PosModel.GhiNhanMua) {
                    $scope.PosModel.ActionPermission = '["GHI_NHAN_MUA"]';
                }
                else {
                    $scope.PosModel.ActionPermission = '[]';
                }
            }

            var initMain = function () {
                $scope.ModalMode = option.Type;
                if (option.Type === 'add') {
                    $scope.Form.Title = "THÊM MỚI ĐIỂM BÁN HÀNG (POS)";
                }
                else if (option.Type === 'edit') {
                    $scope.Form.Title = "CẬP NHẬT ĐIỂM BÁN HÀNG (POS)";
                    $scope.PosModel = {
                        PosId: item.POSId,
                        Code: item.Code,
                        Name: item.Name,
                        MACAddress: item.MACAddress,
                        Description: item.Description,
                        Status: item.Status,
                        ActionPermission: item.ActionPermission,
                        GhiNhanBan: item.ActionPermission.indexOf('GHI_NHAN_BAN') > -1,
                        GhiNhanMua: item.ActionPermission.indexOf('GHI_NHAN_MUA') > -1,
                        CinemaId: item.CinemaId
                    };
                }
                $scope.SerializeActionPermission();
                initDanhSachRap(); 
            }

            var initDanhSachRap = function () {
                var request = PointOfSaleApiService.GetAllCinemaActive();
                request.then(function onSuccess(response) {
                    $scope.Filter.DanhSachRap = response.data.Data;
                    if (option.Type === 'edit') {
                        for (var index = 0; index < $scope.Filter.DanhSachRap.length; index++) {
                            if (item.CinemaId === $scope.Filter.DanhSachRap[index].CinemaId) {
                                $scope.Filter.CinemaSelected = $scope.Filter.DanhSachRap[index];
                            }
                        }
                    }
                }, function onError(response) {
                    console.log("ERROR", response);
                });
            }

            initMain();

        }]);
}();