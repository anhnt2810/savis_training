﻿define(['angularAMD'], function (angularAMD) {
    'use strict';
    var sidebar = {};

    sidebar.init = function (app) {
        app.controller('SidebarController', ['$state', '$rootScope', '$scope', '$q', '$timeout', 'NavigationApiService', 'UtilsService', function ($state, $rootScope, $scope, $q, $timeout, NavigationApiService, UtilsService) {
            //$scope.$on('$includeContentLoaded', function () {
            //    Layout.initSidebar($state);
            //    Layout.setSidebarMenuActiveLink("match"); // init sidebar
            //});

            $scope.Navigations = { Data: [] };

            var listStateOfUser = [];
            // Đệ quy để lấy được tất cả state được phân quyền cho người dùng
            var addStateFromNavigation = function (listNav) {
                listNav.forEach(function (nav) {
                    listStateOfUser.push({ State: nav.Code, Url: nav.UrlRewrite });
                    if (nav.SubRight.length > 0) addStateFromNavigation(nav.SubRight);
                })
            };

            var initData = function () {
                var promise = NavigationApiService.GetNavByUserId(app.CurrentUser.Id);
                promise.then(function onSuccess(response) {
                    var responseResult = response.data;
                    if (responseResult.Status === 1) {
                        $scope.Navigations.Data = responseResult.Data;
                        $timeout(function () {
                            Layout.initSidebar($state);
                            Layout.setSidebarMenuActiveLink("match");
                        });
                    }
                });
                return promise;
            };

            var init = function () {                
                var deferred = $q.defer();
                $q.all([initData()]).then(function (response) {
                    if ($scope.Navigations.Data.length > 0) {
                        addStateFromNavigation($scope.Navigations.Data);
                        UtilsService.User.ListStates = listStateOfUser;
                    }
                    deferred.resolve("");
                    //$('#sidebar-scrollable').slimScroll({
                    //    height: '55vh'
                    //});
                });
                return deferred.promise;
            };

            init();
        }]);
    };

    return sidebar;
});