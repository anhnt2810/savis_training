﻿!function () {
    'use strict';
    var app = angular.module("SavisApp");
    app.controller('PointOfSaleController', ['$scope', '$location', '$filter', '$uibModal', '$q', 'UtilsService', 'ConstantsApp', 'CinemaApiService', 'PointOfSaleApiService', 'toastr',
        function ($scope, $location, $filter, $uibModal, $q, UtilsService, ConstantsApp, CinemaApiService, PointOfSaleApiService, $notifications) {

            var appVersion = app.Version;
            $scope.listPromise = [];

            $scope.RapChieuPhim = {};
            $scope.Filter = {};
            $scope.Filter.Text = "";

            /* Declare variables url form popup add,edit,info */
            var dialogTemplateUrl = '/app-data/views/business/point-of-sale/point-of-sale-item.html' + appVersion;


            /* ------------------------------------------------------------------------------- */

            /* FORM */
            /* ------------------------------------------------------------------------------- */
            $scope.Form = {};

            $scope.FormMode = {};
            $scope.FormMode.IsReadMode = false;
            $scope.FormMode.IsAllowDelete = false;

            /* ------------------------------------------------------------------------------- */


            /* BUTTONs */
            /* ------------------------------------------------------------------------------- */
            $scope.Button = {};
            // Button ADD
            $scope.Button.Add = { Enable: true, Visible: true, Title: "Thêm mới", GrantAccess: true };
            $scope.Button.Add.Click = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: dialogTemplateUrl,
                    controller: 'CreatePointOfSaleController',
                    size: 'lg',
                    keyboard: false,
                    backdrop: 'static',
                    resolve: {
                        item: {},
                        option: {
                            Type: 'add'
                        }
                    }
                });
                modalInstance.result.then(function (response) {
                    if (response.Status === 1) {
                        $notifications.success('Thêm mới thành công', 'Thông báo');
                        initData();
                    };
                }, function () {
                    // $log.info('Modal dismissed at: ' + new Date());
                });
            };

            // Button UPDATE
            $scope.Button.Update = { Enable: true, Visible: true, Title: "Cập nhật", GrantAccess: true };
            $scope.Button.Update.Click = function (item) {
                var modalInstance = $uibModal.open({
                    templateUrl: dialogTemplateUrl,
                    controller: 'CreatePointOfSaleController',
                    size: 'lg',
                    keyboard: false,
                    backdrop: 'static',
                    resolve: {
                        item: item,
                        option: {
                            Type: 'edit'
                        }
                    }
                });

                modalInstance.result.then(function (response) {
                    if (response.Status !== 1) {
                        $notifications.error('Cập nhật xảy ra lỗi');
                        $scope.Grid.PageNumber = 1;
                    } else if (response.Status === 1) {
                        $notifications.success('Cập nhật thành công');
                    }
                    initData();
                }, function () {
                    //$log.info('Modal dismissed at: ' + new Date());
                });
            };
            // Button Delete
            $scope.Button.Delete = { };
            $scope.Button.Delete.Click = function (item) {
                var message = 'Bạn có chắc chắn muốn xóa POS ' + item.Name;
                var infoResult = UtilsService.OpenDialog(message, 'Xác nhận', 'Đồng ý', 'Hủy', 'sm', null);
                infoResult.result.then(function (modalResult) {
                    if (modalResult === 'confirm') {
                        var request = PointOfSaleApiService.DeleteById(item.POSId);
                        request.then(function (response) {
                            console.log(response);
                            if (response.data.Data !== null && response.data.Data[0].Result === true) {
                                $notifications.success('Xóa thành công');
                                initData(); 
                            } else {
                                $notifications.error('Xóa thất bại');
                            }
                        });
                    }
                });
            };


            $scope.Filter = {};
            $scope.Filter.Text = "";


            /* GRIDVIEWs */
            /* ------------------------------------------------------------------------------- */
            $scope.Grid = Object.assign({}, ConstantsApp.GRID_ATTRIBUTES);

            $scope.Grid.Refesh = function () {
                $scope.Grid.PageNumber = 1;
                $scope.Filter.Text = "";
                $location.search("ts", $scope.Filter.Text);
                initData();
            }

            $scope.Grid.DataHeader = [
                { Key: 'Order', Value: "#", Width: '1%', IsSortable: true },
                { Key: 'Code', Value: "Mã POS", Width: '10%', IsSortable: false },
                { Key: 'Name', Value: "Tên POS", Width: 'auto', IsSortable: false },
                { Key: 'CinemaName', Value: "Thuộc rạp", Width: '10%', IsSortable: false },
                { Key: 'ActionPermission', Value: "Quyền hành động", Width: 'auto', IsSortable: false },
                { Key: 'Status', Value: "Trạng thái", Width: '10%', IsSortable: true },
                { Key: 'Actions', Value: "Thao tác", Width: '15%', IsSortable: false }
            ];
            /* Sorting default*/
            var orderBy = $filter('orderBy');
            $scope.Grid.Order = function (predicate, reverse) {
                $scope.Grid.Data = orderBy($scope.Grid.Data, predicate, reverse);
            };
            /* This function is to Check all item */
            $scope.Grid.CheckAllItem = function () {
                $scope.Grid.Check = !$scope.Grid.Check;

                angular.forEach($scope.Grid.Data, function (item) {
                    item.selected = $scope.Grid.Check;
                });

                // If list has more than 1 item
                if ($scope.Grid.Check && $scope.Grid.Data.length > 1) {
                    $scope.Button.Update.Visible = false;
                    $scope.Button.Delete.Visible = true;
                }

                // If list has 1 item
                if ($scope.Grid.Check && $scope.Grid.Data.length === 1) {
                    $scope.Button.Update.Visible = true;
                    $scope.Button.Delete.Visible = true;
                }

                if (!$scope.Grid.Check) {
                    $scope.Button.Update.Visible = false;
                    $scope.Button.Delete.Visible = false;
                }
            };
            /* This function is to Check items in grid highlight*/
            $scope.Grid.GetItemClass = function (item) {
                if (item.selected === true) {
                    return 'selected';
                } else {
                    return '';
                }
            };
            /* This function is to Check items in grid highlight*/
            $scope.Grid.GetItemHeaderClass = function (item) {
                if (item.selected === true) {
                    return 'sorting_asc';
                } else if (item.selected === false) {
                    return 'sorting_desc';
                } else if (!item.IsSortable) {
                    return '';
                } else {
                    return 'sorting';
                }
            };
            /* Sorting grid By Click to header */
            $scope.Grid.ClickToHeader = function (item) {
                if (item.IsSortable) {
                    // Set all class header to default
                    angular.forEach($scope.dataHeader, function (items) {
                        if (items !== item) {
                            items.selected = null;
                        }
                    });
                    // Set for item click sorted
                    if (item.selected === true) {
                        item.selected = false;
                        $scope.Grid.Order(item.Key, false);
                    } else {
                        item.selected = true;
                        $scope.Grid.Order("-" + item.Key, false);
                    }
                }
            };
            /* This function is to Check items in grid*/
            $scope.Grid.CheckItem = function (item) {
                var count = 0;
                // Calculate item selected count     
                angular.forEach($scope.Grid.Data, function (item) {
                    if (item.selected === true) { count++; }
                });

                // Reset all buttons
                $scope.Button.Update.Visible = false;
                $scope.Button.Delete.Visible = false;
                $scope.Grid.Check = false;

                if (count === 1) {
                    $scope.Button.Update.Visible = true;
                    $scope.Button.Delete.Visible = true;
                    $scope.Grid.CheckAll = false;
                }

                if (count > 1) {
                    $scope.Button.Update.Visible = false;
                    $scope.Button.Delete.Visible = true;
                    $scope.Grid.CheckAll = false;
                }
                // If all items is checked
                if (count > 0 && count === $scope.Grid.Data.length) {
                    $scope.Button.Update.Visible = false;
                    $scope.Button.Delete.Visible = true;
                    $scope.Grid.Check = true;
                    $scope.Grid.CheckAll = true;
                }
            };

            $scope.Button.GoToPage = {};
            $scope.Button.GoToPage.Click = function (pageTogo) {
                $location.search("pn", pageTogo);
                $location.search("ts", $scope.Filter.Text);
                $location.search("ps", $scope.Grid.PageSize);
                $location.search("cid", $scope.Filter.CinemaId);
                initData();
            };

            $scope.Button.ChangePageSize = {};
            $scope.Button.ChangePageSize.Click = function () {
                $location.search("ps", $scope.Grid.PageSize);
                $location.search("ps", $scope.Grid.PageSize);
                $location.search("cid", $scope.Filter.CinemaId);
                initData();
            };

            $scope.Grid.Search = function () {
                $location.search("pn", 1);
                $location.search("ps", $scope.Grid.PageSize);
                $location.search("ts", $scope.Filter.Text);
                $location.search("cid", $scope.Filter.CinemaId);
                initData();
            };

            $scope.FilterRapChieu = function () {
                $location.search("pn", 1);
                $location.search("ps", $scope.Grid.PageSize);
                $location.search("ts", $scope.Filter.Text);
                $location.search("cid", $scope.Filter.CinemaId);
                initData(); 
            }

            /* Load data from API binding to Grid*/
            var initData = function () {
                $scope.Grid.CheckAll = false;
                var qs = $location.search();
                /* PageNumber-----*/
                if (typeof (qs["pn"]) !== 'undefined') {
                    $scope.Grid.PageNumber = parseInt(qs["pn"]);
                } else {
                    $location.search("pn", "1");
                    $scope.Grid.PageNumber = 1;
                }
                /* PageSize-----*/
                if (typeof (qs["ps"]) !== 'undefined') {
                    $scope.Grid.PageSize = parseInt(qs["ps"]);
                } else {
                    $location.search("ps", "10");
                    $scope.Grid.PageSize = 10;
                }

                if ($scope.Filter.CinemaSelected != null) {
                    $scope.Filter.CinemaId = $scope.Filter.CinemaSelected.CinemaId;
                }
                else {
                    $scope.Filter.CinemaId = null;
                }

                var jsonObject = {
                    PageNumber : $scope.Grid.PageNumber,
                    PageSize : $scope.Grid.PageSize,
                    TextSearch: $scope.Filter.Text,
                    CinemaId: $scope.Filter.CinemaId
                }
                var jsonString = encodeURIComponent(JSON.stringify(jsonObject));
                var request = PointOfSaleApiService.GetPointOfSaleByStringFilter(jsonString);
                $scope.listPromise.push(request);
                request.then(function onSuccess(response) {
                    //console.log("SUCCESS", response);
                    var responseResult = response.data;
                    $scope.Grid.Data = responseResult.Data;
                    for (var index = 0; index < $scope.Grid.Data.length; index++) {
                        $scope.Grid.Data[index].Order = (jsonObject.PageSize * (jsonObject.PageNumber - 1)) + index + 1;
                        $scope.Grid.Data[index].ActionPermissionString = '';
                        if ($scope.Grid.Data[index].ActionPermission.indexOf('GHI_NHAN_BAN') > -1) {
                            $scope.Grid.Data[index].ActionPermissionString += 'Ghi nhận bán hàng';
                        }
                        if ($scope.Grid.Data[index].ActionPermission.indexOf('GHI_NHAN_MUA') > -1) {
                            if ($scope.Grid.Data[index].ActionPermissionString.length > 0) {
                                $scope.Grid.Data[index].ActionPermissionString += '; Ghi nhận trả hàng';
                            }
                            else {
                                $scope.Grid.Data[index].ActionPermissionString += 'Ghi nhận trả hàng';
                            }
                        }
                    }
                    $scope.Grid.TotalCount = responseResult.TotalCount;
                    $scope.Grid.TotalPage = Math.ceil($scope.Grid.TotalCount / $scope.Grid.PageSize);
                    $scope.Grid.FromRecord = parseInt(($scope.Grid.PageNumber - 1) * $scope.Grid.PageSize + 1);
                    $scope.Grid.ToRecord = $scope.Grid.FromRecord + $scope.Grid.Data.length - 1;

                }, function onError(response) {
                    console.log("ERROR", response);
                });
            };

            var initDanhSachRap = function () {
                var request = PointOfSaleApiService.GetAllCinemaActive();
                request.then(function onSuccess(response) {
                    //console.log("SUCCESS", response);
                    $scope.Filter.DanhSachRap = response.data.Data;
                }, function onError(response) {
                    console.log("ERROR", response);
                });
            }

            var initMain = function () {
                initDanhSachRap();
                initData();
            };

            initMain();
        }]);
}();