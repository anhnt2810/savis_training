﻿//define(['jquery', 'app', 'angular-sanitize',
//    'components/factory/factory',
//    'components/service/apiservice',
//    'components/service/amdservice',
//], function (jQuery, app) {
    !function () {
        'use strict';
        var app = angular.module("SavisApp");
    app.controller('MetadataTemplateCtrl', ['$scope', '$rootScope', '$sce', '$filter', '$timeout', '$location', '$log', '$http', 'constantsFactory',
        'ConstantsApp', '$routeParams', 'Notifications', 'MetadataTemplateService',
        function ($scope, $rootScope, $sce, $filter, $timeout, $location, $log, $http, constantsFactory,
            ConstantsApp, $routeParams, notifications, MetadataTemplateService) {
      
            $scope.closeAlert = function (item) {
                $notifications.pop(item);
            };

            var itemDialogUrl = '/app-data/views/catalog/metadata-template/metadata-template-item.html';

            $scope.ListSelected = [];
            $scope.SelectAll = false;
            $scope.Mode = 1;
            /* Header grid datatable */
            $scope.Headers = [
                //{ Key: 'Order', Value: "STT", Width: '5%', Align: 'left' },
                { Key: 'Name', Value: "Tên loại hình tài liệu", Width: 'auto', Align: 'left' },
                { Key: 'Code', Value: "Mã", Width: 'auto', Align: 'left' },
                { Key: 'Status', Value: "Trạng thái", Width: 'auto', Align: 'left' },
                { Key: 'Description', Value: "Ghi chú", Width: 'auto', Align: 'left' },
                { Key: 'Handler', Value: "Xử Lý", Width: '150px', Align: 'left' },
            ];


            var loadData = function () {
                 var qs = $location.search();
                if (typeof (qs["search"]) !== 'undefined') {
                    $scope.TextSearch = qs["search"];
                } else {
                    $scope.IsNotReload = true; $scope.IsNotReload = true; $location.search("search", "");
                    $scope.TextSearch = "";
                }

                if (typeof (qs["pn"]) !== 'undefined') {
                    $scope.PageNumber = qs["pn"];
                } else {
                    $scope.IsNotReload = true; $scope.IsNotReload = true; $location.search("pn", "1");
                    $scope.PageNumber = 1;
                }

                if (typeof (qs["ps"]) !== 'undefined') {
                    $scope.PageSize = qs["ps"];
                } else {
                    $scope.IsNotReload = true; $scope.IsNotReload = true; $location.search("ps", "10");
                    $scope.PageSize = 10;
                }



                var postData = {};
                postData.PageNumber = $scope.PageNumber;
                postData.PageSize = $scope.PageSize;
                postData.TextSearch = $scope.TextSearch;

                var promise = MetadataTemplateService.GetFilter(postData);
                promise.success(function (data) {
                    $log.debug(data)
                    if (data.Status != null) {
                        $scope.ListData = data.Data;
                        $scope.TotalCount = data.TotalCount;
                        $scope.MaxSizePage = 7;
                        $scope.FromMetadataTemplate = 0;
                        $scope.ToMetadataTemplate = 0;
                        if ($scope.TotalCount !== 0) {
                            $scope.FromMetadataTemplate = parseInt(($scope.PageNumber - 1) * $scope.PageSize + 1);
                            $scope.ToMetadataTemplate = $scope.FromMetadataTemplate + $scope.ListData.length - 1;
                        }
                    }
                }).error(function (response) {
                    $log.debug(response);
                });
                return promise;
            };
            
            $scope.$on('$routeUpdate', function (e) {
                if (!$scope.IsNotReload) {
                    initData();
                } else {
                    $scope.IsNotReload = false;
                }
            });
            loadData();
            /* Sorting default*/
            var orderBy = $filter('orderBy');
            $scope.order = function (predicate, reverse) {
                $scope.ListData = orderBy($scope.ListData, predicate, reverse);
            };
            $scope.SelectItem = function (item) {
                if (!item.Selecting) {
                    var index = $scope.ListSelected.indexOf(item);
                    if (index >= 0) {
                        $scope.ListSelected.splice(index, 1);
                    }
                } else {
                    $scope.ListSelected.push(item);
                }
                if ($scope.ListSelected.length === $scope.ListData.length) {
                    $scope.SelectAll = true;
                } else {
                    $scope.SelectAll = false;
                }
            }

            $scope.SelectAllItem = function () {
                if ($scope.ListSelected.length === $scope.ListData.length) {
                    $scope.ListSelected = [];
                    angular.forEach($scope.ListData, function (file) {
                        file.Selecting = false;
                    });
                    $scope.SelectAll = false;
                } else {
                    $scope.ListSelected = [];
                    angular.forEach($scope.ListData, function (file) {
                        file.Selecting = true;
                        $scope.ListSelected.push(file);
                    });
                    $scope.SelectAll = true;
                }
            }
            /* This function is to Check items in grid highlight*/
            $scope.GetItemHeaderClass = function (item) {
                if (item.selected == true) {
                    return 'table-sort-asc';
                } else if (item.selected == false) {
                    return 'table-sort-desc';
                } else {
                    return 'table-sort-both';
                };
            };
            /* Sorting grid By Click to header */
            $scope.ClickToHeader = function (item) {
                // Set all class header to default
                angular.forEach($scope.ListData, function (items) {
                    if (items != item) {
                        items.selected = null;
                    };
                });
                // Set for item click sorted
                if (item.selected == true) {
                    item.selected = false;
                    $scope.order(item.Key, false);
                } else {
                    item.selected = true;
                    $scope.order(-item.Key, false);
                };
            };
            $scope.Button = {};
            $scope.Button.Create = {};
            $scope.Button.Create.Click = function () {
                window.location.href = '#/metadata-template-manager/creation';
            };

            $scope.Button.Update = {};
            $scope.Button.Update.Click = function (item) {
                if (typeof (item) === "undefined") {
                    item = $scope.ListSelected[0];
                }
                window.location.href = '#/metadata-template-manager/' + item.MetadataTemplateId;
            };

            $scope.Button.Copy = {};
            $scope.Button.Copy.Click = function (item) {
                if (typeof (item) === "undefined") {
                    item = $scope.ListSelected[0];
                }
                var promise = MetadataTemplateService.Clone(item.MetadataTemplateId);
                promise.success(function (response) {
                    $log.debug(response);
                    if (response.Status === 1) {
                        $notifications.success("Sao chép thành công", "Thông báo");
                    } else {
                        $notifications.error("Sao chép thành công", "Thông báo");
                    }
                    loadData();
                }).error(function (response) {

                    $log.error(response);
                    $notifications.error("Sao chép thành công", "Thông báo");
                });
            };

            $scope.Button.Delete = {};
            $scope.Button.Delete.Click = function (item) {
                if (typeof (item) === "undefined") {
                    item = $scope.ListSelected[0];
                }
                var infoResult = ConstantsApp.OpenDialog('Bạn có chắc chắn muốn xóa thông tin này!', 'Chú ý', 'Đồng ý', 'Đóng', 'sm');
                infoResult.result.then(function (modalResult) {
                    if (modalResult == 'confirm') {
                        var promise = MetadataTemplateService.Delete(item.MetadataTemplateId);
                        promise.success(function (response) {
                            $log.debug(response);
                            loadData();
                            if (response.Status === 1) {
                                if (response.Data.Result) {
                                    $notifications.success("Xóa thành công!", "Thông báo");
                                } else {
                                    $notifications.warning("Loại hình tài liệu đang được sử dụng!", "Cảnh báo");
                                }
                            } else {
                                $notifications.warning("Loại hình tài liệu đang được sử dụng!", "Cảnh báo");
                            }
                        }).error(function () {
                            $notifications.error("Xóa thất bại!", "Lỗi");
                        });

                    };
                });
            };
            $scope.Button.DeleteMany = {};
            $scope.Button.DeleteMany.Click = function () {
                var listDeleteting = [];
                angular.forEach($scope.ListData, function (item) {
                    if (item.Selecting) {
                        listDeleteting.push(item.MetadataTemplateId);
                    }
                });
                var infoResult = ConstantsApp.OpenDialog('Bạn có chắc chắn muốn xóa những chủ đề  này!', 'Xác nhận', 'Đồng ý', 'Hủy', 'sm', null);
                infoResult.result.then(function (modalResult) {
                    if (modalResult == 'confirm') {
                        var promise = MetadataTemplateService.DeleteMany(listDeleteting);
                        promise.success(function (response) {
                            $log.debug(response);
                            loadData();
                            if (response.Status === 1) {
                                var listDeleteField = [];
                                for (var i = 0; i < response.Data.length; i++) {
                                    var item = response.Data[i];
                                    if (item.Result == true) {
                                        listDeleteField.push({
                                            Name: item.Model.Name,
                                            Message: "Xóa thành công",
                                            Result: false
                                        });
                                        //$log.debug(item);
                                    } else if (item.Result == false) {
                                        //$log.debug(item);
                                        listDeleteField.push({
                                            Name: item.Model.Name,
                                            Message: item.Message,
                                            Result: true
                                        });
                                    };
                                }
                                var data = {};
                                data = listDeleteField;
                                var infoDeleteResult = ConstantsApp.OpenDialog('Kết quả xóa', 'Chú ý', '', 'Đóng', 'md', data);
                            } else {
                                $notifications.warning("Loại hình tài liệu đang được sử dụng!", "Cảnh báo");
                            }
                        }).error(function () {
                            $notifications.error("Xóa thất bại!", "Lỗi");
                        });
                    };
                });
            };

            $scope.Button.Reload = {};
            $scope.Button.Reload.Click = function () {
                $scope.IsNotReload = true; $scope.IsNotReload = true; $location.search("search", "");
                $scope.IsNotReload = true; $scope.IsNotReload = true; $location.search("pn", "1");
                var cmd = loadData();
                cmd.then(function () {
                    $notifications.success("Tải dữ liệu thành công!", "Thông báo");
                });
            };


            $scope.Button.GoToPageNumber = {};
            $scope.Button.GoToPageNumber.Click = function () {
                $scope.IsNotReload = true; $scope.IsNotReload = true; $location.search("pn", $scope.PageNumber);
                loadData();
            };

            $scope.Button.Search = {};
            $scope.Button.Search.Click = function () {
                $scope.IsNotReload = true; $scope.IsNotReload = true; $location.search("search", $scope.TextSearch);
                var cmd = loadData();
                cmd.then(function () {
                    $notifications.success("Tìm kiếm thành công!");
                });
            };
        }
    ]);
}();
