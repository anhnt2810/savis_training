﻿!function () {
    'use strict';
    var app = angular.module("SavisApp");
    app.controller('OrganizationCtrl', ['$scope', '$timeout', '$location', '$filter', '$http', '$uibModal', '$q', 'UtilsService', 'ConstantsApp', 'OrganizationApiService', 'toastr',
        function ($scope, $timeout, $location, $filter, $http, $uibModal, $q, UtilsService, ConstantsApp, OrganizationApiService, $notifications) {

            /* CONFIGs */
            /* ------------------------------------------------------------------------------- */
            $scope.Config = {};

            /* Declare variables url form popup add,edit,info */
            var itemDialogTemplateUrl = '/app-data/views/core/organization/organization-modal-item.html';

            // Lấy id người dùng hiện tại
            var currentUserId = app.CurrentUser.Id;
            /* ------------------------------------------------------------------------------- */

            /* FORM */
            /* ------------------------------------------------------------------------------- */
            $scope.Form = {};

            $scope.FormMode = {};
            $scope.FormMode.IsReadMode = false;
            $scope.FormMode.IsAllowDelete = false;

            /* ------------------------------------------------------------------------------- */


            /* BUTTONs */
            /* ------------------------------------------------------------------------------- */
            $scope.Button = {};
            // Button ADD
            $scope.Button.Add = { Enable: true, Visible: true, Title: "Thêm mới", GrantAccess: true };
            $scope.Button.Add.Click = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: itemDialogTemplateUrl,
                    controller: 'OrganizationModalCtrl',
                    size: 'md',
                    keyboard: false,
                    backdrop: 'static',
                    resolve: {
                        item: function () {
                            var obj = {};
                            return obj;
                        },
                        option: function () {
                            return [{ Type: 'add' }];
                        }
                    }
                });
                modalInstance.result.then(function (response) {
                    if (response.Status === 1) {
                        $notifications.success('Thêm mới thành công', 'Thông báo');
                        initData();
                    };
                }, function () {
                    // $log.info('Modal dismissed at: ' + new Date());
                });
            };

            // Button UPDATE
            $scope.Button.Update = { Enable: true, Visible: true, Title: "Cập nhật", GrantAccess: true };
            $scope.Button.Update.Click = function (item) {
                var modalInstance = $uibModal.open({
                    templateUrl: itemDialogTemplateUrl,
                    controller: 'OrganizationModalCtrl',
                    size: 'md',
                    keyboard: false,
                    backdrop: 'static',
                    resolve: {
                        item: function () {
                            var obj = {};
                            obj = item;
                            return obj;
                        },
                        option: function () {
                            return [{ Type: 'edit' }];
                        }
                    }
                });

                modalInstance.result.then(function (response) {
                    if (response.Status !== 1) {
                        $notifications.error('Cập nhật xảy ra lỗi');
                        $scope.Grid.PageNumber = 1;
                    } else if (response.Status === 1) {
                        $notifications.success('Cập nhật thành công');
                    }
                    $scope.Button.Delete.Visible = false;
                    initData();
                }, function () {
                    //$log.info('Modal dismissed at: ' + new Date());
                });
            };

            // Button DETAIL
            $scope.Button.Detail = {};
            $scope.Button.Detail.Click = function (item) {
                var modalInstance = $uibModal.open({
                    templateUrl: itemDialogTemplateUrl,
                    controller: 'OrganizationModalCtrl',
                    size: 'lg',
                    //windowClass: 'modal-full',
                    keyboard: false,
                    backdrop: 'static',
                    // Set parameter to chidform (popup form)
                    resolve: {
                        item: function () {
                            var obj = item;
                            return obj;
                        },
                        option: function () {
                            return { Type: 'detail', ReaderId: item.ReaderId };
                        }
                    }
                });

                modalInstance.result.then(function (response) {
                    if (response.Status === -2) {
                        $log.info('Modal dismissed at: ' + new Date());
                    }
                    $scope.Grid.Check = false;
                    $scope.Button.Delete.Visible = false;
                    $scope.Button.Update.Visible = false;
                    $scope.Grid.PageNumber = 1;
                    initData();
                }, function () {
                    //$log.info('Modal dismissed at: ' + new Date());
                });
            };

            // Button DELETE
            $scope.Button.Delete = { Enable: true, Visible: false, Title: "Xóa đã chọn", GrantAccess: true };
            $scope.Button.Delete.Click = function (item) {
                var listItemDeleteId = [];
                var count = 0;
                if (item !== null && typeof item !== 'undefined') {
                    listItemDeleteId.push(item.OrganizationId);
                    count++;
                } else {
                    for (var i = 0; i < $scope.Grid.Data.length; i++) {

                        if ($scope.Grid.Data[i].selected === true) {
                            listItemDeleteId.push($scope.Grid.Data[i].OrganizationId);
                            count++;
                        }
                    }
                }

                var message = "";
                if (count > 1) {
                    message = "Bạn có chắc muốn xóa những đơn vị, phòng ban này ?";
                } else {
                    message = "Bạn có chắc muốn xóa đơn vị, phòng ban này ?";
                }
                var infoResult = UtilsService.OpenDialog(message, 'Xác nhận', 'Đồng ý', 'Hủy', 'sm', null);

                infoResult.result.then(function (modalResult) {
                    if (modalResult === 'confirm') {
                        var listDeleteField = [];

                        var promise = OrganizationApiService.DeleteMany(listItemDeleteId);

                        promise.then(function (response) {
                            angular.forEach(response.data.Data, function (item) {
                                if (item.Result !== -1) {
                                    listDeleteField.push({ Name: item.Name, Result: item.Result === 1 ? true : false, Message: item.Message });
                                }
                                else if (item.Result === -1) {
                                    $scope.error('Lỗi hệ thống !');
                                }
                            });
                            $scope.Grid.PageNumber = 1;
                            initData();

                            var infoDeleteResult = UtilsService.OpenDialog('Kết quả xóa', 'Chú ý', '', 'Đóng', 'md', listDeleteField);
                        });
                    }
                });
                /* Check all item is selected => Request to API  */
            };

            // Button GOTOPAGE
            $scope.Button.GoToPage = {};
            $scope.Button.GoToPage.Click = function (pageTogo) {
                $location.search("pn", pageTogo);
                $location.search("ts", $scope.Filter.Text);
                $location.search("ps", $scope.Grid.PageSize);
                initData();
            };

            // Button GOTOPAGE
            $scope.Button.ChangePageSize = {};
            $scope.Button.ChangePageSize.Click = function () {
                $location.search("ps", $scope.Grid.PageSize);
                initData();
            };

            /* ------------------------------------------------------------------------------- */

            /* DROPDOWNLIST FUNCTIONS */
            /* ------------------------------------------------------------------------------- */
            $scope.DropDownList = {};

            /* ------------------------------------------------------------------------------- */

            /* FILTERs */
            /* ------------------------------------------------------------------------------- */
            $scope.Filter = {};
            $scope.Filter.Text = "";

            /* ------------------------------------------------------------------------------- */

            /* GRIDVIEWs */
            /* ------------------------------------------------------------------------------- */
            $scope.Grid = ConstantsApp.GRID_ATTRIBUTES;

            $scope.Grid.DataHeader = [
                { Key: 'Code', Value: "Mã", Width: '10%', IsSortable: false },
                { Key: 'Name', Value: "Tên đơn vị", Width: 'auto', IsSortable: false },
                { Key: 'ParentName', Value: "Đơn vị cha", Width: '20%', IsSortable: false },
                { Key: 'SoDienThoai', Value: "Điện thoại", Width: '10%', IsSortable: false },
                { Key: 'CreatedOnDate', Value: "Ngày tạo", Width: '10%', IsSortable: true },
                { Key: 'Actions', Value: "Thao tác", Width: '10%', IsSortable: false }
            ];

            $scope.Grid.Search = function () {
                $location.search("pn", 1);
                $location.search("ps", $scope.Grid.PageSize);
                $location.search("ts", $scope.Filter.Text);
                initData();
            };
            /* Sorting default*/
            var orderBy = $filter('orderBy');
            $scope.Grid.Order = function (predicate, reverse) {
                $scope.Grid.Data = orderBy($scope.Grid.Data, predicate, reverse);
            };
            /* This function is to Check all item */
            $scope.Grid.CheckAllItem = function () {
                $scope.Grid.Check = !$scope.Grid.Check;

                angular.forEach($scope.Grid.Data, function (item) {
                    item.selected = $scope.Grid.Check;
                });

                // If list has more than 1 item
                if ($scope.Grid.Check && $scope.Grid.Data.length > 1) {
                    $scope.Button.Update.Visible = false;
                    $scope.Button.Delete.Visible = true;
                }

                // If list has 1 item
                if ($scope.Grid.Check && $scope.Grid.Data.length === 1) {
                    $scope.Button.Update.Visible = true;
                    $scope.Button.Delete.Visible = true;
                }

                if (!$scope.Grid.Check) {
                    $scope.Button.Update.Visible = false;
                    $scope.Button.Delete.Visible = false;
                }
            };
            /* This function is to Check items in grid highlight*/
            $scope.Grid.GetItemClass = function (item) {
                if (item.selected === true) {
                    return 'selected';
                } else {
                    return '';
                }
            };
            /* This function is to Check items in grid highlight*/
            $scope.Grid.GetItemHeaderClass = function (item) {
                if (item.selected === true) {
                    return 'sorting_asc';
                } else if (item.selected === false) {
                    return 'sorting_desc';
                } else if (!item.IsSortable) {
                    return '';
                } else {
                    return 'sorting';
                }
            };
            /* Sorting grid By Click to header */
            $scope.Grid.ClickToHeader = function (item) {
                if (item.IsSortable) {
                    // Set all class header to default
                    angular.forEach($scope.dataHeader, function (items) {
                        if (items !== item) {
                            items.selected = null;
                        }
                    });
                    // Set for item click sorted
                    if (item.selected === true) {
                        item.selected = false;
                        $scope.Grid.Order(item.Key, false);
                    } else {
                        item.selected = true;
                        $scope.Grid.Order("-" + item.Key, false);
                    }
                }
            };
            /* This function is to Check items in grid*/
            $scope.Grid.CheckItem = function (item) {
                var count = 0;
                // Calculate item selected count     
                angular.forEach($scope.Grid.Data, function (item) {
                    if (item.selected === true) { count++; }
                });

                // Reset all buttons
                $scope.Button.Update.Visible = false;
                $scope.Button.Delete.Visible = false;
                $scope.Grid.Check = false;

                if (count === 1) {
                    $scope.Button.Update.Visible = true;
                    $scope.Button.Delete.Visible = true;
                    $scope.Grid.CheckAll = false;
                }

                if (count > 1) {
                    $scope.Button.Update.Visible = false;
                    $scope.Button.Delete.Visible = true;
                    $scope.Grid.CheckAll = false;
                }
                // If all items is checked
                if (count > 0 && count === $scope.Grid.Data.length) {
                    $scope.Button.Update.Visible = false;
                    $scope.Button.Delete.Visible = true;
                    $scope.Grid.Check = true;
                    $scope.Grid.CheckAll = true;
                }
            };
            /* This function click refesh grid program */
            $scope.Grid.Refesh = function () {
                $scope.Grid.PageNumber = 1;
                $scope.Filter.Text = "";
                $location.search("ts", $scope.Filter.Text);
                initData();
            };
            /* ------------------------------------------------------------------------------- */

            /* INIT FUNCTIONS */
            /* ------------------------------------------------------------------------------- */

            // Init nút chức năng theo quyền người dùng
            var initButtonByRightOfUser = function () {
                $scope.Button.Add.GrantAccess = UtilsService.CheckRightOfUser("ORGANIZATION-ADD");
                $scope.Button.Update.GrantAccess = UtilsService.CheckRightOfUser("ORGANIZATION-UPDATE");
                $scope.Button.Delete.GrantAccess = UtilsService.CheckRightOfUser("ORGANIZATION-DELETE");
                return true;
            };

            // Init nhóm quyền của người dùng đăng nhập
            var initUserRole = function () {
                //var promisRole = constantsAMD.Promise.UserInfo;
                //promisRole.success(function (response) {
                //    $scope.UserInRole = response.Data;
                //});
                //return promisRole;
            };

            /* Load data from API binding to Grid*/
            var initData = function () {
                $scope.Grid.CheckAll = false;
                $scope.Button.Delete.Visible = false;
                angular.forEach($scope.Grid.Data, function (items) {
                    items.selected = null;
                });
                $scope.Grid.Check = null;


                var qs = $location.search();
                /* PageNumber-----*/
                if (typeof (qs["pn"]) !== 'undefined') {
                    $scope.Grid.PageNumber = parseInt(qs["pn"]);
                } else {
                    $location.search("pn", "1");
                    $scope.Grid.PageNumber = 1;
                };
                /* PageSize-----*/
                if (typeof (qs["ps"]) !== 'undefined') {
                    $scope.Grid.PageSize = parseInt(qs["ps"]);
                } else {
                    $location.search("ps", "10");
                    $scope.Grid.PageSize = 10;
                };
                /* TextSearch-----*/
                if (typeof (qs["ts"]) !== 'undefined') {
                    $scope.Filter.Text = qs["ts"];
                } else {
                    $location.search("ts", "");
                    $scope.Filter.Text = "";
                };

                // Get data from api
                var postData = {};
                postData.PageNumber = $scope.Grid.PageNumber;
                postData.PageSize = $scope.Grid.PageSize;
                postData.TextSearch = $scope.Filter.Text;

                var p = OrganizationApiService.GetFilter(postData);
                p.then(function onSuccess(response) {
                    console.log("SUCCESS", response);
                    var responseResult = response.data;
                    $scope.Grid.Data = responseResult.Data;

                    //Enable next button
                    $scope.Grid.TotalCount = responseResult.TotalCount;
                    $scope.Grid.TotalPage = Math.ceil($scope.Grid.TotalCount / $scope.Grid.PageSize);
                    $scope.Grid.FromRecord = parseInt(($scope.Grid.PageNumber - 1) * $scope.Grid.PageSize + 1);
                    $scope.Grid.ToRecord = $scope.Grid.FromRecord + $scope.Grid.Data.length - 1;

                    //$notifications.info("Tải dữ liệu thành công", "Dữ liệu");
                }, function onError(response) {
                    console.log("ERROR", response);
                });
            };

            var initMain = function () {
                $q.all([initUserRole()]).then(function () {
                    initButtonByRightOfUser();
                    initData();
                });
            };

            initMain();

            /* ------------------------------------------------------------------------------- */
        }]);
    app.controller("OrganizationModalCtrl", ["$scope", "$q", "$uibModalInstance", "item", "option", "toastr", "OrganizationApiService",
        function ($scope, $q, $uibModalInstance, item, option, $notifications, OrganizationApiService) {

            $scope.Form = {};
            $scope.Form.Title = "";
            $scope.Form.Item = item;
            $scope.Form.Option = option;

            $scope.Button = {};
            $scope.Button.Close = {};
            $scope.Button.Close.Click = function () {
                $uibModalInstance.dismiss('cancel');
            };

            $scope.Autofocus = false;
            $scope.word = /^\s*\\*\/*\:*\?*\**\"*\<*\>*\|*\s*$/;
            $scope.timeValidation = /^\s*([01]?\d|2[0-3]):?([0-5]\d):?([0-5]\d)\s*$/;

            $scope.item = {};

            $scope.IsInfo = false;

            $scope.Select = {};

            var defaultOrgani = { OrganizationId: null, Name: "--Chọn đơn vị/phòng ban--" };

            /* Check parameters type = 'edit' to load data edit to form edit */

            /* Function button "Save" with add and edit button*/
            $scope.Save = function () {
                var position = null;
                /* If Type of parameter = 'add' ===> request to API with parameter */
                if ($scope.Form.Option[0].Type === 'add') {
                    var postData = {};
                    if (typeof ($scope.Select.DanhSachNhomOp) === 'undefined') {
                        postData.ParentId = '00000000-0000-0000-0000-000000000001';
                        postData.Level = 0;
                    } else {
                        postData.ParentId = (typeof $scope.Select.DanhSachNhomOp !== 'undefined' && $scope.Select.DanhSachNhomOp !== null) ? $scope.Select.DanhSachNhomOp.OrganizationId : null;
                        postData.Level = $scope.Select.DanhSachNhomOp.Level + 1;
                        postData.IdPath = $scope.Select.DanhSachNhomOp.IdPath;
                    }
                    postData.Name = $scope.Name;
                    postData.Description = $scope.Description;
                    postData.STT = $scope.STT;
                    postData.SoDienThoai = $scope.SoDienThoai;
                    postData.SoFax = $scope.SoFax;
                    postData.Email = $scope.Email;
                    postData.DiaChi = $scope.DiaChi;
                    postData.Loai = $scope.item.Loai;
                    postData.Code = $scope.Code;

                    var p = OrganizationApiService.AddNew(postData);
                    p.then(function onSuccess(response) {
                        var dataResult = response.data;
                        if (dataResult.Status === 1) {
                            $uibModalInstance.close(dataResult);
                        } else if (dataResult.Status === 0 && dataResult.Message === "Code exist") {
                            $notifications.warning('Mã đơn vị, phòng ban đã tồn tại', 'Cảnh báo');
                        } else $notifications.error(dataResult.Message, "Lỗi");
                    });
                }
                /* If Type of parameter = 'edit' ===> request to API with parameter */
                else {

                    var putData = {};

                    if (typeof ($scope.Select.DanhSachNhomOp) === 'undefined' || $scope.Select.DanhSachNhomOp.OrganizationId === null) {
                        putData.ParentId = '00000000-0000-0000-0000-000000000001';
                        putData.Level = 0;
                    } else {
                        putData.ParentId = (typeof $scope.Select.DanhSachNhomOp !== 'undefined' && $scope.Select.DanhSachNhomOp !== null) ? $scope.Select.DanhSachNhomOp.OrganizationId : null;
                        putData.Level = $scope.Select.DanhSachNhomOp.Level + 1;
                        putData.IdPath = $scope.Select.DanhSachNhomOp.IdPath;
                    }
                    putData.OrganizationId = $scope.Form.Item.OrganizationId;
                    putData.Name = $scope.Name;
                    putData.Description = $scope.Description;
                    putData.STT = $scope.STT;
                    putData.SoDienThoai = $scope.SoDienThoai;
                    putData.SoFax = $scope.SoFax;
                    putData.Email = $scope.Email;
                    putData.DiaChi = $scope.DiaChi;
                    putData.Loai = $scope.item.Loai;
                    putData.Code = $scope.Code;

                    p = OrganizationApiService.Update(putData);
                    p.then(function onSuccess(response) {
                        var dataResult = response.data;
                        if (dataResult.Status === 1) {
                            $uibModalInstance.close(dataResult);
                        } else if (dataResult.Status === 0) {
                            if (dataResult.Message === "Code exist") $notifications.warning('Mã đơn vị, phòng ban đã tồn tại', 'Cảnh báo');
                            if (dataResult.Message === "Circle parent") $notifications.warning('Mô hình cha con A->B->C->..->A không hợp lệ', 'Cảnh báo');
                        } else {
                            $notifications.error("Cập nhật thất bại", "Lỗi");
                        }
                    });
                }
            };

            /* Button delete items is selected */
            $scope.DeleteItems = function () {
                var position = null;
                if ($scope.Form.Option[0].Type === 'del') {
                    /* Check all item is selected => Request to API  */
                    var i = 0;
                    angular.forEach($scope.Form.Item, function (item) {
                        i++;
                    });
                    var j = 0;
                    /* Check all item is selected => Request to API  */
                    angular.forEach($scope.Form.Item, function (item) {
                        var promise = PhongBanApi.DeletePhongBan(item.OrganizationId);
                        promise.success(function (response) {
                            /* If delete item success then delete item in grid*/
                            if (response === true) {
                                item.DeleteSuccess = true;
                            } else {
                                item.DeleteSuccess = false;
                            }
                            j++;
                            if (j === i) {
                                $uibModalInstance.close($scope.Form.Item);
                            }
                        });
                    });
                }
            };

            var initOrganizations = function () {
                var p = OrganizationApiService.GetTree();
                p.then(function onSuccess(response) {
                    var dataResult = response.data;
                    if (dataResult.Status === 1) {
                        $scope.DanhSachNhom = dataResult.Data;
                        $scope.DanhSachNhom.unshift(defaultOrgani);
                        $scope.PhongBan = angular.copy($scope.DanhSachNhom);

                        if ($scope.Form.Option[0].Type === "edit") {
                            angular.forEach($scope.DanhSachNhom, function (itemOr) {
                                if (itemOr.OrganizationId === $scope.Form.Item.OrganizationId) {
                                    $scope.DanhSachNhom.splice($scope.DanhSachNhom.indexOf(itemOr), 1);
                                }
                            })
                        }
                    }
                });

                return p;
            };
            var initFormData = function () {
                /* Call function load data to Grid*/
                if ($scope.Form.Option[0].Type === 'edit') {
                    $scope.Form.Title = "Cập nhật đơn vị, phòng ban";
                    angular.forEach($scope.PhongBan, function (item) {
                        if (item.OrganizationId === $scope.Form.Item.ParentId) {
                            $scope.Select.DanhSachNhomOp = item;
                        }
                    });


                    $scope.OrganizationId = $scope.Form.Item.OrganizationId;
                    $scope.Name = $scope.Form.Item.Name;
                    $scope.Type = $scope.Form.Item.Type;
                    $scope.Description = $scope.Form.Item.Description;
                    $scope.STT = $scope.Form.Item.STT;
                    $scope.SoDienThoai = $scope.Form.Item.SoDienThoai;
                    $scope.SoFax = $scope.Form.Item.SoFax;
                    $scope.Email = $scope.Form.Item.Email;
                    $scope.DiaChi = $scope.Form.Item.DiaChi;
                    $scope.item.Loai = $scope.Form.Item.Loai;
                    $scope.Code = $scope.Form.Item.Code;

                } /* Check parameters type = 'info' to load data info to form info */
                else if ($scope.Form.Option[0].Type === 'info') {
                    $scope.TitleForm = $scope.Form.Option[0].TitleInfo;
                    $scope.Messenger = $scope.Form.Option[0].Messager;
                    $scope.Confirm = $scope.Form.Option[0].ButtonCancel;
                    $scope.IsInfo = true;

                    organizations.then(function (a) {
                        angular.forEach($scope.PhongBan, function (item) {
                            if (item.OrganizationId === $scope.Form.Item.ParentId) {
                                $scope.Select.DanhSachNhomOp = item;
                            }
                        });
                    });

                    $scope.OrganizationId = $scope.Form.Item.OrganizationId;
                    $scope.Name = $scope.Form.Item.Name;
                    $scope.Type = $scope.Form.Item.Type;
                    $scope.Description = $scope.Form.Item.Description;
                    $scope.STT = $scope.Form.Item.STT;
                    $scope.SoDienThoai = $scope.Form.Item.SoDienThoai;
                    $scope.SoFax = $scope.Form.Item.SoFax;
                    $scope.Email = $scope.Form.Item.Email;
                    $scope.DiaChi = $scope.Form.Item.DiaChi;
                    $scope.item.Loai = $scope.Form.Item.Loai;
                    $scope.Code = $scope.Form.Item.Code;

                } /* Check parameters type = 'add' to load data add to form add */
                else if ($scope.Form.Option[0].Type === 'del') {
                    $scope.TitleForm = "Thông báo";
                    var numberItemDelete = 0;
                    angular.forEach($scope.Form.Item, function (item) {
                        numberItemDelete++;
                    });
                    if (numberItemDelete > 1) {
                        $scope.Messenger = "Bạn có chắc chắn muốn xóa những danh mục phòng ban này?";
                    } else {
                        $scope.Messenger = "Bạn có chắc chắn muốn xóa danh mục phòng ban này?";
                    }
                } else if ($scope.Form.Option[0].Type === 'add') {
                    $scope.Form.Title = "Thêm mới đơn vị, phòng ban";
                    $scope.item.Loai = 1;
                    $scope.STT = 0;

                    //Select first item            
                } else {
                    $scope.TitleForm = $scope.Form.Option[0].TitleInfo;
                    $scope.Messenger = $scope.Form.Option[0].Messager;
                    $scope.listDelete = $scope.Form.Option[0].Data;
                    $scope.Confirm = $scope.Form.Option[0].ButtonCancel;
                }
            };

            var initMain = function () {
                $q.all([initOrganizations()]).then(function () {
                    initFormData();
                });
            };

            initMain();
        }]);
}();

