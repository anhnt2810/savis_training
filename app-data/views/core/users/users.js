﻿!function () {
    'use strict';
    var app = angular.module("SavisApp");
    app.controller('UserController', ['$scope', '$location', '$filter', '$http', '$uibModal', '$routeParams', '$log', '$q', 'UtilsService', 'toastr', 'UserApiService', 'OrganizationApiService', 'ConstantsApp',
        function ($scope, $location, $filter, $http, $uibModal, $routeParams, $log, $q, UtilsService, $notifications, UserApiService, OrganizationApiService, ConstantsApp) {

            var passwordChangeDialogTemplateUrl = '/app-data/views/core/users/passwordChangeDialog.html';
            var userItemUrl = '/app-data/views/core/users/user-item.html';
            var userADUrl = '/app-data/views/core/users/users-ad.html';

            var orderBy = $filter('orderBy');
            $scope.order = function (predicate, reverse) {
                $scope.ListData = orderBy($scope.ListData, predicate, reverse);
            };
            $scope.order('-age', false);
            $scope.ListSelected = [];
            $scope.IsSelectAll = false;
            /* Header grid datatable */
            $scope.Headers = [
                //{ Key: 'Index', Value: "STT", Width: '3%' },
                { Key: 'UserName', Value: "Tên tài khoản", Width: "15%" },
                { Key: 'FullName', Value: "Tên người dùng", Width: "18%" },
                { Key: 'RoleList', Value: "Nhóm quyền", Width: "50%" }
            ];

            $scope.Grid = ConstantsApp.GRID_ATTRIBUTES;
            $scope.ListData = [];
            $scope.Select = {};
            $scope.DanhSachNhom = [];
            var defaultOrgani = { OrganizationId: null, Name: "--Tất cả đơn vị/phòng ban--" };

            $scope.optionsState = [
                {
                    name: '--Tất cả trạng thái--',
                    value: 0
                },
                {
                    name: 'Khóa',
                    value: '1'
                },
                {
                    name: 'Không khóa',
                    value: '2'
                }];

            $scope.stateselect = $scope.optionsState[0];
            /* Sorting default*/
            $scope.order = function (predicate, reverse) {
                $scope.ListData = angular.copy(orderBy($scope.ListData, predicate, reverse));
            };

            $scope.ChangeStatus = function (data) {
                data.Status = !data.Status;
            }
            $scope.SelectItem = function (item) {
                if (!item.Selecting) {
                    var index = $scope.ListSelected.indexOf(item);
                    if (index >= 0) {
                        $scope.ListSelected.splice(index, 1);
                    }
                } else {
                    $scope.ListSelected.push(item);
                }
                if ($scope.ListSelected.length === $scope.ListData.length) {
                    $scope.IsSelectAll = true;
                } else {
                    $scope.IsSelectAll = false;
                }
            }

            $scope.SelectAllItem = function () {
                if ($scope.ListSelected.length === $scope.ListData.length) {
                    $scope.ListSelected = [];
                    angular.forEach($scope.ListData, function (file) {
                        file.Selecting = false;
                    });
                    $scope.IsSelectAll = false;
                } else {
                    $scope.ListSelected = [];
                    angular.forEach($scope.ListData, function (file) {
                        file.Selecting = true;
                        $scope.ListSelected.push(file);
                    });
                    $scope.IsSelectAll = true;
                }
            }

            $scope.GetItemHeaderClass = function (item) {
                if (item.selected == true) {
                    return "table-sort-asc";
                } else if (item.selected == false) {
                    return "table-sort-desc";
                } else {
                    return "table-sort-both";
                };
            };

            $scope.ClickToHeader = function (item) {
                angular.forEach($scope.Headers, function (items) {
                    if (items !== item) {
                        items.selected = null;
                    };
                });
                if (item.selected === true) {
                    item.selected = false;
                    $scope.order(item.Key, false);
                } else {
                    item.selected = true;
                    $scope.order("-" + item.Key, false);
                };
            };

            $scope.Button = {};
            $scope.Button.Create = { GrantAccess: true };
            $scope.Button.Create.Click = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: userADUrl,
                    controller: "UserADModalController",
                    size: "lg",
                    backdrop: 'static',
                    resolve: {
                        user: function () {
                            return {};
                        },
                        roles: function () {
                            return $scope.Roles;
                        },
                        applicationId: function () {
                            return $scope.SelectedApplication.SiteId;
                        },
                        options: function () {
                            return [{ Type: "add" }];
                        }
                    }
                });
                modalInstance.result.then(function (returnedUser) {
                    $scope.IsNotReload = true; $scope.IsNotReload = true; $location.search("pn", 1);
                    $scope.IsNotReload = true; $scope.IsNotReload = true; $location.search("ps", 10);
                    $scope.IsNotReload = true; $scope.IsNotReload = true; $location.search("s", "");
                    if (returnedUser.Status === 1) {
                        loadData();
                        $notifications.success("Thêm mới người dùng thành công", "Thông báo");
                    } else {
                        $notifications.error(returnedUser.Data.Message);
                    };
                }, function (response) { });
            };

            $scope.Button.Update = { GrantAccess: true };
            $scope.Button.Update.Click = function (item) {
                var modalInstance = $uibModal.open({
                    templateUrl: userItemUrl,
                    controller: 'UserModalController',
                    size: 'lg',
                    keyboard: false,
                    backdrop: 'static',
                    resolve: {
                        user: function () {
                            return item;
                        },
                        roles: function () {
                            return $scope.Roles;
                        },
                        applicationId: function () {
                            return $scope.SelectedApplication.SiteId;
                        },
                        options: function () {
                            return [{ Type: 'edit', id: $scope.id }];
                        }
                    }
                });

                modalInstance.result.then(function (response) {
                    if (response.Status !== 1) {
                        $notifications.error('Cập nhật xảy ra lỗi');
                        $scope.IsNotReload = true; $scope.IsNotReload = true; $location.search("pn", 1);
                        $scope.IsNotReload = true; $scope.IsNotReload = true; $location.search("ps", 10);
                        $scope.IsNotReload = true; $scope.IsNotReload = true; $location.search("s", "");
                    } else if (response.Status === 1) {
                        $scope.IsNotReload = true; $scope.IsNotReload = true; $location.search("pn", 1);
                        $scope.IsNotReload = true; $scope.IsNotReload = true; $location.search("ps", 10);
                        $scope.IsNotReload = true; $scope.IsNotReload = true; $location.search("s", "");
                        $notifications.success('Cập nhật thành công');
                    }
                    $scope.Button.Delete.Visible = false;
                    loadData();
                }, function () {

                });
            };

            $scope.Button.Info = {};
            $scope.Button.Info.Click = function (item) {
                var modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: userItemUrl,
                    controller: 'UserModalController',
                    size: 'lg',
                    resolve: {
                        user: function () {
                            return item;
                        },
                        roles: function () {
                            return $scope.Roles;
                        },
                        applicationId: function () {
                            return $scope.SelectedApplication.SiteId;
                        },
                        options: function () {
                            return [{ Type: 'info', TitleInfo: 'Chi tiết đơn vị/phòng ban', id: $scope.id }];
                        }
                    }
                });
                modalInstance.result.then(function (field) {
                    var cmd = loadData();
                    cmd.then(function () {
                        $scope.success("Cập nhật thành công bản ghi: " + field.Data.Name);
                    });
                }, function (response) { });
            };

            $scope.Button.Delete = { GrantAccess: true };
            $scope.Button.Delete.Click = function (item) {
                var listItemDelete = [];
                var count = 0;
                if (item != null) {
                    listItemDelete.push(item);
                    count = 1;
                }
                else {
                    for (var i = 0; i < $scope.ListData.length; i++) {

                        if ($scope.ListData[i].Selecting === true) {
                            listItemDelete.push($scope.ListData[i]);
                            count++;
                        };

                    };
                }
                var message = "";
                if (count > 1) {
                    message = "Bạn có chắc chắn muốn xóa những tài khoản người dùng này?";
                } else {
                    message = "Bạn có chắc chắn muốn xóa tài khoản người dùng này?";
                };

                var infoResult = UtilsService.OpenDialog(message, 'Xác nhận', 'Đồng ý', 'Hủy', 'sm', null);

                infoResult.result.then(function (modalResult) {
                    if (modalResult === 'confirm') {

                        var listDeleteField = [];
                        var postData = {};
                        postData.ListItemDelete = listItemDelete;

                        var promise = UserApiService.DeleteMany(postData);

                        promise.then(function (response) {
                            angular.forEach(response.data.Data, function (item) {
                                if (item.Result !== -1) {
                                    listDeleteField.push({ Name: item.UserName, Result: item.Result, Message: item.Message });
                                }
                                else if (item.Result === -1) {
                                    $scope.error("Lỗi hệ thống !");
                                }
                            });
                            $scope.currentPage = 1;
                            if ($scope.SelectedOrganization == "undefined" || $scope.SelectedOrganization == null) {
                                loadData();
                            }
                            else
                                loadData();

                            var infoDeleteResult = UtilsService.OpenDialog('Kết quả xóa', 'Chú ý', '', 'Đóng', 'md', listDeleteField);
                        });
                    };
                });
            };

            $scope.Button.DeleteMany = {};
            $scope.Button.DeleteMany.Click = function () {

            };

            $scope.Button.Reload = {};
            $scope.Button.Reload.Click = function () {
                $scope.IsNotReload = true; $scope.IsNotReload = true; $location.search("s", "");
                $scope.IsNotReload = true; $scope.IsNotReload = true; $location.search("as", "");
                $scope.IsNotReload = true; $scope.IsNotReload = true; $location.search("pn", "1");
                //$scope.filterText = "";
                $scope.stateselect = 0;
                $scope.stateselect = $scope.optionsState[0];
                $scope.Select.DanhSachNhomOp = $scope.PhongBan[0];
                var cmd = loadData();
                cmd.then(function () {
                    //$scope.success("Tải lại thành công!");
                });
            };


            $scope.Button.Lock = { GrantAccess: true };
            $scope.Button.Lock.Click = function (user, state) {
                activeUser(user, state);
            };


            $scope.Button.ChangePassword = { GrantAccess: true };
            $scope.Button.ChangePassword.Click = function (data) {
                openChangePasswordForm(data);
            };

            $scope.Button.GoToPage = {};
            $scope.Button.GoToPage.Click = function (pageTogo) {
                $scope.IsNotReload = true; $scope.IsNotReload = true; $location.search("pn", pageTogo);
                $scope.IsNotReload = true; $scope.IsNotReload = true; $location.search("ps", $scope.Grid.PageSize);
                loadData();
            };

            // Button GOTOPAGE
            $scope.Button.ChangePageSize = {};
            $scope.Button.ChangePageSize.Click = function () {
                $location.search("ps", $scope.Grid.PageSize);
                loadData();
            };

            $scope.Button.Search = {};
            $scope.Button.Search.Click = function () {
                $scope.IsNotReload = true; $scope.IsNotReload = true; $location.search("s", $scope.TextSearch);
                $scope.IsNotReload = true; $scope.IsNotReload = true; $location.search("pn", 1);
                $scope.IsNotReload = true; $scope.IsNotReload = true; $location.search("as", angular.toJson($scope.Condition));
                var cmd = loadData();
                cmd.then(function () {
                    $scope.success("Tìm kiếm thành công!");
                });
            };

            $scope.Button.ResetSearch = {};
            $scope.Button.ResetSearch.Click = function () {
                $scope.TextSearch = "";
                $scope.Condition = {};
                $scope.Button.Search.Click();
            };

            $scope.ChangeOrganization = function () { loadData() };
            $scope.selectState = function () {
                if ($scope.stateselect.value == 0)
                    $scope.state = null;
                else if ($scope.stateselect.value == 1)
                    $scope.state = true;
                else if ($scope.stateselect.value == 2)
                    $scope.state = false;
                $scope.currentPage = 1;
                // $scope.pageSize = 10;
                loadData();
            };
            var activeUser = function (user, state) {
                var promise = UserApiService.ActiveUser(user.UserId, state);
                promise.then(function (response) {
                    angular.forEach($scope.ListData,
                        function (item) {
                            if (item.UserId === response.data.Data.UserId) {
                                item.Active = response.data.Data.Active;
                            }
                        });
                });
            };
            /*Open Edit Password Form*/
            var openChangePasswordForm = function (item) {
                var modalInstance = $uibModal.open({
                    templateUrl: passwordChangeDialogTemplateUrl,
                    controller: 'AdminChangePassWordCtrl',
                    size: 'md',
                    resolve: {
                        userId: function () {
                            return item.UserName;

                        }
                    }
                });

                modalInstance.result.then(function (returnedUser) {
                    if (returnedUser.Result === 1) {
                        $notifications.success(returnedUser.Message, "Thông báo");
                    } else {
                        $notifications.error(returnedUser.Message);
                    };
                }, function () {
                    // $log.info('Modal dismissed at: ' + new Date());
                });
            };
            /* This function click open form delete program */
            $scope.OpenDeleteForm = function (user) {
                var listItemDelete = [];
                var count = 0;
                if (user != null) {
                    listItemDelete.push(user);
                    count = 1;
                }
                else {
                    for (var i = 0; i < $scope.ListData.length; i++) {

                        if ($scope.ListData[i].Selecting === true) {
                            listItemDelete.push($scope.ListData[i]);
                            count++;
                        };

                    };
                }
                var message = "";
                if (count > 1) {
                    message = "Bạn có chắc chắn muốn xóa những tài khoản người dùng này?";
                } else {
                    message = "Bạn có chắc chắn muốn xóa tài khoản người dùng này?";
                };

                var infoResult = UtilsService.OpenDialog(message, 'Xác nhận', 'Đồng ý', 'Hủy', 'sm', null);

                infoResult.result.then(function (modalResult) {
                    if (modalResult === "confirm") {

                        var listDeleteField = [];
                        var postData = {};
                        postData.ListItemDelete = listItemDelete;

                        var promise = UserApiService.DeleteMany(postData);

                        promise.then(function (response) {
                            angular.forEach(response.data.Data, function (item) {
                                if (item.Result !== -1) {
                                    listDeleteField.push({ Name: item.UserName, Result: item.Result, Message: item.Message });
                                }
                                else if (item.Result === -1) {
                                    $scope.error("Lỗi hệ thống !");
                                }
                            });
                            $scope.currentPage = 1;
                            if ($scope.SelectedOrganization == "undefined" || $scope.SelectedOrganization == null) {
                                loadData();
                            }
                            else
                                loadData();

                            var infoDeleteResult = UtilsService.OpenDialog('Kết quả xóa', 'Chú ý', '', 'Đóng', 'md', listDeleteField);
                        });
                    };
                });
                /* Check all item is selected => Request to API  */
            };

            /* INIT FUNCTIONS */
            /* ------------------------------------------------------------------------------- */

            // Init nút chức năng theo quyền người dùng
            var initButtonByRightOfUser = function () {
                $scope.Button.Create.GrantAccess = UtilsService.CheckRightOfUser("USER-ADD");
                $scope.Button.Update.GrantAccess = UtilsService.CheckRightOfUser("USER-UPDATE");
                $scope.Button.Delete.GrantAccess = UtilsService.CheckRightOfUser("USER-DELETE");
                $scope.Button.Lock.GrantAccess = UtilsService.CheckRightOfUser("USER-LOCK");
                $scope.Button.ChangePassword.GrantAccess = UtilsService.CheckRightOfUser("USER-CHANGE-PASSWORD");
                return true;
            };

            /*Lay ve danh sach tat ca site hien tai */
            var initApplication = function () {
                // Show sidebar and full width content
                $("#sidebarDiv").css('display', '');
                $("#contentDiv").attr('class', 'page-content-wrapper');

                var promise = $http({
                    method: 'GET',
                    url: ConstantsApp.BASED_API_URL + ConstantsApp.API_SITE_URL,
                    headers: {
                        'Content-type': ' application/json'
                    }
                }).then(function (response) {
                    if (response.data.Data != null) {
                        $scope.ListApplications = response.data.Data;
                        //$scope.SelectedApplication = $scope.ListApplications[0];
                        angular.forEach($scope.ListApplications, function (site) {
                            if (site.SiteId.toLowerCase() === app.CurrentUser.ApplicationId.toLowerCase())
                                $scope.SelectedApplication = site;
                        });
                    }
                });
                return promise;
            };

            var initOrganizations = function () {

                var promise = OrganizationApiService.GetTree();
                promise.then(function (response) {
                    $scope.DanhSachNhom = response.data.Data;
                    $scope.DanhSachNhom.unshift(defaultOrgani);
                    $scope.PhongBan = angular.copy($scope.DanhSachNhom);
                });

                return promise;
            };

            var loadData = function () {
                var organizations = initOrganizations();
                var qs = $location.search();
                if (typeof (qs["s"]) !== "undefined") {
                    $scope.TextSearch = qs["s"];
                } else {
                    $scope.IsNotReload = true; $scope.IsNotReload = true; $location.search("s", "");
                    $scope.TextSearch = "";
                }
                if (typeof (qs["pn"]) !== "undefined") {
                    $scope.Grid.PageNumber = parseInt(qs["pn"]);
                    $scope.Grid.PageNumber = Math.ceil($scope.Grid.PageNumber);
                    $scope.IsNotReload = true; $scope.IsNotReload = true; $location.search("pn", $scope.Grid.PageNumber);
                    if ($scope.Grid.PageNumber <= 0) {
                        $scope.IsNotReload = true; $scope.IsNotReload = true; $location.search("pn", "1");
                        $scope.Grid.PageNumber = 1;
                    }
                    if ($scope.Grid.PageNumber > $scope.Grid.TotalPage && typeof ($scope.Grid.TotalPage) !== "undefined") {
                        $scope.IsNotReload = true; $scope.IsNotReload = true; $location.search("pn", $scope.Grid.TotalPage);
                        if ($scope.Grid.TotalPage > 0) { $scope.Grid.PageNumber = $scope.Grid.TotalPage; }
                    }
                } else {
                    $scope.IsNotReload = true; $scope.IsNotReload = true; $location.search("pn", "1");
                    $scope.Grid.PageNumber = 1;
                }

                if (typeof (qs["ps"]) !== "undefined") {
                    $scope.Grid.PageSize = parseInt(qs["ps"]);
                    if ($scope.Grid.PageSize <= 0) {
                        $scope.IsNotReload = true; $scope.IsNotReload = true; $location.search("ps", "10");
                        $scope.Grid.PageSize = 10;
                    }
                } else {
                    $scope.IsNotReload = true; $scope.IsNotReload = true; $location.search("ps", "10");
                    $scope.Grid.PageSize = 10;
                }


                if (typeof (qs["as"]) !== "undefined") {
                    var advandSearch = qs["as"];
                    $scope.IsAdvancedSearch = true;
                    $scope.Condition = {};
                    try {
                        $scope.Condition = angular.fromJson(advandSearch);
                    } catch (e) { }
                } else {
                    $scope.Condition = {};
                    $scope.IsAdvancedSearch = false;
                }
                var currentOrganizationId = "";
                var isDonVi = false;
                if (typeof ($scope.Select.DanhSachNhomOp) != "undefined") {
                    currentOrganizationId = $scope.Select.DanhSachNhomOp.OrganizationId;
                    isDonVi = $scope.Select.DanhSachNhomOp.Loai === 1 ? true : false;
                }
                // User query data
                var postData = {};
                postData.PageNumber = $scope.Grid.PageNumber;
                postData.PageSize = $scope.Grid.PageSize;
                postData.TextSearch = $scope.TextSearch;
                //postData.Condition = $scope.Condition;
                postData.IsDonVi = isDonVi;
                postData.OrganizationId = currentOrganizationId;
                postData.IsIncludeRightsAndRoles = true;
                postData.UserId = null;
                postData.RoleId = null;
                postData.Type = 0;// 0 is system user
                try {
                    postData.State = $scope.state;
                } catch (err) { }
                try {
                    postData.RoleId = $scope.selectedrole.RoleId;
                } catch (err) { }
                postData.ApplicationId = $scope.SelectedApplication.SiteId;
                var promise = UserApiService.GetFilter(postData);
                promise.then(function onSuccess(response) {
                    $scope.ListData = [];
                    $scope.Grid.TotalCount = 0;
                    $scope.Grid.TotalPage = 0;
                    $scope.Grid.FromRecord = 0;
                    $scope.Grid.ToRecord = 0;
                    response = response.data;
                    if (response.Status === 1) {
                        //$scope.sucess("Tải dữ liệu thành công !");
                        $scope.ListData = response.Data;
                        organizations.then(function (a) {
                            angular.forEach($scope.PhongBan, function (item) {
                                if (item.OrganizationId === response.Data.OrganizationId) {
                                    $scope.Select.DanhSachNhomOp = item;
                                }
                            });
                        });
                        $scope.Grid.TotalCount = response.TotalCount;
                        $scope.Grid.TotalPage = Math.ceil($scope.Grid.TotalCount / $scope.Grid.PageSize);
                        $scope.Grid.FromRecord = 0;
                        $scope.Grid.ToRecord = 0;
                        if ($scope.Grid.TotalCount !== 0) {
                            $scope.Grid.FromRecord = Math.ceil(($scope.Grid.PageNumber - 1) * $scope.Grid.PageSize + 1);
                            $scope.Grid.ToRecord = $scope.Grid.FromRecord + $scope.Grid.PageSize - 1;
                            if ($scope.Grid.ToRecord > $scope.Grid.TotalCount) {
                                $scope.Grid.ToRecord = $scope.Grid.TotalCount;
                            }
                        }
                    } else if (response.Status === 0) {
                        $scope.error = "Không có dữ liệu!";
                    } else {
                        $scope.error = "Không tải được dữ liệu: " + response.Message;
                    }
                }, function onError(reasonResponse) {
                    $log.debug(reasonResponse);
                    $scope.error = "Không tải được dữ liệu: " + reasonResponse;
                });
                return promise;
            };

            /* Load Role list from Api*/
            var initSystemRoles = function () {
                $http({
                    method: 'GET',
                    url: ConstantsApp.BASED_API_URL + ConstantsApp.API_ROLE_URL
                })
                    .then(function (response) {
                        if (response.Status !== -1) $scope.Roles = response.data.Data;
                    });
            };
            /* Init page */
            var initApplicationPromise = initApplication();
            initApplicationPromise.then(function () {
                $q.all([initSystemRoles(), initButtonByRightOfUser()]).then(function () {
                    loadData();
                });
            });

            /* ------------------------------------------------------------------------------- */
        }]);
    /* Controller for froms Popup Add,Edit,Delete or Info */
    app.controller("UserModalController", ["$scope", "$q", "$http", "$uibModalInstance", '$uibModal', 'user', 'options', 'roles', "toastr", 'UtilsService', "UserApiService", "RoleApiService", 'OrganizationApiService', 'applicationId',
        function ($scope, $q, $http, $uibModalInstance, $uibModal, user, options, roles, $notifications, UtilsService, UserApiService, RoleApiService, OrganizationApiService, applicationId) {

            $scope.Form = {};
            $scope.Form.Title = "";

            //$scope.word = /^\s*\w*\s*\g*$/;
            $scope.word = /(?!.*[\.\-\_]{2,})^[a-zA-Z0-9\.\-\_]{3,24}$/;
            $scope.options = options;
            $scope.Select = {};
            var defaultOrgani = { OrganizationId: null, Name: "--Chọn đơn vị/phòng ban--" };
            var defaultChucVu = { CatalogItemId: null, Name: "--Chọn chức vụ--" };
            /* Declare constant domain and path to Api */
            $scope.Autofocus = true;
            $scope.isEdit = false;
            $scope.IsInfo = false;
            /* assign parameters passed from Form Parent (programmanager.html) or controller (app)*/
            $scope.CurrentUser = user;
            if ($scope.CurrentUser != null) {
                $scope.selected = {
                    item: $scope.CurrentUser[0]
                };
            };

            $scope.items = {};

            $scope.readOnly = false;

            /* Define the form type : add/edit/info */
            var formType = $scope.options[0].Type;

            /*
            * System roles
            */
            $scope.Roles = roles;
            $scope.SelectedRoles = [];
            $scope.CurrentPhongBan = {};

            var initRolesOfCurrentUser = function () {

                var promise = UserApiService.GetRolesByUser($scope.CurrentUser.UserId, applicationId);
                promise.then(function (response) {
                    var userRoles = response.data.Data.Roles;

                    angular.forEach(userRoles, function (userRole) {
                        $scope.SelectedRoles.push(userRole.RoleCode);
                    });
                });

                return promise;
            };

            var initOrganizations = function () {
                var promise = OrganizationApiService.GetTree();
                promise.then(function (response) {
                    $scope.DanhSachNhom = response.data.Data;
                    $scope.DanhSachNhom.unshift(defaultOrgani);
                    $scope.PhongBan = angular.copy($scope.DanhSachNhom);

                    if ($scope.options[0].Type === 'edit') {
                        var position = null;
                        for (var i = 0; i < $scope.CurrentUser.length; i++) {
                            if ($scope.CurrentUser[i].UserId === $scope.options[0].id) {
                                position = i;
                                break;
                            };
                        };
                        if (position != null) {

                            angular.forEach($scope.DanhSachNhom, function (item) {
                                if (item.OrganizationId === items[position].OrganizationId) {
                                    $scope.Select.DanhSachNhomOp = item;
                                }
                            });

                        }

                    }
                });

                return promise;
            };

            var initFormData = function () {
                var organizations = initOrganizations();
                //initChucVu();
                /* Check parameters type = 'edit' to load data edit to form edit */
                if ($scope.options[0].Type === "edit") {

                    $scope.isEdit = true;
                    $scope.readOnly = true;
                    $scope.Form.Title = "Cập nhật người dùng";

                    if ($scope.CurrentUser != null) {
                        var p = UserApiService.GetUserInfo($scope.CurrentUser.UserId);
                        p.then(function onSuccess(response) {

                            var dataResult = response.data;
                            $scope.UserName = dataResult.Data.UserName;
                            $scope.Email = dataResult.Data.Email;
                            $scope.Mobile = dataResult.Data.Mobile;
                            $scope.Comment = dataResult.Data.Comment;
                            $scope.NickName = dataResult.Data.NickName;
                            $scope.FullName = dataResult.Data.FullName;
                            $scope.AnhDaiDien = dataResult.Data.Avatar;
                            if ($scope.AnhDaiDien == null || $scope.AnhDaiDien === '') {
                                $scope.AnhDaiDien = 'assets/admin/layout4/img/icon-user-default.png';
                            }

                            organizations.then(function (a) {
                                angular.forEach($scope.PhongBan, function (item) {
                                    if (item.OrganizationId === dataResult.Data.OrganizationId) {
                                        $scope.Select.DanhSachNhomOp = item;
                                    }
                                });
                            });
                            //chucVuI.then(function (a) {
                            //    angular.forEach($scope.ChucVuData, function (item) {
                            //        if (item.CatalogItemId === data.Data.ChucVuId) {
                            //            $scope.Select.ChucVuOp = item;
                            //        }
                            //    });
                            //});

                            $scope.Active = $scope.CurrentUser.Active;

                        });
                    }

                } /* Check parameters type = 'info' to load data info to form info */
                else if ($scope.options[0].Type === "info") {
                    $scope.IsInfo = true;
                    $scope.Messenger = $scope.options[0].Messager;
                    $scope.readOnly = true;
                    $scope.ProgramTitle = "Thông tin chi tiết tài khoản người dùng";
                    if ($scope.CurrentUser != null) {
                        var promise = $http({
                            method: 'GET',
                            url: apiUrl + ApiUsersUrl + '/' + $scope.CurrentUser.UserId + '/profile',
                        })
                            .success(function (data) {
                                if (data.Data != null) {
                                    $scope.UserName = data.Data.UserName;
                                    $scope.Email = data.Data.Email;
                                    $scope.Mobile = data.Data.Mobile;
                                    $scope.Comment = data.Data.Comment;
                                    $scope.NickName = data.Data.NickName;
                                    $scope.FullName = data.Data.FullName;
                                    $scope.AnhDaiDien = data.Data.Avatar;
                                    if ($scope.AnhDaiDien == null || $scope.AnhDaiDien == '') {
                                        $scope.AnhDaiDien = 'assets/admin/layout4/img/icon-user-default.png';
                                    }
                                    $scope.PhongBanInfo = data.Data.OrganizationName;
                                    $scope.DonVi = data.Data.ParentOrganizationName;

                                    chucVuI.then(function (a) {
                                        angular.forEach($scope.ChucVuData, function (item) {
                                            if (item.CatalogItemId === data.Data.ChucVuId) {
                                                $scope.ChucVuInfo = item.Name;
                                            }
                                        });
                                    });
                                    // $scope.Mobile = $scope.CurrentUser.Mobile;
                                    //$scope.ChucVu = $scope.CurrentUser.ChucVu;
                                    $scope.Active = $scope.CurrentUser.Active;

                                }
                            }).error(function (response) {
                                $log.error();
                                $log.debug(response);
                            });
                    }
                } /* Check parameters type = 'add' to load data add to form add */

                else if ($scope.options[0].Type === "add") {
                    $scope.Form.Title = "Thêm mới người dùng";
                    $scope.isAdd = true;
                    $scope.Active = true;
                    //Select first item            
                }
                else {
                    $scope.Form.Title = $scope.options[0].TitleInfo;
                    $scope.Messenger = $scope.options[0].Messager;
                    $scope.listDelete = $scope.options[0].Data;
                    $scope.Confirm = $scope.options[0].ButtonCancel;
                };
            };

            // Init nút chức năng theo quyền người dùng
            var initButtonByRightOfUser = function () {
                $scope.Button.ChangePassword.GrantAccess = UtilsService.CheckRightOfUser("USER-CHANGE-PASSWORD");
                return true;
            };

            $scope.Button = {};
            $scope.Button.ChangePassword = { GrantAccess: true };
            $scope.Button.ChangePassword.Click = function () {
                openFormEditPassWord();
            };

            /* 
            * User name availability check
            * This section provive user availability check, to check whether an user name is valid or not
            * A scope function will be defined to store information of availability
            */
            $scope.Availability = {};
            $scope.Availability.Status = 0;

            /* Check availability function */
            $scope.CheckNameAvailability = function (name) {
                var p = UserApiService.CheckNameAvailability(name);
                p.then(function onSuccess(response) {
                    if (response.data.Status === -1) {
                        $scope.Availability.Status = -1;
                        return false;
                    }
                    $scope.Availability.Status = 1;
                    return true;
                });
            };

            var addUser = function () {
                var postData = {};
                postData.ApplicationId = applicationId;
                postData.UserOrganization = {};
                postData.UserOrganization.OrganizationId = (typeof $scope.Select.DanhSachNhomOp != 'undefined' && $scope.Select.DanhSachNhomOp != null) ? $scope.Select.DanhSachNhomOp.OrganizationId : null;
                postData.UserOrganization.CreatedByUserId = app.CurrentUser.Id;
                postData.UserOrganization.LastModifiedByUserID = app.CurrentUser.Id;
                postData.UserName = ($scope.UserName + "").replace(/(?:\s)\s/g, '');
                postData.FullName = ($scope.FullName + "").replace(/(?:\s)\s/g, '');
                postData.Mobile = $scope.Mobile;
                postData.Email = $scope.Email;
                postData.CreatedByUserId = app.CurrentUser.Id;
                postData.ModifiedByUserId = app.CurrentUser.Id;
                postData.Password = $scope.items.Password;
                postData.Roles = [];
                postData.Active = $scope.Active;
                postData.Type = true;
                postData.NickName = ($scope.NickName + "").replace(/(?:\s)\s/g, '');
                postData.Avatar = "assets/layout4/img/icon-user-default.png";
                postData.ChucVuId = (typeof $scope.Select.ChucVuOp != "undefined" && $scope.Select.ChucVuOp != null) ? $scope.Select.ChucVuOp.CatalogItemId : null;
                // Process selected roles
                angular.forEach($scope.SelectedRoles, function (selectedRoleCode) {
                    angular.forEach($scope.Roles, function (role) {
                        if (selectedRoleCode === role.RoleCode) {
                            // Add this selected role to putData
                            postData.Roles.push(role);
                        }
                    });
                });

                var p = UserApiService.AddNew(postData);
                p.then(function onSuccess(response) {
                    var dataResult = response.data;
                    if (dataResult.Status === 1) {
                        $uibModalInstance.close(dataResult);
                    } else if (dataResult.Status === 0 && dataResult.Message === "Code exist") {
                        $notifications.warning("Mã người dùng đã tồn tại", "Cảnh báo");
                    } else {
                        $notifications.error("Thêm mới thất bại", "Lỗi");
                    }
                });
            };

            var updateUser = function () {
                var putData = {};
                putData.UserOrganization = {};
                putData.ApplicationId = applicationId;
                putData.UserId = $scope.CurrentUser.UserId;
                putData.UserName = $scope.UserName;
                putData.UserOrganization.OrganizationId = (typeof $scope.Select.DanhSachNhomOp != "undefined" && $scope.Select.DanhSachNhomOp != null) ? $scope.Select.DanhSachNhomOp.OrganizationId : null;
                putData.ChucVuId = (typeof $scope.Select.ChucVuOp != "undefined" && $scope.Select.ChucVuOp != null) ? $scope.Select.ChucVuOp.CatalogItemId : null;
                putData.ModifiedByUserId = app.CurrentUser.Id;
                putData.UserOrganization.LastModifiedByUserID = app.CurrentUser.Id;
                putData.FullName = ($scope.FullName + "").replace(/(?:\s)\s/g, '');
                putData.Mobile = $scope.Mobile;
                putData.Email = $scope.Email;
                putData.Password = $scope.items.Password;
                putData.Roles = [];
                putData.Active = $scope.Active;
                putData.NickName = ($scope.NickName + "").replace(/(?:\s)\s/g, '');
                putData.UpdateDate = new Date();
                // Process selected roles
                angular.forEach($scope.SelectedRoles, function (selectedRoleCode) {
                    angular.forEach($scope.Roles, function (role) {
                        if (selectedRoleCode === role.RoleCode) {
                            // Add this selected role to putData
                            putData.Roles.push(role);
                        }
                    });
                });

                var p = UserApiService.Update(putData);
                p.then(function onSuccess(response) {
                    var dataResult = response.data;
                    if (dataResult.Status === 1) {
                        $uibModalInstance.close(dataResult);
                    } else {
                        $notifications.error("Cập nhật thất bại", "Lỗi");
                    }
                });
            };

            /* Function button "Save" with add and edit button*/
            $scope.Save = function () {
                $scope.user_form.$setSubmitted();
                //if ($scope.Select.DanhSachNhomOp === undefined) {
                //    $scope.$broadcast('UiSelectDemo1');
                //    return false;
                //    $scope.error("Vui lòng kiểm tra thông tin đã nhập");
                //    return;
                //}
                //else {
                //    if ($scope.Select.DanhSachNhomOp.OrganizationId === null) {
                //        $scope.$broadcast('UiSelectDemo1');
                //        return false;
                //        $scope.error("Vui lòng kiểm tra thông tin đã nhập");
                //        return;
                //    }
                //}
                if ($scope.user_form.$invalid) {
                    angular.element("[name='" + $scope.user_form.$name + "']").find('.ng-invalid:visible:first').focus();
                    UtilsService.OpenDialog('Vui lòng kiểm tra thông tin đã nhập!', 'Chú ý', '', 'Đóng', 'md', '');
                    return false;
                }

                /* If Type of parameter = 'add' ==> request to API with parameter */
                if ($scope.options[0].Type === "add") {
                    $scope.UserName = ($scope.UserName + "").replace(/(?:\s)\s/g, '');
                    if ($scope.UserName === "undefined")
                        $scope.UserName = "";

                    var checkName = UserApiService.CheckNameAvailability($scope.UserName);
                    checkName.then(function onSuccess(response) {
                        if (response.data.Status === -1) {
                            UtilsService.OpenDialog("Tên tài khoản đã tồn tại, vui lòng chọn tên khác!", "Chú ý", '', "Đóng", "md", '');
                            return false;
                        }

                        /* Check the two inserted password */
                        if ($scope.items.Password !== $scope.items.ConfirmedPassword) {
                            UtilsService.OpenDialog('Mật khẩu xác nhận không chính xác!', 'Chú ý', '', 'Đóng', 'md', '');
                            return false;
                        }

                        if ($scope.Select.DanhSachNhomOp.OrganizationId === null) {
                            UtilsService.OpenDialog("Bạn cần chọn đơn vị/phòng ban!", 'Chú ý', '', 'Đóng', 'md', '');
                            return false;
                        }

                        // Add user
                        addUser();

                        return true;
                    });
                }
                /* If Type of parameter = 'edit' ==> request to API with parameter */
                else {

                    if ($scope.Select.DanhSachNhomOp.OrganizationId === null) {
                        UtilsService.OpenDialog("Bạn cần chọn đơn vị/phòng ban!", 'Chú ý', '', 'Đóng', 'md', '');
                        return false;
                    }

                    updateUser();
                };
            };

            //SelectAllRole
            $scope.SelectAllRole = function () {
                ////angular.forEach($scope.Roles, function (rl) {
                ////    $scope.SelectedRoles.push(rl);
                ////});
                //$scope.SelectedRoles = $scope.Roles;
            };

            /* Button close Popup Form */
            $scope.Cancel = function () {
                $uibModalInstance.dismiss();
            };

            var profileDialogTemplateUrl = '/app-data/views/core/users/passwordChangeDialog.html';
            var infoDialogTemplateUrl = '/app-data/views/core/users/infoDialog.html';

            /*Open Edit Password Form*/
            var openFormEditPassWord = function () {
                var modalInstance = $uibModal.open({
                    templateUrl: profileDialogTemplateUrl,
                    controller: "AdminChangePassWordCtrl",
                    size: "md",
                    resolve: {
                        userId: function () {
                            return $scope.CurrentUser.UserName;

                        }
                    }
                });

                modalInstance.result.then(function (returnedUser) {
                    if (returnedUser.Result === 1) {
                        $notifications.success(returnedUser.Message, "Thông báo");
                    } else {
                        $notifications.error(returnedUser.Message);
                    };
                }, function () {
                    // $log.info('Modal dismissed at: ' + new Date());
                });
            };

            /* Form data initiate */
            initFormData();
            if (formType !== "add") {
                $scope.Availability.Status = 1;
                initRolesOfCurrentUser();
                initButtonByRightOfUser();
            }
        }]);
    /*
    *  Administrator controller
    *  This controller is used to change user password
    */
    app.controller("AdminChangePassWordCtrl", ["$scope", "$uibModalInstance", 'userId', "toastr", "UserApiService", "UtilsService",
        function ($scope, $uibModalInstance, userId, $notifications, UserApiService, UtilsService) {

            $scope.Form = {};

            $scope.Form.Title = "Đổi mật khẩu";

            $scope.btnSave = function () {
                var postData = {};
                postData.UserName = userId;
                postData.Password = $scope.PasswordNew;
                postData.PasswordNew = $scope.PasswordNew;
                postData.IsChangePassApplicationSide = true;

                if ($scope.PasswordNew !== $scope.ConfirmedPassword) {
                    UtilsService.OpenDialog('Mật khẩu xác nhận không chính xác!', 'Chú ý', '', 'Đóng', 'md', '');
                    return false;
                }

                var p = UserApiService.ChangePass(userId, postData);
                p.then(function onSuccess(response) {
                    var dataResult = response.data;
                    $uibModalInstance.close(dataResult);
                });

                //$http({
                //    method: 'PUT',
                //    url: ConstantsApp.BASED_API_URL + ApiUsersUrl + '/' + userId + '/password',
                //    data: postData
                //})
                //.success(function (data) {

                //    if (data != null) {
                //        $uibModalInstance.close(data);
                //    };

                //}).error(function (response) {
                //    $log.error();
                //    $log.debug(response);
                //});
            };

            /* Button close Popup Form */
            $scope.cancel = function () {
                $uibModalInstance.dismiss('cancel');
            };

        }]);
}();
