﻿
!function () {
    'use strict';
    var app = angular.module("SavisApp");
    app.controller('SiteAccessCtrl', ['$scope', '$rootScope', '$q', '$route', '$http', '$stateParams', 'UtilsService', '$log', 'ConstantsApp', 'SiteApiService', 'ngAuthController', 'UserApiService',
        function ($scope, $rootScope, $q, $route, $http, $stateParams, UtilsService, $log, ConstantsApp, SiteApiService, ngAuthController, UserApiService) {

            // Config for api uri
            var apiUrl = ConstantsApp.BASED_API_URL;

            // ** IMPORTANT : Authorize mode config
            // If this mode is set to true, user can only access sites which is accesible
            // otherwise user can access all hosted site in the system
            var isAuthorizeMode = true;

            // Config for data storage, default site and site list
            $scope.Sites = [];
            $scope.DefaultSite = {};

            // Current siteId
            var currentSiteId = "";

            // Mode : mode is the current state of select site screen, there is only 2 mode
            // 1. Default mode : this mode is activated when user first navigate to dashboard/default, when user's in this mode, site
            // user will be automatically redirect after site choosed
            // 2. Intent mode : this mode is used to choose another site, user won't be automatically refreshed           
            var mode = $stateParams.mode;

            // If mode is not 'intent', we set it to default
            var isDefaultMode = (mode !== 'intent');

            // Hide sidebar if we're in default mode        
            if (isDefaultMode) {
                //$('#sidebarDiv').css('display', 'none');
                //$('#headerDiv').css('display', 'none');
                //$('#footerDiv').css('display', 'none');
                $('#loadingDiv').css("z-index", 0);
            }
            // Get all hosted sites
            var initAllSites = function () {
                var promise = SiteApiService.GetAllSite();
                promise.then(function onSuccess(response) {
                    var responseResult = response.data;
                    $scope.Sites = responseResult.Data;

                    angular.forEach($scope.Sites, function (site) {
                        if (site.SiteId === currentSiteId) {
                            site.IsSelected = true;
                        }
                    });

                }).catch(function (response) {

                });
                return promise;
            };

            // Get all accessible sites of this current user
            var initAccessibleSites = function () {               
                var promise = SiteApiService.GetAccessibleSite();
                promise.then(function onSuccess(response) {
                    var responseResult = response.data;
                    if (responseResult !== null && responseResult.Status === 1) {
                        $scope.Sites = responseResult.Data;
                        angular.forEach($scope.Sites, function (site) {
                            if (site.SiteId === currentSiteId) {
                                site.IsSelected = true;
                            }
                        });
                    }
                }).catch(function (response) {
                    $log.error();
                    $log.debug(response);
                });
                return promise;
            };

            // Get default site of this current user
            var initDefaultSites = function () {              
                var promise = SiteApiService.GetDefaultSite();
                promise.then(function onSuccess(response) {
                    var responseResult = response.data;
                    if (responseResult !== null && responseResult.Status === 1) {
                        // Load data to grid
                        $scope.DefaultSite = responseResult.Data;
                    }
                }).catch(function (response) {
                    $log.error();
                    $log.debug(response);
                });

                return promise;
            };

            // Check lock status user
            var isLockUser = true;
            var checkLockUser = function () {                
                // Người dùng là nhân viên nội bộ của hệ thống
                var userType = 0;
                var promise = UserApiService.CheckUserExistAndActiveByType(userType);
                promise.then(function onSuccess(response) {
                    var responseResult = response.data;
                    if (responseResult !== null && responseResult.Data) {
                        isLockUser = false;
                    }
                }).catch(function (response) {
                    $log.error();
                    $log.debug(response);
                });
            };

            // Init function in authorize mode
            // in authorize mode, we get accessible sites and default site from API
            // instead of get all available sites
            var initInAuthorizeMode = function () {               
                var deferred = $q.defer();
                var accessibleSitesPromise = initAccessibleSites();
                var defaultSitePromise = initDefaultSites();
                var checkLockUserPromise = checkLockUser();
                $q.all([checkLockUserPromise, accessibleSitesPromise, defaultSitePromise]).then(function (response) {
                    // 0. Kiểm tra user có bị lock và có tồn tại hay ko?
                    if (isLockUser) {
                        // Tài khoản bị khóa
                        var message = "Tài khoản không tồn tại hoặc đã bị khóa. Liên hệ Quản trị hệ thống để được hỗ trợ !";
                        var deniedResult = UtilsService.OpenDialog(message, 'Thông báo', 'Xác nhận chuyển hướng', '', 'sm', '');

                        deniedResult.result.then(function (modalResult) {
                            if (modalResult === 'confirm') {
                                ngAuthController.logOut();
                            }
                        });
                    }
                    else {
                        // 1. Kiểm tra nếu có site truy cập mặc định 
                        if (!angular.equals($scope.DefaultSite, {})) {
                            // Tự động đến site mặc định
                            $scope.SelectSite($scope.DefaultSite);
                        }
                        else {
                            // 2. Nếu không có site truy cập mặc định kiểm tra danh sách cho phép truy cập
                            if ($scope.Sites.length === 0) {
                                // Không có site được phép truy cập đưa ra thông báo
                                message = "Tài khoản của bạn chưa được phân quyền truy cập vào hệ thống. Liên hệ Quản trị hệ thống để được hỗ trợ !";
                                deniedResult = UtilsService.OpenDialog(message, 'Thông báo', 'Xác nhận chuyển hướng', '', 'md', '');

                                deniedResult.result.then(function (modalResult) {
                                    if (modalResult === 'confirm') {
                                        ngAuthController.logOut();
                                    }
                                });
                            } else {
                                // Mặc định truy cập site đầu tiên trong danh sách
                                var site = $scope.Sites[0];
                                $scope.SelectSite(site);
                            }
                        }
                    }
                });
            };

            // Init function in admin mode
            // For this mode, all site will loaded
            var initInAdminMode = function () {
                initAllSites();
            };

            var init = function () {
                if (isAuthorizeMode) initInAuthorizeMode();
                else initInAdminMode();
            }

            // Init site from local storage
            var initSiteFromStorage = function () {
                currentSiteId = window.localStorage['SiteId'];

                if (currentSiteId && isDefaultMode) {

                    // Hide header
                    $('#sidebarDiv').css('display', '');

                    // Refresh page
                    $location.url('/');
                } else {
                    return false;
                }
            };

            // Select site, this function runs when user click to Select button, we select site, 
            // set local storage value and refresh the page
            $scope.SelectSite = function (site) {
                debugger
                window.localStorage['site_id'] = site.SiteId;
                window.localStorage['site_name'] = site.SiteName;
                window.localStorage['valid_user'] = "1";
                app.CurrentUser.ApplicationId = window.localStorage['site_id'];               
                // Tracking login
                var postData = {
                    FullName: "",
                    Object: "Hệ thống",
                    Description: "Đăng nhập thành công vào phần mềm",
                    Action: "LOGIN"
                };

                var cmd = $http({
                    method: 'POST',
                    url: ConstantsApp.BASED_API_URL + ConstantsApp.API_SYSTEM_TRACKING_URL,
                    headers: {
                        'Content-type': ' application/json'
                    },
                    data: postData
                });

                $q.all([cmd]).then(function (r) {
                    // Refresh the whole windows
                    console.log('REFRESH THE PAGE');
                    window.location.href = '/';
                });
              
            };

            $scope.CheckSelectedSite = function (site) {
                if (site.SiteId === window.localStorage['site_id'])
                    return 'text-danger'
                else
                    return 'text-primary'
            }

            // Run init function
            init();

        }])
}();
