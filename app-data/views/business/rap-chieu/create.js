﻿!function () {
    'use strict';
    var app = angular.module("SavisApp");
    app.controller("CreateCinemaCtrl", ["$scope", "$q", "$uibModalInstance", '$timeout', 'Upload', 'UtilsService', 'ConstantsApp', "item", "option", "toastr", "CinemaApiService",
        function ($scope, $q, $uibModalInstance, $timeout, Upload, UtilsService, ConstantsApp, item, option, $notifications, CinemaApiService) {

            $scope.Form = {};
            $scope.Form.Title = "";
            $scope.Form.Item = item;
            $scope.Form.Option = option;
            $scope.DanhMuc = {};
            $scope.Button = {};
            $scope.Button.Close = {};
            $scope.listPromise = [];
            $scope.Cinema = {};
            $scope.Autofocus = false;
            $scope.word = /^\s*\\*\/*\:*\?*\**\"*\<*\>*\|*\s*$/;
            $scope.timeValidation = /^\s*([01]?\d|2[0-3]):?([0-5]\d):?([0-5]\d)\s*$/;
            $scope.item = {};
            $scope.IsInfo = false;
            $scope.Select = {};
            $scope.DanhMuc.TinhThanh = [];
            $scope.DanhMuc.Selected = null;

            //đong button
            $scope.Button.Close.Click = function () {
                $uibModalInstance.dismiss('cancel');
            };


            //#region các danh mục liên kết tới phòng chiếu
            //lay danh muc thanh pho
            var getListCity = function () {
                var p = CinemaApiService.GetListCity(1, 100, "");
                $scope.listPromise.push(p);
                p.then(function onSuccess(response) {
                    var dataResult = response.data;
                    if (dataResult.Status === 1) {
                        var responseResult = response.data;
                        $scope.DanhMuc.TinhThanh = responseResult.Data;

                    } else $notifications.error("Lấy danh sách thất bại", "Lỗi");
                });
            }

            //lay danh muc loại rap chiếu
            var getListCinemaType = function () {
                var p = CinemaApiService.GetListCinemaType();
                $scope.listPromise.push(p);
                p.then(function onSuccess(response) {
                    var dataResult = response.data;
                    if (dataResult.Status === 1) {
                        var responseResult = response.data;
                        $scope.DanhMuc.LoaiRapChieu = responseResult.Data;

                    } else $notifications.error("Lấy danh sách thất bại", "Lỗi");
                });
            }

            //lay du lieu danh muc
            var initCategory = function () {
                getListCity();
                getListCinemaType();
            }

            //#endregion
            //#region up ảnh đại diện cho rạp
            $scope.Upload = {};
            $scope.UploadFiles = function (item, files, errFiles, typeUpload) {
                $scope.Upload.Files = files;
                $scope.Upload.ErrFiles = errFiles;
                angular.forEach(files, function (fileitem) {
                    fileitem.IsUploadDone = false;
                    fileitem.upload = Upload.upload({
                        url: ConstantsApp.BASED_UPLOADFILE_URL + ConstantsApp.API_UPLOAD_URL + "?token=",//can dinh nghia token la gi
                        data: {
                            file: fileitem,
                            token: "asd" //can dinh nghia token la gi
                        }
                    });

                    fileitem.upload.then(function (response) {
                        $timeout(function () {
                            if (response.data.data === null) {
                                fileitem.IsUploadDone = true;
                            } else {
                                var lenghtPath = response.data.Data[0].AbsolutePath.split("/").length - 1;
                                if (lenghtPath >= 0) {
                                    $scope.NameFile = response.data.Data[0].AbsolutePath.split("/")[lenghtPath];
                                }
                                //if (!($scope.Cinema.Picture === "" || $scope.Cinema.Picture === null || $scope.Cinema.Picture === undefined)) {
                                $scope.Cinema.Picture = response.data.Data[0].AbsolutePath;
                                $scope.Cinema.LinkPicture = ConstantsApp.BASED_FILE_URL + "/" + response.data.Data[0].AbsolutePath;
                                // }
                            }
                        });
                    }, function (response) {
                        if (response.status > 0) {
                            fileitem.IsUploadDone = true;
                            $scope.Upload.ErrFiles.push(fileitem);
                        }
                    }, function (evt) {
                        fileitem.progress = parseInt(100.0 * evt.loaded / evt.total);
                    });
                });
            };

            $scope.RemoveImage = function (item) {
                var message = "Bạn có chắc muốn xóa ảnh này?";
                var infoResult = UtilsService.OpenDialog(message, 'Xác nhận', 'Đồng ý', 'Hủy', 'sm', null);
                infoResult.result.then(function (modalResult) {
                    if (modalResult === 'confirm') {
                        if ($scope.Cinema.Picture !== null && $scope.Cinema.Picture !== undefined) {
                            $scope.Cinema.Picture = "";
                            $scope.Cinema.LinkPicture = "";
                        }
                    }
                });
            };
            //#endregion
            /* Function button "Save" with add and edit button*/
            $scope.Save = function () {
                var position = null;
                /* If Type of parameter = 'add' ===> request to API with parameter */
                if ($scope.Form.Option[0].Type === 'add') {
                    var postData = {};
                    postData.CityId = $scope.Cinema.CitySelected.CatalogItemId;
                    postData.CinemaTypeId = $scope.Cinema.LoaiRapChieuSelected.CatalogItemId;
                    postData.Code = $scope.Cinema.Code;
                    postData.SAPCode = $scope.Cinema.SAPCode;
                    postData.Name = $scope.Cinema.Name;
                    postData.Name_F = $scope.Cinema.Name_F;
                    postData.Address = $scope.Cinema.Address;
                    postData.Address_F = $scope.Cinema.Address_F;
                    postData.Description = $scope.Cinema.Description;
                    postData.Description_F = $scope.Cinema.Description_F;
                    postData.Header = $scope.Cinema.Header;
                    postData.Footer = $scope.Cinema.Footer;
                    postData.Order = $scope.Cinema.Order;
                    postData.PhoneNumber = $scope.Cinema.PhoneNumber;
                    postData.Ext = $scope.Cinema.Ext;
                    postData.TaxNumber = $scope.Cinema.TaxNumber;
                    postData.Fax = $scope.Cinema.Fax;
                    postData.Status = $scope.Cinema.Status;

                    postData.Latitude = $scope.Cinema.Latitude;
                    postData.Longtitude = $scope.Cinema.Longtitude;
                    postData.EnterpriseName = $scope.Cinema.EnterpriseName;
                    postData.EnterpriseAddress = $scope.Cinema.EnterpriseAddress;
                    postData.Picture = $scope.Cinema.Picture;
                    postData.FormNo = $scope.Cinema.FormNo;
                    postData.Sign = $scope.Cinema.Sign;

                    var p = CinemaApiService.AddNew(postData);
                    p.then(function onSuccess(response) {
                        var dataResult = response.data;
                        if (dataResult.Status === 1) {
                            $uibModalInstance.close(dataResult);
                        } else if (dataResult.Status === 0) {
                            $notifications.warning('Mã rạp chiếu tồn tại', 'Cảnh báo');
                        } else $notifications.error("Thêm mới thất bại", "Lỗi");
                    });
                }
                    /* If Type of parameter = 'edit' ===> request to API with parameter */
                else {

                    var putData = {};
                    putData.CityId = $scope.Cinema.CitySelected.CatalogItemId;
                    putData.CinemaTypeId = $scope.Cinema.LoaiRapChieuSelected.CatalogItemId;
                    putData.Code = $scope.Cinema.Code;
                    putData.SAPCode = $scope.Cinema.SAPCode;
                    putData.Name = $scope.Cinema.Name;
                    putData.Name_F = $scope.Cinema.Name_F;
                    putData.Address = $scope.Cinema.Address;
                    putData.Address_F = $scope.Cinema.Address_F;
                    putData.Description = $scope.Cinema.Description;
                    putData.Description_F = $scope.Cinema.Description_F;
                    putData.Header = $scope.Cinema.Header;
                    putData.Footer = $scope.Cinema.Footer;
                    putData.Order = $scope.Cinema.Order;
                    putData.PhoneNumber = $scope.Cinema.PhoneNumber;
                    putData.Ext = $scope.Cinema.Ext;
                    putData.TaxNumber = $scope.Cinema.TaxNumber;
                    putData.Fax = $scope.Cinema.Fax;
                    putData.Status = $scope.Cinema.Status;
                    putData.CinemaId = $scope.Cinema.CinemaId;
                    putData.Latitude = $scope.Cinema.Latitude;
                    putData.Longtitude = $scope.Cinema.Longtitude;
                    putData.EnterpriseName = $scope.Cinema.EnterpriseName;
                    putData.EnterpriseAddress = $scope.Cinema.EnterpriseAddress;
                    putData.Picture = $scope.Cinema.Picture;
                    putData.FormNo = $scope.Cinema.FormNo;
                    putData.Sign = $scope.Cinema.Sign;


                    var p = CinemaApiService.Edit(putData);
                    p.then(function onSuccess(response) {
                        var dataResult = response.data;
                        if (dataResult.Status === 1) {
                            $uibModalInstance.close(dataResult);
                        } else if (dataResult.Status === 0) {
                            $notifications.warning('Mã rạp chiếu tồn tại', 'Cảnh báo');
                        } else $notifications.error("Cập nhật thất bại", "Lỗi");
                    });
                }
            };

            var initFormData = function (item) {
                /* Call function load data to Grid*/
                if ($scope.Form.Option[0].Type === 'edit' || $scope.Form.Option[0].Type === 'detail') {
                    $scope.Form.Title = "Cập nhật rạp chiếu";
                    $scope.Cinema.CityId = item.CityId;
                    $scope.Cinema.LoaiRapChieuId = item.CinemaTypeId;
                    $scope.Cinema.Code = item.Code;
                    $scope.Cinema.SAPCode = item.SAPCode;
                    $scope.Cinema.Name = item.Name;
                    $scope.Cinema.Name_F = item.Name_F;
                    $scope.Cinema.Address = item.Address;
                    $scope.Cinema.Address_F = item.Address_F;
                    $scope.Cinema.Description = item.Description;
                    $scope.Cinema.Description_F = item.Description_F;
                    $scope.Cinema.Header = item.Header;
                    $scope.Cinema.Footer = item.Footer;
                    $scope.Cinema.EnterpriseName = item.EnterpriseName;
                    $scope.Cinema.EnterpriseAddress = item.EnterpriseAddress;
                    $scope.Cinema.Order = item.Order;
                    $scope.Cinema.PhoneNumber = item.PhoneNumber;

                    $scope.Cinema.Ext = item.Ext;
                    $scope.Cinema.TaxNumber = item.TaxNumber;

                    $scope.Cinema.Fax = item.Fax;
                    $scope.Cinema.Latitude = item.Latitude;
                    $scope.Cinema.Longtitude = item.Longtitude;
                    $scope.Cinema.Status = item.Status;
                    $scope.Cinema.Picture = item.Picture;
                    $scope.Cinema.LinkPicture = ConstantsApp.BASED_FILE_URL + "/" + item.Picture;
                    $scope.Cinema.FormNo = item.FormNo;
                    $scope.Cinema.Sign = item.Sign;

                    $scope.Cinema.CinemaId = item.CinemaId;

                } else if ($scope.Form.Option[0].Type === 'add') {
                    $scope.Form.Title = "Thêm mới rạp chiếu";
                    $scope.Cinema.Status = true;
                    $scope.Cinema.Order = 0;
                    // Lấy mã sinh tự động                   
                    var p = CinemaApiService.GetCinemaAutoCode("CNM", 3);
                    p.then(function onSuccess(response) {
                        var dataResult = response.data;
                        if (dataResult.Status === 1) {
                            $scope.Cinema.Code = dataResult.Data;
                        } else $notifications.error("Mã sinh tự động thất bại", "Lỗi");
                    });

                } else {
                    $scope.TitleForm = $scope.Form.Option[0].TitleInfo;
                    $scope.Messenger = $scope.Form.Option[0].Messager;
                    $scope.listDelete = $scope.Form.Option[0].Data;
                    $scope.Confirm = $scope.Form.Option[0].ButtonCancel;
                }
                if ($scope.Form.Option[0].Type === 'detail') {
                    $scope.ModalMode = 'detail';
                    $scope.Form.Title = "Thông tin chi tiết rạp chiếu";
                }
            };

            //lấy item của mảng *category* với điều kiện là trường *field* có giá trị là *value*
            var getItemFromList = function (field, category, value) {
                for (var i = 0; i < category.length; i++) {
                    if (category[i][field] == value) {
                        return category[i];
                    }
                }
            }

            //binding id du lieu voi danh muc
            var bindingCategory = function () {
                $scope.Cinema.LoaiRapChieuSelected = getItemFromList('CatalogItemId', $scope.DanhMuc.LoaiRapChieu, $scope.Cinema.LoaiRapChieuId);
                $scope.Cinema.CitySelected = getItemFromList('CatalogItemId', $scope.DanhMuc.TinhThanh, $scope.Cinema.CityId);
            }
            var initMain = function () {
                initCategory();
                if ($scope.Form.Option[0].Type === 'add') {
                    initFormData($scope.Form.Item);
                } else {
                    initFormData($scope.Form.Item);
                    $q.all($scope.listPromise).then(function () {
                        bindingCategory();
                    });
                }

            };

            initMain();
        }]);
}();

